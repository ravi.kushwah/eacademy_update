-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 29, 2023 at 04:13 PM
-- Server version: 5.7.34
-- PHP Version: 8.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kamleshyadav_superadmin`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_versions`
--

CREATE TABLE `app_versions` (
  `id` int(11) NOT NULL,
  `latest_version` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_versions`
--

INSERT INTO `app_versions` (`id`, `latest_version`) VALUES
(1, 11);

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `added_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` varchar(250) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `batches`
--

CREATE TABLE `batches` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `sub_cat_id` int(11) NOT NULL,
  `batch_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `batch_type` int(11) NOT NULL COMMENT '1= batch free , 2=batch paid',
  `batch_price` varchar(100) NOT NULL,
  `batch_offer_price` varchar(50) NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `batch_image` varchar(200) NOT NULL,
  `no_of_student` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `pay_mode` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batches`
--

INSERT INTO `batches` (`id`, `admin_id`, `cat_id`, `sub_cat_id`, `batch_name`, `start_date`, `end_date`, `start_time`, `end_time`, `batch_type`, `batch_price`, `batch_offer_price`, `description`, `batch_image`, `no_of_student`, `status`, `pay_mode`) VALUES
(8, 1, 2, 20, 'Physics Olympiad', '2021-08-24', '2026-12-31', '14:00:00', '16:00:00', 2, '7000', '700', 'Cras pretium faucibus turpis, ut aliquet odio congue pharetra. Sed id ante porta, viverra nibh sit amet, tristique nibh. Morbi at congue ante. Integer quis varius sapien. Phasellus vitae tempus mi, a auctor magna. Integer euismod a tortor quis pharetra. In nec risus non nisi porttitor tincidunt. Aenean luctus sem mattis laoreet iaculis. Pellentesque cursus mauris vel augue mattis fermentum.', 'learning_2206211058041_220906161736.png', 0, 1, 'Online'),
(5, 1, 6, 16, 'Math Olympiad by Kamath ', '2021-08-18', '2026-12-31', '16:00:00', '19:00:00', 2, '100', '50', 'Cras pretium faucibus turpis, ut aliquet odio congue pharetra. Sed id ante porta, viverra nibh sit amet, tristique nibh. Morbi at congue ante. Integer quis varius sapien', 'offline_pay_adminn_220621105829.png', -1, 1, 'Online'),
(9, 1, 2, 20, 'Chemistry Olympiad', '2021-08-26', '2024-12-31', '16:00:00', '19:00:00', 1, '', '', 'Cras pretium faucibus turpis, ut aliquet odio congue pharetra. Sed id ante porta, viverra nibh sit amet, tristique nibh. Morbi at congue ante. Integer quis varius sapien. Phasellus vitae tempus mi, a auctor magna. Integer euismod a tortor quis pharetra. In nec risus non nisi porttitor tincidunt. Aenean luctus sem mattis laoreet iaculis. Pellentesque cursus mauris vel augue mattis fermentum.', '2023-03-06-Features-Ivanna_230418175950.jpg', 7, 1, 'Online'),
(24, 1, 6, 16, 'Competition 2021', '2021-10-04', '2025-03-26', '12:00:00', '13:00:00', 2, '1000', '800', 'Cras pretium faucibus turpis, ut aliquet odio congue pharetra. Sed id ante porta, viverra nibh sit amet, tristique nibh. Morbi at congue ante. Integer quis varius sapien. Phasellus vitae tempus mi, a auctor magna. Integer euismod a tortor quis pharetra. In nec risus non nisi porttitor tincidunt. Aenean luctus sem mattis laoreet iaculis. Pellentesque cursus mauris vel augue mattis fermentum.', 'student_(2)_230303110600.png', 4, 1, 'offline'),
(25, 13, 7, 21, 'testing', '2022-05-02', '2022-05-31', '13:00:00', '14:00:00', 1, '', '', 'cdcds', '', 1, 0, 'Online'),
(29, 13, 7, 21, 'Hindi Grammar', '2022-05-02', '2022-05-31', '13:00:00', '14:00:00', 1, '', '', 'cdcds', '', 1, 0, 'Online'),
(34, 16, 8, 23, 'dddddede', '2023-03-10', '2023-03-24', '00:00:00', '02:00:00', 1, '', '', 'ddddasdas', 'brandsection_230301151712.jpg', 1, 0, 'Online');

-- --------------------------------------------------------

--
-- Table structure for table `batch_category`
--

CREATE TABLE `batch_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batch_category`
--

INSERT INTO `batch_category` (`id`, `name`, `slug`, `status`, `time`, `admin_id`) VALUES
(2, 'Olympiad', 'engineering', 1, '2022-05-03 09:01:03', 1),
(4, 'Development', 'it', 1, '2022-05-03 09:01:03', 1),
(6, 'Arts', 'the_arts', 1, '2022-05-03 09:01:03', 1),
(7, 'softwear', 'softwear', 1, '2023-04-25 04:27:42', 1);

-- --------------------------------------------------------

--
-- Table structure for table `batch_fecherd`
--

CREATE TABLE `batch_fecherd` (
  `id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `batch_specification_heading` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `batch_fecherd` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `batch_fecherd`
--

INSERT INTO `batch_fecherd` (`id`, `batch_id`, `batch_specification_heading`, `batch_fecherd`) VALUES
(1, 1, 'What will I learn?', '[\"You will be able to solve complex problem easily.\",\"Feature 2\",\"Feature 3\"]'),
(3, 2, 'ddgf', '[\"gfhgffdgh\",\"fdghgfhjkhkj\"]'),
(4, 2, '', '[\"Hello testing 1\"]'),
(5, 3, '', '[\"\"]'),
(6, 4, '', '[\"\"]'),
(7, 5, 'What will I learn?', '[\"You will be able to solve complex problem easily.\",\"You will be able to solve complex problem easily.\",\"You will be able to solve complex problem easily.\",\"You will be able to solve complex problem easily.\",\"You will be able to solve complex problem easily.\",\"You will be able to solve complex problem easily.\"]'),
(9, 6, 'What will I learn?', '[\"You will be able to solve complex problem easily.\",\"Understand concepts of networking.\",\"Feature 3\"]'),
(10, 6, 'What will I get?', '[\"Books\",\"Practice Kit\",\"Live support\"]'),
(11, 7, 'What will I learn?', '[\"You will be able to solve complex problem easily.\",\"Features 2\",\"Features 3\"]'),
(12, 7, 'What will I get?', '[\"10000 + questions\",\"100+ test papers\",\"Video Lectures for all topic\"]'),
(13, 8, 'What will I learn?', '[\"What will I learn?\"]'),
(15, 9, 'What will I learn?', '[\"You will be able to solve complex problem easily.\",\"Features 2\",\"Features 3\"]'),
(16, 9, 'What will I get?', '[\"10000+ Questions\",\"100+ Papers\",\"Live Classes\",\"Video Lectures\"]'),
(17, 10, 'What will I learn?', '[\"You will be able to solve complex problem easily.\",\"Feature 2\",\"Feature 3\"]'),
(18, 10, 'What will I get?', '[\"1000+ questions\",\"500+ papers\",\"Study Material\"]'),
(19, 11, '', '[\"\"]'),
(20, 12, 'What will I learn?', '[\"You will be able to solve complex problems easily.\",\"You will be able to solve complex problems easily.\",\"You will be able to solve complex problems easily.\"]'),
(21, 13, '', '[\"\"]'),
(22, 14, 'What will I learn?', '[\"You will be able to solve tricky problems easily.\",\"You will be able to solve tricky problems easily.\"]'),
(23, 15, 'What will I learn?', '[\"You will be able to solve complex problem easily.\",\"You will be able to solve complex problem easily.\"]'),
(24, 15, 'What will I get?', '[\"10000+ questions\",\"100+ Sample papers\",\"Weekly live classes\"]'),
(25, 16, 'What will I learn?', '[\"You will be able to solve complex problem easily.\",\"You will be able to solve complex problem easily.\"]'),
(26, 16, 'What will I get?', '[\"1000+ Question papers\",\"100+ Test series\",\"Daily live classes\"]'),
(27, 17, 'What will I learn?', '[\"You will be able to solve complex problem easily.\",\"You will be able to solve complex problem easily.\"]'),
(28, 17, 'What will I get?', '[\"1000+ Question papers\",\"1000+ Solved papers\",\"Live Classes\"]'),
(29, 18, 'What will I learn?', '[\"You will be able to solve complex problem easily.\",\"You will be able to solve complex problem easily.\"]'),
(30, 18, 'What will I get?', '[\"Video Tutorial\",\"100+ Series of programs\",\"10+ Programs for your portfolio \",\"Certificate after course completion\"]'),
(31, 14, 'What will I get?', '[\"Live classes\",\"24*7 Support\",\"5 Projects for portfolio \",\"Certificate after completion\"]'),
(32, 12, 'What will I get?', '[\"Live classes\",\"5 Projects for your portfolio\",\"Certificate after completions\",\"24*7 Support\",\"Life time access\"]'),
(33, 19, 'What will I learn?', '[\"You will be able to solve complex problem easily.\",\"You will be able to solve complex problem easily.\",\"You will be able to solve complex problem easily.\"]'),
(34, 19, 'What will I get?', '[\"10 Programs to showcase in your portfolio \",\"Certificate after completion\",\"24*7 Support\",\"Live classes\"]'),
(35, 20, '', '[\"\"]'),
(36, 21, 'What will I learn?', '[\"Test 1\",\"Test 2\"]'),
(37, 22, 'test', '[\"test\"]'),
(38, 23, '', '[\"\"]'),
(39, 24, 'New Fetures', '[\"10 Programs to showcase in your portfolio \",\"Certificate after completion\",\"24*7 Support\",\"Live classes\"]'),
(40, 25, '', '[\"\"]'),
(41, 26, '', '[\"\"]'),
(42, 28, 'test', '[\"10 Programs to showcase in your portfolio \",\"Certificate after completion\",\"24*7 Support\",\"Live classes\"]'),
(43, 30, '', '[\"\"]'),
(44, 31, '', '[\"\"]'),
(45, 32, '', '[\"\"]'),
(46, 33, 'cdccsdcdcddsc', '[\"fvfdvdfv\",\"fvdvdefvdvfdvdv\"]'),
(47, 34, '', '[\"\"]'),
(52, 33, 'referr43rdf34r', '[\"fverfer321v34v32vdf2vfdv dv f vd fd\",\"frefrefrefefcrfre\",\"freferfrefrefrrere\",\"ferefreerfcrfccerfre\"]'),
(53, 35, 'bgfbfg', '[\"bgfbgfbfgbfg\"]'),
(55, 5, '', '[\"\"]'),
(56, 36, '', '[\"\"]');

-- --------------------------------------------------------

--
-- Table structure for table `batch_subcategory`
--

CREATE TABLE `batch_subcategory` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batch_subcategory`
--

INSERT INTO `batch_subcategory` (`id`, `cat_id`, `name`, `slug`, `status`, `time`, `admin_id`) VALUES
(18, 4, 'Java', 'java', 1, '2022-05-03 09:01:03', 1),
(19, 4, 'Python', 'python', 1, '2022-05-03 09:01:03', 1),
(20, 2, 'Maths', 'maths', 1, '2022-05-03 09:01:03', 1),
(21, 3, 'mobile', 'mobile', 1, '2022-11-15 12:49:36', 1),
(22, 9, 'xcdcdscs', 'xcdcdscs', 1, '2023-02-06 09:26:19', 1),
(23, 7, 'devloper', 'devloper', 1, '2023-04-25 04:28:53', 1);

-- --------------------------------------------------------

--
-- Table structure for table `batch_subjects`
--

CREATE TABLE `batch_subjects` (
  `id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `chapter` varchar(500) NOT NULL,
  `sub_start_date` date NOT NULL,
  `sub_end_date` date NOT NULL,
  `sub_start_time` time NOT NULL,
  `sub_end_time` time NOT NULL,
  `chapter_status` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'id of completed chapter',
  `chapter_complt_date` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `total_chapter_complt_date` datetime NOT NULL,
  `added_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batch_subjects`
--

INSERT INTO `batch_subjects` (`id`, `batch_id`, `teacher_id`, `subject_id`, `chapter`, `sub_start_date`, `sub_end_date`, `sub_start_time`, `sub_end_time`, `chapter_status`, `chapter_complt_date`, `total_chapter_complt_date`, `added_on`) VALUES
(134, 15, 8, 1, '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\"]', '2021-09-16', '2021-12-31', '14:00:00', '16:00:00', '', '', '0000-00-00 00:00:00', '2021-10-02 14:39:42'),
(135, 15, 11, 13, '[\"74\"]', '2021-10-08', '2021-10-29', '14:00:00', '16:00:00', '', '', '0000-00-00 00:00:00', '2021-10-02 14:39:42'),
(251, 9, 9, 3, '[\"79\",\"80\",\"81\"]', '2021-08-26', '2021-12-31', '16:00:00', '19:00:00', '', '', '0000-00-00 00:00:00', '2023-04-18 18:25:37'),
(116, 10, 10, 7, '[\"46\",\"47\",\"48\"]', '2021-08-25', '2021-12-31', '17:00:00', '20:00:00', '', '', '0000-00-00 00:00:00', '2021-09-28 15:03:01'),
(117, 10, 8, 1, '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\"]', '2021-08-25', '2021-12-31', '17:00:00', '20:00:00', '', '', '0000-00-00 00:00:00', '2021-09-28 15:03:01'),
(235, 5, 29, 1, '[\"84\",\"85\"]', '2021-08-18', '2021-12-31', '16:00:00', '19:00:00', '', '', '0000-00-00 00:00:00', '2023-03-10 18:22:58'),
(229, 8, 8, 2, '[\"83\"]', '2021-08-24', '2021-12-31', '14:00:00', '16:00:00', '', '', '0000-00-00 00:00:00', '2023-03-06 17:33:07'),
(132, 14, 11, 12, '[\"51\",\"52\",\"53\",\"54\",\"55\",\"56\",\"57\",\"58\",\"59\"]', '2021-09-16', '2021-09-30', '17:00:00', '06:00:00', '', '', '0000-00-00 00:00:00', '2021-10-02 11:43:39'),
(122, 16, 8, 1, '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\"]', '2021-09-16', '2021-11-30', '12:00:00', '14:00:00', '', '', '0000-00-00 00:00:00', '2021-09-28 15:05:22'),
(121, 17, 8, 1, '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\"]', '2021-09-16', '2021-11-30', '14:00:00', '16:00:00', '', '', '0000-00-00 00:00:00', '2021-09-28 15:05:09'),
(114, 18, 11, 12, '[\"51\",\"52\",\"53\",\"54\",\"55\",\"56\",\"57\",\"58\",\"59\"]', '2021-09-16', '2021-12-31', '15:00:00', '17:00:00', '[\"51\"]', '{\"51\":\"29-09-2021\"}', '0000-00-00 00:00:00', '2021-09-28 15:02:22'),
(133, 12, 11, 11, '[\"60\",\"61\",\"62\",\"63\",\"64\",\"65\",\"66\",\"67\",\"68\"]', '2021-09-22', '2021-09-21', '15:00:00', '16:00:00', '', '', '0000-00-00 00:00:00', '2021-10-02 11:43:54'),
(113, 19, 11, 13, '[\"69\",\"70\",\"71\",\"72\",\"73\",\"74\",\"75\",\"76\"]', '2021-09-16', '2021-12-31', '16:00:00', '18:00:00', '', '', '0000-00-00 00:00:00', '2021-09-28 15:02:09'),
(125, 20, 11, 11, '[\"65\"]', '2021-09-16', '2021-12-31', '12:00:00', '00:00:00', '', '', '0000-00-00 00:00:00', '2021-09-28 15:06:00'),
(241, 21, 18, 1, '[\"85\"]', '2021-10-22', '2021-10-31', '15:00:00', '20:00:00', '', '', '0000-00-00 00:00:00', '2023-04-08 16:05:28'),
(136, 22, 11, 13, '[\"72\",\"74\",\"75\"]', '2021-09-04', '2021-09-04', '18:00:00', '20:00:00', '', '', '0000-00-00 00:00:00', '2021-10-02 16:54:03'),
(161, 25, 14, 16, '[\"87\"]', '2022-05-03', '2022-05-26', '13:00:00', '14:00:00', '', '', '0000-00-00 00:00:00', '2022-05-04 18:07:22'),
(162, 25, 14, 15, '[\"86\"]', '2022-05-04', '2022-05-31', '13:00:00', '14:00:00', '', '', '0000-00-00 00:00:00', '2022-05-04 18:07:22'),
(245, 24, 29, 3, '[\"81\"]', '2021-10-04', '2021-10-30', '12:00:00', '13:00:00', '', '', '0000-00-00 00:00:00', '2023-04-08 17:14:27'),
(244, 24, 18, 3, '[\"80\"]', '2022-06-23', '2022-06-30', '13:00:00', '14:00:00', '', '', '0000-00-00 00:00:00', '2023-04-08 17:14:27'),
(243, 24, 28, 1, '[\"84\"]', '2022-06-23', '2022-06-24', '15:00:00', '16:00:00', '', '', '0000-00-00 00:00:00', '2023-04-08 17:14:27'),
(190, 30, 29, 1, '[\"85\",\"84\"]', '2022-12-08', '2022-12-31', '12:00:00', '02:00:00', '', '', '0000-00-00 00:00:00', '2022-12-08 11:31:24'),
(191, 31, 28, 1, '[\"85\",\"84\"]', '2022-12-09', '2022-12-25', '10:00:00', '15:00:00', '', '', '0000-00-00 00:00:00', '2022-12-08 14:03:36'),
(203, 32, 29, 3, '[\"80\"]', '2023-02-14', '2023-02-14', '13:00:00', '15:00:00', '', '', '0000-00-00 00:00:00', '2023-02-13 18:20:29'),
(202, 32, 30, 7, '[\"48\"]', '2023-02-14', '2023-02-24', '13:00:00', '14:00:00', '', '', '0000-00-00 00:00:00', '2023-02-13 18:20:29'),
(204, 32, 29, 3, '[\"79\"]', '2023-02-14', '2023-02-22', '13:00:00', '15:00:00', '', '', '0000-00-00 00:00:00', '2023-02-13 18:20:29'),
(206, 34, 17, 17, '[\"88\"]', '2023-03-17', '2023-03-24', '00:00:00', '02:00:00', '', '', '0000-00-00 00:00:00', '2023-03-01 15:17:12');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `title` text CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `image` text CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `admin_id` int(11) NOT NULL,
  `added_by` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `title`, `image`, `description`, `admin_id`, `added_by`, `status`, `create_at`) VALUES
(1, 'How to manage Time?', 'students_230302151511.png', '<div class=\"lorem\"> <h2>Lorem ipsum text Here</h2><p>It is a long-established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of lett</p></div>', 1, '', 1, '2021-09-20 18:11:57'),
(2, 'Is English is easy?', 'students_230302151447.png', '<div class=\"lorem\"> <h2>Lorem ipsum text Here</h2><p>It is a long-established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of lett</p></div>', 1, '', 1, '2021-09-21 12:58:45');

-- --------------------------------------------------------

--
-- Table structure for table `blog_comments`
--

CREATE TABLE `blog_comments` (
  `id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `admin_id` int(100) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `user_role` varchar(11) NOT NULL,
  `user_name` text CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `user_email` text CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `user_mobile` text CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `user_image` varchar(100) NOT NULL,
  `comments` text CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `status` int(11) NOT NULL COMMENT '0 = painding ,1 =complete , 2 = decline',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `blog_comments_reply`
--

CREATE TABLE `blog_comments_reply` (
  `id` int(11) NOT NULL,
  `comment_id` varchar(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `name` text CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `email` text CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `reply` text CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `image` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `book_pdf`
--

CREATE TABLE `book_pdf` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `batch` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `topic` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_pdf`
--

INSERT INTO `book_pdf` (`id`, `admin_id`, `title`, `batch`, `topic`, `subject`, `file_name`, `status`, `added_by`, `added_at`) VALUES
(1, 1, 'test', '[\"9\"]', '', '3', 'sample220321231833230427160212.pdf', 1, 1, '2023-04-27 16:02:12');

-- --------------------------------------------------------

--
-- Table structure for table `certificate`
--

CREATE TABLE `certificate` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `added_id` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `certificate`
--

INSERT INTO `certificate` (`id`, `student_id`, `batch_id`, `added_id`, `date`) VALUES
(3, 1, 9, 1, '2023-04-28');

-- --------------------------------------------------------

--
-- Table structure for table `certificate_setting`
--

CREATE TABLE `certificate_setting` (
  `id` int(11) NOT NULL,
  `heading` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sub_heading` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `certificate_logo` varchar(500) NOT NULL,
  `signature_image` varchar(500) NOT NULL,
  `template_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `certificate_setting`
--

INSERT INTO `certificate_setting` (`id`, `heading`, `sub_heading`, `title`, `description`, `certificate_logo`, `signature_image`, `template_id`) VALUES
(1, 'Certificate', 'Certificate of Completion', 'Program', 'has successfully completed the Academyeo course on {batch} via attending classes, completing assignments and passing quiz or mini project. We wish our student all the best for future endeavors.', 'favicon10.png', 'signature2.png', 3);

-- --------------------------------------------------------

--
-- Table structure for table `chapters`
--

CREATE TABLE `chapters` (
  `id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `chapter_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `no_of_questions` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chapters`
--

INSERT INTO `chapters` (`id`, `subject_id`, `chapter_name`, `status`, `no_of_questions`) VALUES
(38, 8, 'Guptas Vakatakas and Vardhanas', 1, 0),
(42, 9, 'International Events', 1, 0),
(43, 9, 'India and its neighbouring countries', 1, 0),
(44, 9, 'Chronology of Medieval India and their important systems', 1, 0),
(45, 9, 'Name of the Kings who built important ancient Temples and Institutions like Nalanda', 1, 0),
(46, 7, 'Verbs', 1, 2),
(47, 7, 'Grammar', 1, 0),
(48, 7, 'Article', 1, 1),
(49, 10, 'test 1', 1, 0),
(50, 10, 'test 2', 1, 0),
(51, 12, 'Basics', 1, 0),
(52, 12, 'Variable Declaration', 1, 0),
(53, 12, 'Definition and Scope', 1, 0),
(54, 12, 'Data Types', 1, 0),
(55, 12, 'Storage Classes', 1, 0),
(56, 12, 'Input and Output', 1, 2),
(57, 12, 'Operators', 1, 0),
(58, 12, 'Preprocessor', 1, 0),
(59, 12, 'Arrays and String', 1, 0),
(60, 11, 'Basics', 1, 0),
(61, 11, 'Variable Declaration', 1, 0),
(62, 11, 'Definition and Scope', 1, 0),
(63, 11, 'Data Types', 1, 0),
(64, 11, 'Storage Classes', 1, 0),
(65, 11, 'Input and Output', 1, 0),
(66, 11, 'Operators', 1, 0),
(67, 11, 'Preprocessor', 1, 0),
(68, 11, 'Arrays and String', 1, 0),
(69, 13, 'Variable Declaration', 1, 0),
(70, 13, 'Definition and Scope', 1, 0),
(71, 13, 'Data Types', 1, 0),
(72, 13, 'Storage Classes', 1, 0),
(73, 13, 'Input and Output', 1, 0),
(74, 13, 'Operators', 1, 0),
(75, 13, 'Preprocessor', 1, 0),
(76, 13, 'Arrays and String', 1, 0),
(79, 3, 'Inorganic', 1, 9),
(80, 3, 'Organic', 1, 14),
(81, 3, 'Periodic table', 1, 1),
(82, 2, 'Atoms', 1, 15),
(83, 2, 'Molecules', 1, 1),
(84, 1, 'Alegebra', 1, 9),
(85, 1, 'Addition', 1, 3),
(86, 15, 'addition', 1, 5),
(87, 16, 'dfgdfgd', 1, 0),
(88, 17, 'fdbvnh', 1, 1),
(93, 19, 'cdccscd', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `course_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admin_id` int(11) NOT NULL,
  `course_duration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class_size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time_duration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `enquiry`
--

CREATE TABLE `enquiry` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `message` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enquiry`
--

INSERT INTO `enquiry` (`id`, `name`, `mobile`, `email`, `subject`, `message`, `date`) VALUES
(1, 'Dillon Watkins', '744747474774', 'biregyduwa@mailinator.com', 'Alias amet culpa o', 'Quae sunt in incidi', '2023-04-27');

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

CREATE TABLE `exams` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL COMMENT '1 - mock, 2 - practice',
  `format` int(11) NOT NULL COMMENT '1 - Shuffle, 2 - Fix',
  `batch_id` int(11) NOT NULL,
  `total_question` varchar(255) NOT NULL,
  `time_duration` varchar(255) NOT NULL COMMENT 'In Minute Only',
  `question_ids` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mock_sheduled_date` date NOT NULL,
  `mock_sheduled_time` time NOT NULL,
  `total_marks` text NOT NULL,
  `marking_parcent` varchar(250) NOT NULL DEFAULT '0.25',
  `status` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`id`, `admin_id`, `name`, `type`, `format`, `batch_id`, `total_question`, `time_duration`, `question_ids`, `mock_sheduled_date`, `mock_sheduled_time`, `total_marks`, `marking_parcent`, `status`, `added_by`, `added_at`) VALUES
(1, 1, 'testing', 1, 1, 9, '5', '5', '[\"5\",\"4\",\"3\",\"2\",\"1\"]', '2023-04-27', '15:49:00', '5', '0.25', 1, 1, '2023-04-27 15:49:22'),
(2, 1, 'demo', 2, 1, 9, '5', '2', '[\"5\",\"4\",\"3\",\"2\",\"1\"]', '0000-00-00', '00:00:00', '5', '0.25', 1, 1, '2023-04-27 16:04:34'),
(3, 1, 'Testing Mock', 1, 1, 9, '5', '12', '[\"5\",\"4\",\"3\",\"2\",\"1\"]', '2023-04-29', '11:50:00', '5', '0', 1, 1, '2023-04-29 11:45:34'),
(4, 1, 'testing', 1, 1, 9, '5', '20', '[\"5\",\"4\",\"3\",\"2\",\"1\"]', '2023-04-29', '12:00:00', '5', '0', 1, 1, '2023-04-29 11:51:48'),
(5, 1, 'testing', 1, 1, 9, '5', '20', '[\"5\",\"4\",\"3\",\"2\",\"1\"]', '2023-04-29', '16:00:00', '5', '0.25', 1, 1, '2023-04-29 12:22:33'),
(6, 1, 'Inorganic Organic quizes', 2, 1, 9, '5', '20', '[\"5\",\"4\",\"3\",\"2\",\"1\"]', '0000-00-00', '00:00:00', '5', '0', 1, 1, '2023-04-29 14:32:42'),
(7, 1, 'Testing Quizes', 1, 1, 9, '5', '20', '[\"5\",\"4\",\"3\",\"2\",\"1\"]', '2023-06-03', '16:00:00', '5', '0.25', 1, 1, '2023-04-29 14:42:37');

-- --------------------------------------------------------

--
-- Table structure for table `extra_classes`
--

CREATE TABLE `extra_classes` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) NOT NULL,
  `batch_id` varchar(500) NOT NULL,
  `added_at` datetime NOT NULL,
  `completed_date_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `extra_classes`
--

INSERT INTO `extra_classes` (`id`, `admin_id`, `date`, `start_time`, `end_time`, `teacher_id`, `description`, `status`, `batch_id`, `added_at`, `completed_date_time`) VALUES
(1, 1, '2023-04-27', '06:30:00', '07:35:00', 33, 'Hello ', 'Incomplete', '[\"24\"]', '2023-04-27 15:57:44', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `extra_class_attendance`
--

CREATE TABLE `extra_class_attendance` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `added_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE `facilities` (
  `id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facilities`
--

INSERT INTO `facilities` (`id`, `title`, `icon`, `description`, `status`) VALUES
(1, 'Study Material', 'icofont-address-book', 'We Provide Top Class Study Material to our students.', 1),
(2, 'Test Series', 'icofont-notepad', 'We provide 100+ test series for all the courses.', 1),
(3, 'Certificate', 'icofont-certificate', 'We provide certificates after completing the course successfully.', 1),
(4, 'Class Rooms', 'icofont-building', 'Fully furnished classrooms.', 1),
(5, '24 hrs. Expert Guidance', 'icofont-teacher', 'We provide around-the-clock support to our students.', 1),
(6, 'brainstorming', 'icofont-brainstorming', 'Brainstorming session Every week. ', 0),
(7, 'Ut dolor reiciendis ', 'icofont-brainstorming', 'Minim id rerum omni', 1);

-- --------------------------------------------------------

--
-- Table structure for table `frontend_details`
--

CREATE TABLE `frontend_details` (
  `id` int(11) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `youtube` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  `linkedin` varchar(255) NOT NULL,
  `map_api` varchar(255) NOT NULL,
  `slider_details` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cont_heading` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cont_sub_heading` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cont_form_heading` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `faci_heading` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `faci_sub_heading` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `frst_crse_heading` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `frst_crse_sub_heading` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `frst_crse_desc` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sec_crse_heading` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sec_crse_sub_heading` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `abt_frst_heading` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `abt_frst_sub_heading` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `abt_year` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `abt_frst_desc` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `abt_frst_img` varchar(255) NOT NULL,
  `abt_sec_img` varchar(255) NOT NULL,
  `abt_sec_heading` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `abt_sec_desc` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `abt_secbtn_text` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `abt_secbtn_url` varchar(255) NOT NULL,
  `abt_thrd_heading` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `abt_thrd_sub_heading` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `abt_thrd_desc` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `abt_thrd_img` varchar(255) NOT NULL,
  `total_toppers` int(11) NOT NULL,
  `trusted_students` int(11) NOT NULL,
  `years_of_histry` int(11) NOT NULL,
  `testimonial` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `testi_heading` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `testi_subheading` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `selectn_heading` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `selectn_subheading` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `selection` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `teacher_heading` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `teacher_subheading` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `no_of_teacher` int(11) NOT NULL,
  `header_btn_txt` varchar(255) NOT NULL,
  `header_btn_url` varchar(255) NOT NULL,
  `client_imgs` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `frontend_details`
--

INSERT INTO `frontend_details` (`id`, `mobile`, `email`, `address`, `facebook`, `youtube`, `twitter`, `instagram`, `linkedin`, `map_api`, `slider_details`, `cont_heading`, `cont_sub_heading`, `cont_form_heading`, `faci_heading`, `faci_sub_heading`, `frst_crse_heading`, `frst_crse_sub_heading`, `frst_crse_desc`, `sec_crse_heading`, `sec_crse_sub_heading`, `abt_frst_heading`, `abt_frst_sub_heading`, `abt_year`, `abt_frst_desc`, `abt_frst_img`, `abt_sec_img`, `abt_sec_heading`, `abt_sec_desc`, `abt_secbtn_text`, `abt_secbtn_url`, `abt_thrd_heading`, `abt_thrd_sub_heading`, `abt_thrd_desc`, `abt_thrd_img`, `total_toppers`, `trusted_students`, `years_of_histry`, `testimonial`, `testi_heading`, `testi_subheading`, `selectn_heading`, `selectn_subheading`, `selection`, `teacher_heading`, `teacher_subheading`, `no_of_teacher`, `header_btn_txt`, `header_btn_url`, `client_imgs`) VALUES
(1, '1234567890', 'ravi.kushwah@pixelnx.com', '04 A Agroha Nagar, Dewas, Madhya Pradesh', 'https://www.facebook.com', 'https://www.youtube.com', 'https://www.twitter.com', 'https://www.instagram.com', 'https://www.linkedin.com', '', '{\"slider_heading\":[\"Choose Best For Your Education\",\"Choose Best For Your Education\",\"Choose Best For Your Education\"],\"slider_subheading\":[\"Welcome to E-Academy\",\"Welcome to E-Academy\",\"Welcome to E-Academy\"],\"slider_desc\":[\"\",\"\",\"\"],\"slider_img\":[\"slider3.png\",\"slider1.png\",\"slider2.png\"]}', 'Contact Us For You Query', 'START WITH US', 'Send Us A Message', 'Our Facilities are ', 'Our Facilities ', 'Online Learning Plateform', 'WE ARE E - ACADEMY', '', 'Our Syllabus', 'WE ENHANCE YOUR TALENT', 'Why Choose Us', 'ABOUT E-ACADEMY', '1997', 'Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore eesdoeit dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation and in ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum<br /><br />Excepteur sint occaecat cupidatat noesn proident sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut peerspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantiuws totam rem aperiam, eaque ipsa quae.Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore eesdoeit dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.', 'about_img1.png', 'about_img2.png', 'We Take Care Of Your Careers Do Not Worry', 'We Are Very Cost Friendly And We Would Love To Be A Part Of Your Home Happiness For A Long Lorem Ipsum Dolor Sit Amet, Consectetur Adipisicing Elit Sed Eiusmod.', 'Contact Us', 'http://kamleshyadav.in/e-academy/contact-us', 'Why Choose Us From Thousands', 'OUR MISSION', 'Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore eesdoeit dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation and in ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum.<br /><br />Excepteur sint occaecat cupidatat noesn proident sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut peerspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantiuws totam rem aperiam, eaque ipsa quae.', 'vission_img.png', 654, 200, 50, '{\"25\":\"Consectetur adipiscing elit, sed do eiusmod tempor incididunt uerset labore et dolore magna aliqua. Qesuis ipsum esuspendisse ultriceies gravida Risus commodo viverra andes aecenas accumsan lacus vel facilisis. \",\"20\":\"Consectetur adipiscing elit, sed do eiusmod tempor incididunt uerset labore et dolore magna aliqua. Qesuis ipsum esuspendisse ultriceies gravida Risus commodo viverra andes aecenas accumsan lacus vel facilisis. \"}', 'What Student Says', 'E- ACADEMY REVIEWS', 'Minima odio quasi to', 'Mollit illum illo e', '{\"24\":\"Nesciunt earum recu\",\"22\":\"cdcsd\",\"4\":\"Quia harum vitae ab \",\"18\":\"Ex velit perferendis\",\"26\":\"Labore quidem evenie\",\"11\":\"Rerum enim aut et hi\"}', 'Qualified Teacher', 'OUR EXPERTS', 6, '', '', '[\"01.png\",\"02.png\",\"03.png\",\"04.png\",\"05.png\",\"06.png\"]');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) NOT NULL,
  `upload` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `video_url` varchar(255) NOT NULL,
  `video` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `title`, `type`, `upload`, `image`, `video_url`, `video`, `status`, `admin_id`) VALUES
(46, 'Python', 'Image', 'File', 'Python.jpg', '', '', 1, 1),
(47, 'Cartoon', 'Image', 'File', 'faq.jpg', '', 'sample-mp4-file.mp4', 1, 1),
(48, 'Rerum vitae a aut la', 'Video', 'File', '', '', 'Teacher_ID_20_CL_1_SUB_ID_56_Topic_ID_330_Lang_English_Tut_1_2022-06-29_1656493987_(2).mp4', 1, 1),
(49, 'Cillum ea eveniet s', 'Video', 'File', '', '', 'big_buck_bunny_720p_1mb2.mp4', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE `general_settings` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `key_text` text NOT NULL,
  `velue_text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `general_settings`
--

INSERT INTO `general_settings` (`id`, `title`, `key_text`, `velue_text`) VALUES
(1, 'payment gateway type select 1 = razorpay, 2 paypal', 'payment_type', '2'),
(2, 'razorpay key id ', 'razorpay_key_id', 'rzp_test_xMfu0QOlZb2v2h'),
(3, 'razorpay secret key', 'razorpay_secret_key', 'KUxxEE0MLRddMcRYpUzZCEtS'),
(4, 'paypal client id', 'paypal_client_id', 'AT0JTb6aq7-C0HjS9eIjnJW398NLu_4UHrCg2jTycrA75ipFvMklMNdQtHhBkZyrLfFHFrpR4t76_WUk'),
(5, 'paypal secret key', 'paypal_secret_key', ''),
(6, 'select language type ', 'language_name', 'english'),
(7, 'select currency code', 'currency_code', 'USD'),
(8, 'select currency decimal code', 'currency_decimal_code', '$'),
(9, 'currency converter api', 'currency_converter_api', ''),
(10, 'send mails SMTP ', 'smtp_mail', 'support@vigoredu.in'),
(11, 'smtp password mail', 'smtp_pwd', 'support@7483'),
(12, 'smtp server type mail', 'server_type', 'server_mail'),
(13, 'smtp host mail', 'smtp_host', 'vigoredu.in'),
(14, 'smtp host mails', 'smtp_port', '465'),
(15, 'smtp smtp encryption', 'smtp_encryption', 'ssl'),
(16, 'sandbox accounts', 'sandbox_accounts', 'sb-fy6ad22118441@business.example.com'),
(17, 'Firebase Accounts', 'firebase_key', 'AAAAFU0Nyks:APA91bFWu1zpzRasM60cqJjMvfcL5Uc667MP38b5CaYd5O3g-ioRYGtVSvBCdFUt5ea4H8eIDbPKNs98z5W0RxFfRsswy07p1EbSKRRlQkUA1b9sb_fBC2sHvFJZWhpILlZlOqz0_M4u');

-- --------------------------------------------------------

--
-- Table structure for table `homeworks`
--

CREATE TABLE `homeworks` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `subject_id` int(11) NOT NULL,
  `batch_id` text NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `added_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jetsi_setting`
--

CREATE TABLE `jetsi_setting` (
  `id` int(11) NOT NULL,
  `batch` int(11) NOT NULL,
  `jetsi_api_key` varchar(500) NOT NULL,
  `jetsi_api_secret` varchar(500) NOT NULL,
  `meeting_number` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `added_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jetsi_setting`
--

INSERT INTO `jetsi_setting` (`id`, `batch`, `jetsi_api_key`, `jetsi_api_secret`, `meeting_number`, `password`, `status`, `admin_id`, `added_at`) VALUES
(1, 24, '', '', 'mid=2580360210', '123', 1, 1, '2023-04-26 18:36:31'),
(2, 9, '', '', 'mid=2613144937', '123', 1, 1, '2023-04-27 12:47:54'),
(3, 21, '', '', 'mid=6609147142', '12345', 1, 1, '2023-04-27 15:31:07');

-- --------------------------------------------------------

--
-- Table structure for table `leave_management`
--

CREATE TABLE `leave_management` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `subject` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `leave_msg` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `total_days` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `added_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leave_management`
--

INSERT INTO `leave_management` (`id`, `teacher_id`, `student_id`, `admin_id`, `batch_id`, `subject`, `leave_msg`, `from_date`, `to_date`, `total_days`, `status`, `added_at`) VALUES
(1, 0, 2, 1, 0, 'yyyy', 'ggh&', '2023-04-27', '2023-04-27', 0, 0, '2023-04-27 10:08:23'),
(2, 18, 0, 1, 24, 'cdscsdc', 'Hello Sir, cdscdscdscdsc', '2023-04-28', '2023-04-29', 1, 0, '2023-04-27 10:29:28');

-- --------------------------------------------------------

--
-- Table structure for table `live_class_history`
--

CREATE TABLE `live_class_history` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `start_time` varchar(500) NOT NULL,
  `end_time` varchar(500) NOT NULL,
  `date` date NOT NULL,
  `entry_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `admin_id` int(11) NOT NULL,
  `type_class` tinyint(11) DEFAULT NULL COMMENT '1=Zoom and 2=Jetsi'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `live_class_history`
--

INSERT INTO `live_class_history` (`id`, `uid`, `batch_id`, `subject_id`, `chapter_id`, `start_time`, `end_time`, `date`, `entry_date_time`, `admin_id`, `type_class`) VALUES
(1, 1, 24, 3, 80, '06:36:37 pm', '06:38:30 pm', '2023-04-26', '2023-04-26 13:08:30', 1, 2),
(3, 1, 9, 3, 79, '06:38:51 pm', '06:39:47 pm', '2023-04-26', '2023-04-26 13:09:47', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `live_class_setting`
--

CREATE TABLE `live_class_setting` (
  `id` int(11) NOT NULL,
  `batch` int(11) NOT NULL,
  `zoom_api_key` varchar(500) NOT NULL,
  `zoom_api_secret` varchar(500) NOT NULL,
  `meeting_number` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `added_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `live_class_setting`
--

INSERT INTO `live_class_setting` (`id`, `batch`, `zoom_api_key`, `zoom_api_secret`, `meeting_number`, `password`, `status`, `admin_id`, `added_at`) VALUES
(11, 9, 'xwYMnt52tXkxaxr9lxa1hNI4IxnNqHi26iFw', 'z8dZ3DgN9QeGXENLECR32NIW3HwhG4K4Kqce', '7065341117', 'Tlg3MXFnQUVGRzRyeENpaThrcXREQT09', 1, 1, '2023-02-06 22:00:10'),
(12, 32, 'pRzOjo698A1e8Z0ZVPrGkOGmKCLpdzmmJmWc', 'W3xJcDWB7ws4Qq27tUOZ3SNG8EWaqpSFDLId', '9678611838', 'wMyW40', 1, 1, '2023-02-13 18:21:50');

-- --------------------------------------------------------

--
-- Table structure for table `mock_result`
--

CREATE TABLE `mock_result` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL,
  `paper_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `start_time` time NOT NULL,
  `submit_time` time NOT NULL,
  `total_question` int(11) NOT NULL,
  `time_duration` varchar(255) NOT NULL,
  `attempted_question` int(11) NOT NULL,
  `question_answer` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `percentage` double NOT NULL,
  `added_on` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mock_result`
--

INSERT INTO `mock_result` (`id`, `admin_id`, `student_id`, `paper_id`, `paper_name`, `date`, `start_time`, `submit_time`, `total_question`, `time_duration`, `attempted_question`, `question_answer`, `percentage`, `added_on`) VALUES
(7, 1, 1, 18, 'test', '2023-04-27', '14:47:00', '14:47:00', 2, '15', 2, '{\"5\":\"A\",\"4\":\"A\"}', 40, '2023-04-27 14:47:36'),
(5, 1, 1, 10, 'inorganic paper ', '2023-04-26', '11:51:00', '11:52:16', 2, '12', 2, '{\"1\":\"D\",\"2\":\"A\"}', 37.5, '2023-04-26 11:52:28'),
(6, 1, 1, 12, 'Quize of 26-april-22', '2023-04-26', '14:15:00', '14:15:00', 5, '6', 3, '{\"2\":\"A\",\"3\":\"A\",\"5\":\"A\"}', 60, '2023-04-26 14:15:10'),
(8, 1, 2, 1, 'testing', '2023-04-27', '15:50:00', '15:51:00', 5, '5', 4, '{\"5\":\"A\",\"4\":\"A\",\"1\":\"A\",\"2\":\"A\"}', 80, '2023-04-27 15:51:52'),
(9, 1, 1, 3, 'Testing Mock', '2023-04-29', '11:52:00', '11:52:00', 5, '12', 5, '{\"1\":\"D\",\"2\":\"A\",\"3\":\"A\",\"4\":\"A\",\"5\":\"A\"}', 80, '2023-04-29 11:52:26');

-- --------------------------------------------------------

--
-- Table structure for table `notes_pdf`
--

CREATE TABLE `notes_pdf` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `batch` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `topic` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notes_pdf`
--

INSERT INTO `notes_pdf` (`id`, `admin_id`, `title`, `batch`, `topic`, `subject`, `file_name`, `status`, `added_by`, `added_at`) VALUES
(1, 1, 'hello', '[\"9\"]', '80', '3', 'sample2203212318331230220181923_(1)230220212120230427160234.pdf', 1, 1, '2023-04-27 16:02:34');

-- --------------------------------------------------------

--
-- Table structure for table `notices`
--

CREATE TABLE `notices` (
  `id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `notice_for` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `date` date NOT NULL,
  `admin_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `added_by` varchar(255) NOT NULL,
  `read_status` int(11) NOT NULL,
  `added_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notices`
--

INSERT INTO `notices` (`id`, `title`, `description`, `notice_for`, `status`, `date`, `admin_id`, `student_id`, `teacher_id`, `added_by`, `read_status`, `added_at`) VALUES
(1, 'hello', 'for testing', 'Both', 1, '2023-04-27', 1, 0, 0, '', 1, '2023-04-27 12:43:59'),
(2, 'hello', 'dc', 'Student', 1, '2023-04-27', 1, 0, 0, '', 1, '2023-04-27 12:46:13'),
(3, 'hello ', 'notice for u', 'Student', 1, '2023-04-27', 1, 0, 0, '', 1, '2023-04-27 15:29:04');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `notification_type` varchar(255) NOT NULL,
  `msg` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0',
  `time` datetime DEFAULT NULL,
  `seen_by` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `student_id`, `batch_id`, `notification_type`, `msg`, `url`, `status`, `time`, `seen_by`) VALUES
(1, 1, 9, 'Exam', 'New Mock Paper Added', 'student/mock-paper', 0, '2023-04-25 12:35:13', '0000-00-00 00:00:00'),
(2, 1, 9, 'Exam', 'New Practice Paper Added', 'student/practice-paper', 0, '2023-04-25 12:44:16', NULL),
(3, 1, 9, 'Exam', 'New Mock Paper Added', 'student/mock-paper', 0, '2023-04-25 14:07:36', '0000-00-00 00:00:00'),
(4, 1, 9, 'Exam', 'New Practice Paper Added', 'student/practice-paper', 0, '2023-04-25 14:35:51', NULL),
(5, 1, 9, 'Exam', 'New Practice Paper Added', 'student/practice-paper', 0, '2023-04-25 18:24:55', NULL),
(6, 1, 0, '', '', '', 0, NULL, NULL),
(7, 1, 9, 'Exam', 'New Practice Paper Added', 'student/practice-paper', 0, '2023-04-26 11:15:40', NULL),
(8, 1, 0, '', '', '', 0, NULL, NULL),
(9, 1, 0, '', '', '', 0, NULL, NULL),
(10, 1, 0, '', '', '', 0, NULL, NULL),
(11, 1, 9, 'Exam', 'New Mock Paper Added', 'student/mock-paper', 0, '2023-04-26 14:11:27', '0000-00-00 00:00:00'),
(12, 1, 9, 'Exam', 'New Mock Paper Added', 'student/mock-paper', 0, '2023-04-27 12:04:37', '0000-00-00 00:00:00'),
(13, 1, 0, '', '', '', 0, NULL, NULL),
(14, 1, 0, 'Notice', 'New Notice Added', 'student/notice', 0, '2023-04-27 12:43:59', '0000-00-00 00:00:00'),
(15, 1, 0, 'Notice', 'New Notice Added', 'student/notice', 0, '2023-04-27 12:46:13', '0000-00-00 00:00:00'),
(16, 1, 9, 'Exam', 'New Practice Paper Added', 'student/practice-paper', 0, '2023-04-27 12:51:51', NULL),
(17, 1, 9, 'Exam', 'New Practice Paper Added', 'student/practice-paper', 0, '2023-04-27 12:55:34', NULL),
(18, 1, 9, 'Exam', 'New Mock Paper Added', 'student/mock-paper', 0, '2023-04-27 13:00:15', '0000-00-00 00:00:00'),
(19, 1, 9, 'Exam', 'New Mock Paper Added', 'student/mock-paper', 0, '2023-04-27 14:46:22', '0000-00-00 00:00:00'),
(20, 1, 0, '', '', '', 0, NULL, NULL),
(21, 1, 0, 'Notice', 'New Notice Added', 'student/notice', 0, '2023-04-27 15:29:04', '0000-00-00 00:00:00'),
(22, 2, 0, 'Notice', 'New Notice Added', 'student/notice', 0, '2023-04-27 15:29:04', '0000-00-00 00:00:00'),
(23, 2, 21, 'Exam', 'New Practice Paper Added', 'student/practice-paper', 0, '2023-04-27 15:32:35', NULL),
(24, 2, 21, 'Exam', 'New Mock Paper Added', 'student/mock-paper', 0, '2023-04-27 15:34:12', '0000-00-00 00:00:00'),
(25, 0, 0, 'Vacancy', 'New Upcoming Exam Added', 'student/vacancy', 0, '2023-04-27 15:34:14', '0000-00-00 00:00:00'),
(26, 2, 9, 'Exam', 'New Mock Paper Added', 'student/mock-paper', 0, '2023-04-27 15:49:22', '0000-00-00 00:00:00'),
(27, 1, 24, 'Extra-class', 'New ExtraClass Added', 'student/extra-classes', 0, '2023-04-27 15:57:44', '0000-00-00 00:00:00'),
(28, 0, 0, 'Vacancy', 'New Upcoming Exam Added', 'student/vacancy', 0, '2023-04-27 15:59:58', '0000-00-00 00:00:00'),
(29, 2, 9, 'Library', 'New Book Added', 'student/book', 0, '2023-04-27 16:02:12', '0000-00-00 00:00:00'),
(30, 2, 9, 'Notes', 'New Notes Added', 'student/notes', 0, '2023-04-27 16:02:35', '0000-00-00 00:00:00'),
(31, 2, 9, 'Exam', 'New Practice Paper Added', 'student/practice-paper', 0, '2023-04-27 16:04:34', NULL),
(32, 2, 9, 'Video-Lecture', 'New Video Added', 'student/video-lecture', 0, '2023-04-27 16:10:28', '0000-00-00 00:00:00'),
(33, 1, 9, 'Exam', 'New Mock Paper Added', 'student/mock-paper', 0, '2023-04-29 11:45:34', '0000-00-00 00:00:00'),
(34, 2, 9, 'Exam', 'New Mock Paper Added', 'student/mock-paper', 0, '2023-04-29 11:45:34', '0000-00-00 00:00:00'),
(35, 1, 9, 'Exam', 'New Mock Paper Added', 'student/mock-paper', 0, '2023-04-29 11:51:48', '0000-00-00 00:00:00'),
(36, 2, 9, 'Exam', 'New Mock Paper Added', 'student/mock-paper', 0, '2023-04-29 11:51:48', '0000-00-00 00:00:00'),
(37, 1, 0, '', '', '', 0, NULL, NULL),
(38, 2, 0, '', '', '', 0, NULL, NULL),
(39, 1, 9, 'Exam', 'New Practice Paper Added', 'student/practice-paper', 0, '2023-04-29 14:32:42', NULL),
(40, 2, 9, 'Exam', 'New Practice Paper Added', 'student/practice-paper', 0, '2023-04-29 14:32:42', NULL),
(41, 1, 0, '', '', '', 0, NULL, NULL),
(42, 2, 0, '', '', '', 0, NULL, NULL),
(43, 1, 0, '', '', '', 0, NULL, NULL),
(44, 2, 0, '', '', '', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `old_paper_pdf`
--

CREATE TABLE `old_paper_pdf` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `title` varchar(250) CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `batch` varchar(250) CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `topic` varchar(250) CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `subject` varchar(250) CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `file_name` varchar(250) CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `old_paper_pdf`
--

INSERT INTO `old_paper_pdf` (`id`, `admin_id`, `title`, `batch`, `topic`, `subject`, `file_name`, `status`, `added_by`, `added_at`) VALUES
(1, 1, 'test', '[\"9\"]', '', '3', 'sample2203212318331230220181923_(1)230220212120230427160303.pdf', 1, 1, '2023-04-27 16:03:03');

-- --------------------------------------------------------

--
-- Table structure for table `practice_result`
--

CREATE TABLE `practice_result` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL,
  `paper_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `start_time` time NOT NULL,
  `submit_time` time NOT NULL,
  `total_question` int(11) NOT NULL,
  `time_duration` varchar(255) NOT NULL,
  `attempted_question` int(11) NOT NULL,
  `question_answer` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `added_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `percentage` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `practice_result`
--

INSERT INTO `practice_result` (`id`, `admin_id`, `student_id`, `paper_id`, `paper_name`, `date`, `start_time`, `submit_time`, `total_question`, `time_duration`, `attempted_question`, `question_answer`, `added_on`, `percentage`) VALUES
(45, 1, 2, 19, 'rtest', '2023-04-27', '15:32:00', '15:32:00', 2, '10', 2, '{\"5\":\"A\",\"4\":\"A\"}', '2023-04-27 15:32:51', '100.00'),
(46, 1, 2, 2, 'demo', '2023-04-27', '16:05:00', '16:05:00', 5, '2', 5, '{\"5\":\"A\",\"4\":\"A\",\"1\":\"A\",\"3\":\"A\",\"2\":\"A\"}', '2023-04-27 16:05:30', '100.00'),
(47, 1, 1, 2, 'demo', '2023-04-29', '11:43:00', '11:44:00', 5, '2', 5, '{\"1\":\"A\",\"2\":\"B\",\"3\":\"A\",\"4\":\"A\",\"5\":\"A\"}', '2023-04-29 11:44:00', '75.00'),
(44, 1, 1, 15, 'student practice', '2023-04-27', '14:44:00', '14:44:00', 2, '20', 2, '{\"4\":\"A\",\"5\":\"A\"}', '2023-04-27 14:44:23', '100.00'),
(41, 1, 1, 2, 'inorganic practice quize', '2023-04-27', '12:03:00', '12:03:00', 3, '20', 3, '{\"1\":\"A\",\"2\":\"A\",\"3\":\"A\"}', '2023-04-27 12:03:16', '100.00'),
(43, 1, 1, 15, 'student practice', '2023-04-27', '13:02:00', '13:02:00', 2, '20', 2, '{\"4\":\"A\",\"5\":\"A\"}', '2023-04-27 13:02:16', '100.00'),
(42, 1, 1, 14, 'student practice', '2023-04-27', '12:54:00', '12:54:00', 2, '20', 2, '{\"4\":\"B\",\"5\":\"B\"}', '2023-04-27 12:54:49', '0.00'),
(38, 1, 1, 1, 'Inorganic Organic quizes', '2023-04-26', '18:53:00', '14:45:00', 5, '12', 5, '{\"1\":\"D\",\"2\":\"D\",\"3\":\"A\",\"4\":\"D\",\"5\":\"D\"}', '2023-04-26 14:45:40', '20.00'),
(36, 1, 1, 11, 'Inorganic Organic quizes', '2023-04-26', '14:30:00', '14:30:14', 5, '12', 5, '{\"1\":\"D\",\"2\":\"D\",\"3\":\"A\",\"4\":\"D\",\"5\":\"D\"}', '2023-04-26 14:30:15', '20.00'),
(39, 1, 1, 11, 'Inorganic Organic quizes', '2023-04-26', '17:15:00', '17:15:00', 5, '12', 5, '{\"1\":\"D\",\"2\":\"D\",\"3\":\"D\",\"4\":\"A\",\"5\":\"D\"}', '2023-04-26 17:15:55', '20.00'),
(40, 1, 1, 11, 'Inorganic Organic quizes', '2023-04-27', '12:02:00', '12:02:00', 5, '12', 1, '{\"4\":\"A\"}', '2023-04-27 12:02:21', '20.00'),
(48, 1, 1, 6, 'Inorganic Organic quizes', '2023-04-29', '15:10:00', '15:11:00', 5, '20', 5, '{\"1\":\"A\",\"2\":\"A\",\"3\":\"A\",\"4\":\"A\",\"5\":\"A\"}', '2023-04-29 15:11:09', '100.00');

-- --------------------------------------------------------

--
-- Table structure for table `privacy_policy_data`
--

CREATE TABLE `privacy_policy_data` (
  `id` int(11) NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `privacy_policy_data`
--

INSERT INTO `privacy_policy_data` (`id`, `description`) VALUES
(1, '&lt;h1&gt;Terms and Conditions&lt;/h1&gt;\r\n\r\n&lt;p&gt;Welcome to Loan Script&lt;/p&gt;\r\n\r\n&lt;p&gt;These terms and conditions outline the rules and regulations for the use of Loan Script Website, located at &lt;!--{C}&lt;!--?php echo base_url(); ?--&gt;--&amp;gt;.&lt;/p&gt;\r\n\r\n&lt;p&gt;By accessing this website we assume you accept these terms and conditions. Do not continue to use Loan SCript if you do not agree to take all of the terms and conditions stated on this page.&lt;/p&gt;\r\n\r\n&lt;p&gt;The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and all Agreements: &amp;quot;Client&amp;quot;, &amp;quot;You&amp;quot; and &amp;quot;Your&amp;quot; refers to you, the person log on this website and compliant to the Company&amp;rsquo;s terms and conditions. &amp;quot;The Company&amp;quot;, &amp;quot;Ourselves&amp;quot;, &amp;quot;We&amp;quot;, &amp;quot;Our&amp;quot; and &amp;quot;Us&amp;quot;, refers to our Company. &amp;quot;Party&amp;quot;, &amp;quot;Parties&amp;quot;, or &amp;quot;Us&amp;quot;, refers to both the Client and ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner for the express purpose of meeting the Client&amp;rsquo;s needs in respect of provision of the Company&amp;rsquo;s stated services, in accordance with and subject to, prevailing law of Netherlands. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.&lt;/p&gt;\r\n\r\n&lt;h1&gt;Cookies&lt;/h1&gt;\r\n\r\n&lt;p&gt;We employ the use of cookies. By accessing Loan SCript, you agreed to use cookies in agreement with theLoan Script Privacy Policy.&lt;/p&gt;\r\n\r\n&lt;p&gt;Most interactive websites use cookies to let us retrieve the user&amp;rsquo;s details for each visit. Cookies are used by our website to enable the functionality of certain areas to make it easier for people visiting our website. Some of our affiliate/advertising partners may also use cookies.&lt;/p&gt;\r\n\r\n&lt;h1&gt;License&lt;/h1&gt;\r\n\r\n&lt;p&gt;Unless otherwise stated, Loan Script and/or its licensors own the intellectual property rights for all material on Loan SCript. All intellectual property rights are reserved. You may access this from Loan SCript for your own personal use subjected to restrictions set in these terms and conditions.&lt;/p&gt;\r\n\r\n&lt;p&gt;You must not:&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n &lt;li&gt;Republish material from Loan Script&lt;/li&gt;\r\n &lt;li&gt;Sell, rent or sub-license material from Loan Script&lt;/li&gt;\r\n &lt;li&gt;Reproduce, duplicate or copy material from Loan Script&lt;/li&gt;\r\n &lt;li&gt;Redistribute content from Loan Script&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p&gt;This Agreement shall begin on the date hereof. Our Terms and Conditions were created with the help of the Terms And Conditions Generator.&lt;/p&gt;\r\n\r\n&lt;p&gt;Parts of this website offer an opportunity for users to post and exchange opinions and information in certain areas of the website. Livo Bank does not filter, edit, publish or review Comments prior to their presence on the website. Comments do not reflect the views and opinions of Livo Bank,its agents and/or affiliates. Comments reflect the views and opinions of the person who post their views and opinions. To the extent permitted by applicable laws, Livo Bank shall not be liable for the Comments or for any liability, damages or expenses caused and/or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website.&lt;/p&gt;\r\n\r\n&lt;p&gt;Livo Bank reserves the right to monitor all Comments and to remove any Comments which can be considered inappropriate, offensive or causes breach of these Terms and Conditions.&lt;/p&gt;\r\n\r\n&lt;p&gt;You warrant and represent that:&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n &lt;li&gt;You are entitled to post the Comments on our website and have all necessary licenses and consents to do so;&lt;/li&gt;\r\n &lt;li&gt;The Comments do not invade any intellectual property right, including without limitation copyright, patent or trademark of any third party;&lt;/li&gt;\r\n &lt;li&gt;The Comments do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful material which is an invasion of privacy&lt;/li&gt;\r\n &lt;li&gt;The Comments will not be used to solicit or promote business or custom or present commercial activities or unlawful activity.&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p&gt;You hereby grant Livo Bank a non-exclusive license to use, reproduce, edit and authorize others to use, reproduce and edit any of your Comments in any and all forms, formats or media.&lt;/p&gt;\r\n\r\n&lt;h1&gt;Hyperlinking to our Content&lt;/h1&gt;\r\n\r\n&lt;p&gt;The following organizations may link to our Website without prior written approval:&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n &lt;li&gt;Government agencies;&lt;/li&gt;\r\n &lt;li&gt;Search engines;&lt;/li&gt;\r\n &lt;li&gt;News organizations;&lt;/li&gt;\r\n &lt;li&gt;Online directory distributors may link to our Website in the same manner as they hyperlink to the Websites of other listed businesses; and&lt;/li&gt;\r\n &lt;li&gt;System wide Accredited Businesses except soliciting non-profit organizations, charity shopping malls, and charity fundraising groups which may not hyperlink to our Web site.&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p&gt;These organizations may link to our home page, to publications or to other Website information so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products and/or services; and (c) fits within the context of the linking party&amp;rsquo;s site.&lt;/p&gt;\r\n\r\n&lt;p&gt;We may consider and approve other link requests from the following types of organizations:&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n &lt;li&gt;commonly-known consumer and/or business information sources;&lt;/li&gt;\r\n &lt;li&gt;dot.com community sites;&lt;/li&gt;\r\n &lt;li&gt;associations or other groups representing charities;&lt;/li&gt;\r\n &lt;li&gt;online directory distributors;&lt;/li&gt;\r\n &lt;li&gt;internet portals;&lt;/li&gt;\r\n &lt;li&gt;accounting, law and consulting firms; and&lt;/li&gt;\r\n &lt;li&gt;educational institutions and trade associations.&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p&gt;We will approve link requests from these organizations if we decide that: (a) the link would not make us look unfavorably to ourselves or to our accredited businesses; (b) the organization does not have any negative records with us; (c) the benefit to us from the visibility of the hyperlink compensates the absence of Livo Bank; and (d) the link is in the context of general resource information.&lt;/p&gt;\r\n\r\n&lt;p&gt;These organizations may link to our home page so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products or services; and (c) fits within the context of the linking party&amp;rsquo;s site.&lt;/p&gt;\r\n\r\n&lt;p&gt;If you are one of the organizations listed in paragraph 2 above and are interested in linking to our website, you must inform us by sending an e-mail to Livo Bank. Please include your name, your organization name, contact information as well as the URL of your site, a list of any URLs from which you intend to link to our Website, and a list of the URLs on our site to which you would like to link. Wait 2-3 weeks for a response.&lt;/p&gt;\r\n\r\n&lt;p&gt;Approved organizations may hyperlink to our Website as follows:&lt;/p&gt;\r\n\r\n&lt;p&gt;We may consider and approve other link requests from the following types of organizations:&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n &lt;li&gt;By use of our corporate name; or&lt;/li&gt;\r\n &lt;li&gt;By use of the uniform resource locator being linked to; or&lt;/li&gt;\r\n &lt;li&gt;By use of any other description of our Website being linked to that makes sense within the context and format of content on the linking party&amp;rsquo;s site.&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p&gt;No use of Livo Bank&amp;#39;s logo or other artwork will be allowed for linking absent a trademark license agreement.&lt;/p&gt;\r\n\r\n&lt;h1&gt;iFrames&lt;/h1&gt;\r\n\r\n&lt;p&gt;Without prior approval and written permission, you may not create frames around our Webpages that alter in any way the visual presentation or appearance of our Website.&lt;/p&gt;\r\n\r\n&lt;h1&gt;Content Liability&lt;/h1&gt;\r\n\r\n&lt;p&gt;We shall not be hold responsible for any content that appears on your Website. You agree to protect and defend us against all claims that is rising on your Website. No link(s) should appear on any Website that may be interpreted as libelous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.&lt;/p&gt;\r\n\r\n&lt;h1&gt;Your Privacy&lt;/h1&gt;\r\n\r\n&lt;p&gt;Please read Privacy Policy&lt;/p&gt;\r\n\r\n&lt;h1&gt;Reservation of Rights&lt;/h1&gt;\r\n\r\n&lt;p&gt;We reserve the right to request that you remove all links or any particular link to our Website. You approve to immediately remove all links to our Website upon request. We also reserve the right to amen these terms and conditions and it&amp;rsquo;s linking policy at any time. By continuously linking to our Website, you agree to be bound to and follow these linking terms and conditions.&lt;/p&gt;\r\n\r\n&lt;h1&gt;Removal of links from our website&lt;/h1&gt;\r\n\r\n&lt;p&gt;If you find any link on our Website that is offensive for any reason, you are free to contact and inform us any moment. We will consider requests to remove links but we are not obligated to or so or to respond to you directly.&lt;/p&gt;\r\n\r\n&lt;p&gt;We do not ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we promise to ensure that the website remains available or that the material on the website is kept up to date.&lt;/p&gt;\r\n\r\n&lt;h1&gt;Disclaimer&lt;/h1&gt;\r\n\r\n&lt;p&gt;To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website. Nothing in this disclaimer will:&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n &lt;li&gt;limit or exclude our or your liability for death or personal injury;&lt;/li&gt;\r\n &lt;li&gt;limit or exclude our or your liability for fraud or fraudulent misrepresentation;&lt;/li&gt;\r\n &lt;li&gt;limit any of our or your liabilities in any way that is not permitted under applicable law; or&lt;/li&gt;\r\n &lt;li&gt;exclude any of our or your liabilities that may not be excluded under applicable law.&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p&gt;The limitations and prohibitions of liability set in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer, including liabilities arising in contract, in tort and for breach of statutory duty.&lt;/p&gt;\r\n\r\n&lt;p&gt;As long as the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature.&lt;/p&gt;\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `question` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `options` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `answer` varchar(255) NOT NULL,
  `question_mask` text NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `added_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `admin_id`, `subject_id`, `chapter_id`, `question`, `options`, `answer`, `question_mask`, `added_by`, `status`, `category`, `added_on`) VALUES
(1, 1, 3, 81, '<p>phenol formula</p>\r\n', '[\"\\u003Cp\\u003EC6H5OH\\u003C\\/p\\u003E\\r\\n\",\"\\u003Cp\\u003EC6H5\\u003C\\/p\\u003E\\r\\n\",\"\\u003Cp\\u003EC6H5H\\u003C\\/p\\u003E\\r\\n\",\"\\u003Cp\\u003EC6H5O\\u003C\\/p\\u003E\\r\\n\"]', 'A', '1', 1, 1, 0, '2023-04-25 12:33:00'),
(2, 1, 3, 79, '<p>Benzene formula</p>\r\n', '[\"\\u003Cp\\u003E\\u0026nbsp;C₆H₆\\u003C\\/p\\u003E\\r\\n\",\"\\u003Cp\\u003E\\u0026nbsp;C₆H₆9\\u003C\\/p\\u003E\\r\\n\",\"\\u003Cp\\u003E\\u0026nbsp;C₆H₆o\\u003C\\/p\\u003E\\r\\n\",\"\\u003Cp\\u003E\\u0026nbsp;C₆H₆P\\u003C\\/p\\u003E\\r\\n\"]', 'A', '1', 1, 1, 0, '2023-04-25 12:34:15'),
(3, 1, 3, 79, '<p>chemical formulas&nbsp;Aldehydes&nbsp;</p>\r\n', '[\"\\u003Cp\\u003ER\\u0026minus;CH=O\\u003C\\/p\\u003E\\r\\n\",\"\\u003Cp\\u003EAldehydes\\u0026nbsp;\\u003C\\/p\\u003E\\r\\n\",\"\\u003Cp\\u003Esdf\\u003C\\/p\\u003E\\r\\n\",\"\\u003Cp\\u003Esdfsd\\u003C\\/p\\u003E\\r\\n\"]', 'A', '1', 1, 1, 0, '2023-04-25 12:43:16'),
(4, 1, 3, 79, '<p>2. Mass of an&nbsp;<a href=\"https://en.wikipedia.org/wiki/Atom\">atom</a>&nbsp;is equals to which of the following</p>\r\n', '[\"\\u003Cp\\u003E(c)Neutrons and protons.\\u003C\\/p\\u003E\\r\\n\",\"\\u003Cp\\u003ENeutrons.\\u003C\\/p\\u003E\\r\\n\",\"\\u003Cp\\u003E\\u0026nbsp;protons.\\u003C\\/p\\u003E\\r\\n\",\"\\u003Cp\\u003Eand\\u003C\\/p\\u003E\\r\\n\"]', 'A', '1', 1, 1, 0, '2023-04-26 11:13:13'),
(5, 1, 3, 79, '<p>The atomic number during a chemical reaction will be</p>\r\n', '[\"\\u003Cp\\u003ERemains the same.\\u003C\\/p\\u003E\\r\\n\",\"\\u003Cp\\u003EIncreases.\\u003C\\/p\\u003E\\r\\n\",\"\\u003Cp\\u003EIncreases.\\u003C\\/p\\u003E\\r\\n\",\"\\u003Cp\\u003EIncreases.\\u003C\\/p\\u003E\\r\\n\"]', 'A', '1', 1, 1, 0, '2023-04-26 11:14:38');

-- --------------------------------------------------------

--
-- Table structure for table `site_details`
--

CREATE TABLE `site_details` (
  `id` int(11) NOT NULL,
  `site_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `site_favicon` varchar(255) NOT NULL,
  `site_logo` varchar(255) NOT NULL,
  `site_minilogo` varchar(255) NOT NULL,
  `site_loader` varchar(255) NOT NULL,
  `site_author` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `site_keywords` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `site_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `enrollment_word` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `copyright_text` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `timezone` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_details`
--

INSERT INTO `site_details` (`id`, `site_title`, `site_favicon`, `site_logo`, `site_minilogo`, `site_loader`, `site_author`, `site_keywords`, `site_description`, `enrollment_word`, `copyright_text`, `timezone`) VALUES
(1, 'E Academy', 'favicon8.png', 'elogo1.svg', 'favicon7.png', 'e-academy.gif', 'Kamlesh Yadav', 'e academy,academy,education academy', 'Description about e-academy', 'ACAD', 'Copyright © 2023 E Academy. All Right Reserved.', 'Asia/Kolkata');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `enrollment_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `multi_batch` text NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `contact_no` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `father_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `father_designtn` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `batch_id` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `login_status` int(11) NOT NULL,
  `admission_date` date NOT NULL,
  `status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL COMMENT '(0 unpaid ) (1 paid)',
  `brewers_check` varchar(50) NOT NULL,
  `token` varchar(500) NOT NULL,
  `app_version` varchar(100) NOT NULL,
  `added_by` varchar(50) NOT NULL,
  `last_login_app` datetime NOT NULL,
  `pay_mode` int(11) NOT NULL,
  `google_token` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `admin_id`, `name`, `enrollment_id`, `multi_batch`, `password`, `image`, `email`, `contact_no`, `gender`, `dob`, `father_name`, `father_designtn`, `address`, `batch_id`, `login_status`, `admission_date`, `status`, `payment_status`, `brewers_check`, `token`, `app_version`, `added_by`, `last_login_app`, `pay_mode`, `google_token`) VALUES
(1, 1, 'saloni', 'ACAD0027', '', '202cb962ac59075b964b07152d234b70', 'student_img.png', 'saloni@yopmail.com', '', '', '0000-00-00', '', '', '', '9', 1, '2023-04-25', 1, 1, 'pCsT6IG14i', 'diDo6RPuTgKdOfgtUKLT_-:APA91bHMC82STZC9NgfAcBWSbrFMTpi3XvwCQPCkfxElmnpNG--d4f4MmxxOjNrggAnaizHrr7Bnzl3IZ9sTBDWuUxFxlT0KWaetLjBnPLhGBUo6MJ5-dgdLBpqqHTgipYYAhLmyNKE1', '16', 'student', '2023-04-29 11:42:44', 0, ''),
(2, 1, 'Rahul', 'ACAD0086', '', '202cb962ac59075b964b07152d234b70', 'student_img.png', 'rahul@gmail.com', '7896541236', '', '0000-00-00', '', '', '', '9', 1, '2023-04-27', 1, 0, '', 'dss325veT3y93cGob4LP7D:APA91bHCKfp55sjkV2g60wgei4IU9jHd2cRUqUZ3IluEMiAV7cdR1kyJnVDzbPpk1zDXNf4Agd4PxMIsk4eoaSEZGfoa-Vayo-Chf5R6BXlMDvzrxGkWcXlWJ28OFvEc2tB4WcVUhBsx', '16', 'student', '2023-04-27 23:25:54', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `student_doubts_class`
--

CREATE TABLE `student_doubts_class` (
  `doubt_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `subjects_id` varchar(100) NOT NULL,
  `chapters_id` varchar(500) NOT NULL,
  `users_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `teacher_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `appointment_date` date NOT NULL,
  `appointment_time` varchar(100) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL COMMENT '0 = pending, 1 = approve, 2 = decline',
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_doubts_class`
--

INSERT INTO `student_doubts_class` (`doubt_id`, `student_id`, `teacher_id`, `batch_id`, `subjects_id`, `chapters_id`, `users_description`, `teacher_description`, `appointment_date`, `appointment_time`, `create_at`, `status`, `admin_id`) VALUES
(1, 1, 9, 9, '3', '80', 'DOUGHT TESTING', 'bfgbfbgfb', '2023-04-28', '6:30 AM', '2023-04-27 16:12:59', 1, 1),
(2, 1, 9, 9, '3', '79', 'gdfcghsvfcxvdv', '', '0000-00-00', '', '2023-04-27 17:10:14', 2, 1),
(3, 1, 9, 9, '3', '81', 'for admin ', '', '0000-00-00', '', '2023-04-27 17:17:50', 2, 1),
(4, 1, 28, 24, '1', '84', 'for maths', 'ffsdsfsfsdf', '2023-04-28', '5:30 AM', '2023-04-27 17:19:32', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `student_payment_history`
--

CREATE TABLE `student_payment_history` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `transaction_id` longtext NOT NULL,
  `mode` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `student_payment_history`
--

INSERT INTO `student_payment_history` (`id`, `student_id`, `batch_id`, `transaction_id`, `mode`, `amount`, `create_at`, `admin_id`) VALUES
(1, 1, 24, '3M066029AR790923G', '', 800, '2023-04-27 15:40:49', 0);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `subject_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `no_of_questions` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `subject_name`, `status`, `no_of_questions`, `admin_id`) VALUES
(1, 'Maths', 1, 31, 1),
(2, 'Physics', 1, 16, 1),
(3, 'Chemistry', 1, 25, 1),
(7, 'English', 1, 3, 1),
(15, 'math', 1, 5, 13),
(16, 'dfgdgdfgd', 1, 0, 13),
(19, 'dcdc', 1, 0, 21),
(17, 'f fd f f', 1, 1, 16);

-- --------------------------------------------------------

--
-- Table structure for table `sudent_batchs`
--

CREATE TABLE `sudent_batchs` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `added_by` varchar(100) NOT NULL,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sudent_batchs`
--

INSERT INTO `sudent_batchs` (`id`, `student_id`, `batch_id`, `status`, `create_at`, `added_by`, `admin_id`) VALUES
(1, 1, 9, 0, '2023-04-25 12:24:14', 'student', 1),
(2, 2, 21, 0, '2023-04-27 15:26:09', 'student', 1),
(3, 1, 24, 0, '2023-04-27 15:40:49', 'student', 0),
(4, 2, 9, 0, '2023-04-27 15:49:05', 'student', 1);

-- --------------------------------------------------------

--
-- Table structure for table `temp_data`
--

CREATE TABLE `temp_data` (
  `id` int(11) NOT NULL,
  `temp` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_data`
--

INSERT INTO `temp_data` (`id`, `temp`) VALUES
(1, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(2, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(3, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(4, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(5, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(6, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(7, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(8, '{\"name\":\"saloni\",\"mobile\":\"8989898989\",\"email\":\"saloni@yopmail.com\",\"versionCode\":\"null\",\"token\":\"cvVkifAqSa6VoEHRvEzqiU:APA91bGpGqRpjbjMEDFmTXs-rTFOI9MwYDqOVXJYYw7ilzyYLJ4J9i9aIM__AMf1x5fTwiCT1iuZQQbrpbuZql4FS53ySQ5lIfiBm42oMzVweOHbkDKMUHx8Q_jEeKY2h7XE7RDaYVJQ\"}'),
(9, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(10, '{\"search\":\"\",\"start\":\"3\",\"length\":\"6\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(11, '{\"student_id\":\"1\"}'),
(12, '{\"student_id\":\"1\"}'),
(13, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(14, '{\"transaction_id\":\"\",\"amount\":\"\",\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(15, '{\"student_id\":\"1\"}'),
(16, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(17, '{\"student_id\":\"1\"}'),
(18, '{\"student_id\":\"1\"}'),
(19, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(20, '{\"student_id\":\"1\"}'),
(21, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(22, '{\"student_id\":\"1\"}'),
(23, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(24, '{\"exam_type\":\"mock_test\",\"paper_name\":\"inorganic paper \",\"start_time\":\"12:40:30 PM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"2\",\"time_duration\":\"12\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"A\\\"}\",\"paper_id\":\"1\"}'),
(25, '{\"exam_type\":\"practice\",\"paper_name\":\"inorganic practice quize\",\"start_time\":\"12:45:42 PM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"3\",\"time_duration\":\"20\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"A\\\",\\\"3\\\":\\\"A\\\"}\",\"paper_id\":\"2\"}'),
(26, '{\"exam_type\":\"practice\",\"paper_name\":\"inorganic practice quize\",\"start_time\":\"12:49:01 PM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"3\",\"time_duration\":\"20\",\"question_answer\":\"{\\\"1\\\":\\\"D\\\",\\\"2\\\":\\\"A\\\",\\\"3\\\":\\\"D\\\"}\",\"paper_id\":\"2\"}'),
(27, '{\"student_id\":\"1\"}'),
(28, '{\"student_id\":\"1\"}'),
(29, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(30, '{\"student_id\":\"1\"}'),
(31, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(32, '{\"exam_type\":\"mock_test\",\"paper_name\":\"test\",\"start_time\":\"02:10:26 PM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"3\",\"time_duration\":\"8\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"A\\\",\\\"3\\\":\\\"A\\\"}\",\"paper_id\":\"3\"}'),
(33, '{\"student_id\":\"1\"}'),
(34, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(35, '{\"student_id\":\"1\"}'),
(36, '{\"student_id\":\"1\"}'),
(37, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(38, '{\"student_id\":\"1\"}'),
(39, '{\"student_id\":\"1\"}'),
(40, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(41, '{\"student_id\":\"1\"}'),
(42, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(43, '{\"student_id\":\"1\"}'),
(44, '{\"student_id\":\"1\"}'),
(45, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(46, '{\"student_id\":\"1\"}'),
(47, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(48, '{\"exam_type\":\"practice\",\"paper_name\":\"inorganic practice quize\",\"start_time\":\"02:35:07 PM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"3\",\"time_duration\":\"20\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"A\\\",\\\"3\\\":\\\"A\\\"}\",\"paper_id\":\"2\"}'),
(49, '{\"exam_type\":\"practice\",\"paper_name\":\"inorganic practice quize\",\"start_time\":\"02:36:33 PM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"3\",\"time_duration\":\"20\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"A\\\",\\\"3\\\":\\\"A\\\"}\",\"paper_id\":\"2\"}'),
(50, '{\"student_id\":\"1\"}'),
(51, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(52, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(53, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(54, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(55, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(56, '{\"student_id\":\"1\"}'),
(57, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(58, '{\"exam_type\":\"practice\",\"paper_name\":\"inorganic practice quize\",\"start_time\":\"06:18:05 PM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"3\",\"time_duration\":\"20\",\"question_answer\":\"{\\\"1\\\":\\\"D\\\",\\\"2\\\":\\\"A\\\",\\\"3\\\":\\\"D\\\"}\",\"paper_id\":\"2\"}'),
(59, '{\"student_id\":\"1\"}'),
(60, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(61, '{\"exam_type\":\"practice\",\"paper_name\":\"inorganic practice quize\",\"start_time\":\"06:19:09 PM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"3\",\"time_duration\":\"20\",\"question_answer\":\"{\\\"1\\\":\\\"D\\\",\\\"2\\\":\\\"A\\\",\\\"3\\\":\\\"B\\\"}\",\"paper_id\":\"2\"}'),
(62, '{\"batch_id\":\"9\",\"exam_type\":\"mock_test\",\"admin_id\":\"1\",\"student_id\":\"6\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"A\\\"}\",\"paper_name\":\"testing by saloni\",\"total_question\":\"3\",\"start_time\":\"18:53\",\"time_duration\":\"5\",\"paper_id\":\"1\"}'),
(63, '{\"batch_id\":\"9\",\"exam_type\":\"mock_test\",\"admin_id\":\"1\",\"student_id\":\"6\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"A\\\"}\",\"paper_name\":\"inorganic paper\",\"total_question\":\"3\",\"start_time\":\"18:53\",\"time_duration\":\"5\",\"paper_id\":\"1\"}'),
(64, '{\"student_id\":\"1\"}'),
(65, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(66, '{\"exam_type\":\"practice\",\"paper_name\":\"test\",\"start_time\":\"06:23:47 PM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"3\",\"time_duration\":\"2\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"A\\\",\\\"3\\\":\\\"A\\\"}\",\"paper_id\":\"4\"}'),
(67, '{\"student_id\":\"1\"}'),
(68, '{\"student_id\":\"1\"}'),
(69, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(70, '{\"student_id\":\"1\"}'),
(71, '{\"student_id\":\"1\"}'),
(72, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(73, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(74, '{\"batch_id\":\"9\",\"exam_type\":\"mock_test\",\"admin_id\":\"1\",\"student_id\":\"6\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"A\\\"}\",\"paper_name\":\"inorganic paper\",\"total_question\":\"3\",\"start_time\":\"18:53\",\"time_duration\":\"5\",\"paper_id\":\"1\"}'),
(75, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(76, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(77, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(78, '{\"search\":\"\",\"start\":\"4\",\"length\":\"8\",\"limit\":\"3\"}'),
(79, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(80, '{\"student_id\":\"1\"}'),
(81, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(82, '{\"exam_type\":\"practice\",\"paper_name\":\"inorganic practice quize\",\"start_time\":\"10:49:37 AM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"3\",\"time_duration\":\"20\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"B\\\",\\\"3\\\":\\\"A\\\"}\",\"paper_id\":\"2\"}'),
(83, '{\"batch_id\":\"9\",\"exam_type\":\"mock_test\",\"admin_id\":\"1\",\"student_id\":\"6\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"A\\\"}\",\"paper_name\":\"inorganic paper\",\"total_question\":\"3\",\"start_time\":\"18:53\",\"time_duration\":\"5\",\"paper_id\":\"1\"}'),
(84, '{\"batch_id\":\"9\",\"exam_type\":\"practice\",\"admin_id\":\"1\",\"student_id\":\"6\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"A\\\"}\",\"paper_name\":\"Inorganic Practice Quize\",\"total_question\":\"3\",\"start_time\":\"18:53\",\"time_duration\":\"5\",\"paper_id\":\"1\"}'),
(85, '{\"batch_id\":\"9\",\"exam_type\":\"practice\",\"admin_id\":\"1\",\"student_id\":\"6\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"A\\\"}\",\"paper_name\":\"Inorganic Practice Quize\",\"total_question\":\"3\",\"start_time\":\"18:53\",\"time_duration\":\"5\",\"paper_id\":\"1\"}'),
(86, '{\"student_id\":\"1\"}'),
(87, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(88, '{\"batch_id\":\"9\",\"exam_type\":\"moct_test\",\"admin_id\":\"1\",\"student_id\":\"6\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"A\\\"}\",\"paper_name\":\"inorganic paper\",\"total_question\":\"3\",\"start_time\":\"18:53\",\"time_duration\":\"5\",\"paper_id\":\"1\"}'),
(89, '{\"exam_type\":\"practice\",\"paper_name\":\"inorganic practice quize\",\"start_time\":\"11:00:17 AM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"3\",\"time_duration\":\"20\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"D\\\",\\\"3\\\":\\\"A\\\"}\",\"paper_id\":\"2\"}'),
(90, '{\"batch_id\":\"9\",\"exam_type\":\"moct_test\",\"admin_id\":\"1\",\"student_id\":\"6\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"A\\\"}\",\"paper_name\":\"inorganic paper\",\"total_question\":\"3\",\"start_time\":\"18:53\",\"time_duration\":\"5\",\"paper_id\":\"1\"}'),
(91, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(92, '{\"student_id\":\"1\"}'),
(93, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(94, '{\"exam_type\":\"practice\",\"paper_name\":\"Inorganic Organic quizes\",\"start_time\":\"11:16:17 AM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"5\",\"time_duration\":\"12\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"A\\\",\\\"3\\\":\\\"A\\\",\\\"4\\\":\\\"A\\\",\\\"5\\\":\\\"A\\\"}\",\"paper_id\":\"11\"}'),
(95, '{\"batch_id\":\"9\",\"exam_type\":\"moct_test\",\"admin_id\":\"1\",\"student_id\":\"6\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"A\\\"}\",\"paper_name\":\"inorganic paper\",\"total_question\":\"3\",\"start_time\":\"18:53\",\"time_duration\":\"5\",\"paper_id\":\"1\"}'),
(96, '{\"batch_id\":\"9\",\"exam_type\":\"practice\",\"admin_id\":\"1\",\"student_id\":\"6\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"A\\\"}\",\"paper_name\":\"Inorganic Practice Quize\",\"total_question\":\"3\",\"start_time\":\"18:53\",\"time_duration\":\"5\",\"paper_id\":\"1\"}'),
(97, '{\"exam_type\":\"practice\",\"paper_name\":\"Inorganic Organic quizes\",\"start_time\":\"11:19:22 AM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"5\",\"time_duration\":\"12\",\"question_answer\":\"{\\\"3\\\":\\\"A\\\"}\",\"paper_id\":\"11\"}'),
(98, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(99, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(100, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(101, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(102, '{\"student_id\":\"1\"}'),
(103, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(104, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(105, '{\"student_id\":\"1\"}'),
(106, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(107, '{\"student_id\":\"1\"}'),
(108, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(109, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(110, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(111, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(112, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(113, '{\"student_id\":\"1\"}'),
(114, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(115, '{\"exam_type\":\"mock_test\",\"paper_name\":\"Quize of 26-april-22\",\"start_time\":\"02:15:02 PM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"5\",\"time_duration\":\"6\",\"question_answer\":\"{\\\"2\\\":\\\"A\\\",\\\"3\\\":\\\"A\\\",\\\"5\\\":\\\"A\\\"}\",\"paper_id\":\"12\"}'),
(116, '{\"exam_type\":\"practice\",\"paper_name\":\"Inorganic Organic quizes\",\"start_time\":\"02:16:06 PM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"5\",\"time_duration\":\"12\",\"question_answer\":\"{\\\"1\\\":\\\"D\\\",\\\"2\\\":\\\"D\\\",\\\"3\\\":\\\"A\\\",\\\"4\\\":\\\"D\\\",\\\"5\\\":\\\"D\\\"}\",\"paper_id\":\"11\"}'),
(117, '{\"exam_type\":\"practice\",\"paper_name\":\"Inorganic Organic quizes\",\"start_time\":\"02:21:14 PM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"5\",\"time_duration\":\"12\",\"question_answer\":\"{\\\"1\\\":\\\"D\\\",\\\"2\\\":\\\"D\\\",\\\"3\\\":\\\"D\\\",\\\"4\\\":\\\"D\\\",\\\"5\\\":\\\"A\\\"}\",\"paper_id\":\"11\"}'),
(118, '{\"batch_id\":\"9\",\"exam_type\":\"practice\",\"admin_id\":\"1\",\"student_id\":\"1\",\"question_answer\":\"{\\\"1\\\":\\\"D\\\",\\\"2\\\":\\\"D\\\",\\\"3\\\":\\\"D\\\",\\\"4\\\":\\\"D\\\",\\\"5\\\":\\\"A\\\"}\",\"paper_name\":\"Inorganic Organic quizes\",\"total_question\":\"3\",\"start_time\":\"18:53\",\"time_duration\":\"5\",\"paper_id\":\"1\"}'),
(119, '{\"batch_id\":\"9\",\"exam_type\":\"practice\",\"admin_id\":\"1\",\"student_id\":\"1\",\"question_answer\":\"{\\\"1\\\":\\\"D\\\",\\\"2\\\":\\\"D\\\",\\\"3\\\":\\\"A\\\",\\\"4\\\":\\\"D\\\",\\\"5\\\":\\\"D\\\"}\",\"paper_name\":\"Inorganic Organic quizes\",\"total_question\":\"5\",\"start_time\":\"18:53\",\"time_duration\":\"12\",\"paper_id\":\"1\"}'),
(120, '{\"batch_id\":\"9\",\"exam_type\":\"practice\",\"admin_id\":\"1\",\"student_id\":\"1\",\"question_answer\":\"{\\\"1\\\":\\\"D\\\",\\\"2\\\":\\\"D\\\",\\\"3\\\":\\\"A\\\",\\\"4\\\":\\\"D\\\",\\\"5\\\":\\\"D\\\"}\",\"paper_name\":\"Inorganic Organic quizes\",\"total_question\":\"5\",\"start_time\":\"18:53\",\"time_duration\":\"12\",\"paper_id\":\"1\"}'),
(121, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(122, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(123, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(124, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(125, '{\"student_id\":\"1\"}'),
(126, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(127, '{\"exam_type\":\"practice\",\"paper_name\":\"Inorganic Organic quizes\",\"start_time\":\"05:15:42 PM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"5\",\"time_duration\":\"12\",\"question_answer\":\"{\\\"1\\\":\\\"D\\\",\\\"2\\\":\\\"D\\\",\\\"3\\\":\\\"D\\\",\\\"4\\\":\\\"A\\\",\\\"5\\\":\\\"D\\\"}\",\"paper_id\":\"11\"}'),
(128, '{\"student_id\":\"1\"}'),
(129, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(130, '{\"search\":\"\",\"start\":\"3\",\"length\":\"6\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(131, '{\"search\":\"\",\"start\":\"3\",\"length\":\"6\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(132, '{\"student_id\":\"1\"}'),
(133, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(134, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(135, '{\"student_id\":\"1\"}'),
(136, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(137, '{\"student_id\":\"1\"}'),
(138, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(139, '{\"student_id\":\"1\"}'),
(140, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(141, '{\"student_id\":\"1\"}'),
(142, '{\"student_id\":\"1\"}'),
(143, '{\"student_id\":\"1\"}'),
(144, '{\"student_id\":\"1\"}'),
(145, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(146, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(147, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(148, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(149, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(150, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(151, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(152, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(153, '{\"student_id\":\"1\"}'),
(154, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(155, '{\"exam_type\":\"practice\",\"paper_name\":\"Inorganic Organic quizes\",\"start_time\":\"12:02:09 PM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"5\",\"time_duration\":\"12\",\"question_answer\":\"{\\\"4\\\":\\\"A\\\"}\",\"paper_id\":\"11\"}'),
(156, '{\"exam_type\":\"practice\",\"paper_name\":\"inorganic practice quize\",\"start_time\":\"12:03:09 PM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"3\",\"time_duration\":\"20\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"A\\\",\\\"3\\\":\\\"A\\\"}\",\"paper_id\":\"2\"}'),
(157, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(158, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(159, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(160, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(161, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(162, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(163, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(164, '{\"search\":\"\",\"start\":\"4\",\"length\":\"8\",\"limit\":\"3\"}'),
(165, '{\"search\":\"\",\"start\":\"0\",\"student_id\":\"1\",\"length\":\"4\",\"limit\":\"3\"}'),
(166, '{\"search\":\"\",\"start\":\"3\",\"student_id\":\"1\",\"length\":\"6\",\"limit\":\"3\"}'),
(167, '{\"search\":\"\",\"start\":\"3\",\"student_id\":\"1\",\"length\":\"6\",\"limit\":\"3\"}'),
(168, '{\"search\":\"\",\"start\":\"3\",\"student_id\":\"1\",\"length\":\"6\",\"limit\":\"3\"}'),
(169, '{\"student_id\":\"1\"}'),
(170, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(171, '{\"search\":\"\",\"start\":\"0\",\"student_id\":\"1\",\"length\":\"4\",\"limit\":\"3\"}'),
(172, '{\"search\":\"\",\"start\":\"3\",\"student_id\":\"1\",\"length\":\"6\",\"limit\":\"3\"}'),
(173, '{\"search\":\"\",\"start\":\"3\",\"student_id\":\"1\",\"length\":\"6\",\"limit\":\"3\"}'),
(174, '{\"search\":\"\",\"start\":\"3\",\"student_id\":\"1\",\"length\":\"6\",\"limit\":\"3\"}'),
(175, '{\"student_id\":\"1\"}'),
(176, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(177, '{\"search\":\"\",\"start\":\"0\",\"student_id\":\"1\",\"length\":\"4\",\"limit\":\"3\"}'),
(178, '{\"search\":\"\",\"start\":\"3\",\"student_id\":\"1\",\"length\":\"6\",\"limit\":\"3\"}'),
(179, '{\"student_id\":\"1\"}'),
(180, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(181, '{\"student_id\":\"1\"}'),
(182, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(183, '{\"student_id\":\"1\"}'),
(184, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(185, '{\"student_id\":\"1\"}'),
(186, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(187, '{\"student_id\":\"1\"}'),
(188, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(189, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(190, '{\"total_question\":\"2\",\"student_id\":\"1\",\"start_time\":\"12:54:19 PM\",\"admin_id\":\"1\",\"time_duration\":\"20\",\"question_answer\":\"{\\\"4\\\":\\\"B\\\",\\\"5\\\":\\\"B\\\"}\",\"paper_id\":\"14\",\"paper_name\":\"student practice\",\"exam_type\":\"practice\"}'),
(191, '{\"total_question\":\"2\",\"student_id\":\"1\",\"start_time\":\"01:02:05 PM\",\"admin_id\":\"1\",\"time_duration\":\"20\",\"question_answer\":\"{\\\"4\\\":\\\"A\\\",\\\"5\\\":\\\"A\\\"}\",\"paper_id\":\"15\",\"paper_name\":\"student practice\",\"exam_type\":\"practice\"}'),
(192, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(193, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(194, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(195, '{\"student_id\":\"1\"}'),
(196, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(197, '{\"search\":\"\",\"start\":\"0\",\"student_id\":\"1\",\"length\":\"4\",\"limit\":\"3\"}'),
(198, '{\"search\":\"\",\"start\":\"3\",\"student_id\":\"1\",\"length\":\"6\",\"limit\":\"3\"}'),
(199, '{\"search\":\"\",\"start\":\"3\",\"student_id\":\"1\",\"length\":\"6\",\"limit\":\"3\"}'),
(200, '{\"student_id\":\"1\"}'),
(201, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(202, '{\"student_id\":\"1\"}'),
(203, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(204, '{\"total_question\":\"2\",\"student_id\":\"1\",\"start_time\":\"02:44:06 PM\",\"admin_id\":\"1\",\"time_duration\":\"20\",\"question_answer\":\"{\\\"4\\\":\\\"A\\\",\\\"5\\\":\\\"A\\\"}\",\"paper_id\":\"15\",\"paper_name\":\"student practice\",\"exam_type\":\"practice\"}'),
(205, '{\"total_question\":\"2\",\"student_id\":\"1\",\"start_time\":\"02:47:26 PM\",\"admin_id\":\"1\",\"time_duration\":\"15\",\"question_answer\":\"{\\\"5\\\":\\\"A\\\",\\\"4\\\":\\\"A\\\"}\",\"paper_id\":\"18\",\"paper_name\":\"test\",\"exam_type\":\"mock_test\"}'),
(206, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(207, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(208, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(209, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(210, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(211, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(212, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(213, '{\"student_id\":\"1\"}'),
(214, '{\"student_id\":\"1\"}'),
(215, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(216, '{\"search\":\"\",\"start\":\"0\",\"student_id\":\"1\",\"length\":\"4\",\"limit\":\"3\"}'),
(217, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(218, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(219, '{\"name\":\"Rahul\",\"email\":\"rahul@gmail.com\",\"versionCode\":\"null\",\"token\":\"ezvAmPuXQSO0Trh7pjFaso:APA91bEsd0XhUPfdgLavtOtFovfkC7a6FsOOFzjpjV7YGFB3VGmRtSZyhFQtestthAdrLk2-l2VSCYvA2kWKmNDBTffTReB-tQfAK7uDm94c0lKRN6303tFP4TubvgQrwGmxjzbK0hed\",\"mobile\":\"7896541236\"}'),
(220, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(221, '{\"search\":\"\",\"start\":\"4\",\"length\":\"8\",\"limit\":\"3\"}'),
(222, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(223, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(224, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(225, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(226, '{\"search\":\"\",\"start\":\"4\",\"length\":\"8\",\"limit\":\"3\"}'),
(227, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(228, '{\"search\":\"\",\"start\":\"3\",\"length\":\"6\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(229, '{\"student_id\":\"1\"}'),
(230, '{\"student_id\":\"1\"}'),
(231, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(232, '{\"search\":\"\",\"start\":\"0\",\"student_id\":\"2\",\"length\":\"4\",\"limit\":\"3\"}'),
(233, '{\"search\":\"\",\"start\":\"3\",\"student_id\":\"2\",\"length\":\"6\",\"limit\":\"3\"}'),
(234, '{\"student_id\":\"1\"}'),
(235, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(236, '{\"student_id\":\"1\"}'),
(237, '{\"student_id\":\"1\"}'),
(238, '{\"student_id\":\"1\"}'),
(239, '{\"student_id\":\"1\"}'),
(240, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(241, '{\"student_id\":\"1\"}'),
(242, '{\"student_id\":\"1\"}'),
(243, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(244, '{\"student_id\":\"1\"}'),
(245, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(246, '{\"student_id\":\"1\"}'),
(247, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(248, '{\"student_id\":\"1\"}'),
(249, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(250, '{\"student_id\":\"1\"}'),
(251, '{\"search\":\"\",\"start\":\"3\",\"student_id\":\"2\",\"length\":\"6\",\"limit\":\"3\"}'),
(252, '{\"amount\":\"\",\"student_id\":\"2\",\"batch_id\":\"21\",\"transaction_id\":\"\"}'),
(253, '{\"student_id\":\"2\"}'),
(254, '{\"batch_id\":\"21\",\"student_id\":\"2\"}'),
(255, '{\"student_id\":\"2\"}'),
(256, '{\"batch_id\":\"21\",\"student_id\":\"2\"}'),
(257, '{\"search\":\"\",\"start\":\"0\",\"student_id\":\"2\",\"length\":\"4\",\"limit\":\"3\"}'),
(258, '{\"search\":\"\",\"start\":\"3\",\"student_id\":\"2\",\"length\":\"6\",\"limit\":\"3\"}'),
(259, '{\"student_id\":\"2\"}'),
(260, '{\"batch_id\":\"21\",\"student_id\":\"2\"}'),
(261, '{\"student_id\":\"2\"}'),
(262, '{\"batch_id\":\"21\",\"student_id\":\"2\"}'),
(263, '{\"student_id\":\"2\"}'),
(264, '{\"batch_id\":\"21\",\"student_id\":\"2\"}'),
(265, '{\"student_id\":\"2\"}'),
(266, '{\"batch_id\":\"21\",\"student_id\":\"2\"}'),
(267, '{\"student_id\":\"2\"}'),
(268, '{\"batch_id\":\"21\",\"student_id\":\"2\"}'),
(269, '{\"total_question\":\"2\",\"student_id\":\"2\",\"start_time\":\"03:32:42 PM\",\"admin_id\":\"1\",\"time_duration\":\"10\",\"question_answer\":\"{\\\"5\\\":\\\"A\\\",\\\"4\\\":\\\"A\\\"}\",\"paper_id\":\"19\",\"paper_name\":\"rtest\",\"exam_type\":\"practice\"}'),
(270, '{\"student_id\":\"2\"}'),
(271, '{\"batch_id\":\"21\",\"student_id\":\"2\"}'),
(272, '{\"student_id\":\"2\"}'),
(273, '{\"student_id\":\"2\"}'),
(274, '{\"search\":\"\",\"start\":\"0\",\"student_id\":\"2\",\"length\":\"4\",\"limit\":\"3\"}'),
(275, '{\"search\":\"\",\"start\":\"2\",\"student_id\":\"2\",\"length\":\"5\",\"limit\":\"3\"}'),
(276, '{\"search\":\"\",\"start\":\"3\",\"student_id\":\"2\",\"length\":\"6\",\"limit\":\"3\"}'),
(277, '{\"amount\":\"\",\"student_id\":\"2\",\"batch_id\":\"9\",\"transaction_id\":\"\"}'),
(278, '{\"search\":\"\",\"start\":\"0\",\"student_id\":\"2\",\"length\":\"4\",\"limit\":\"3\"}'),
(279, '{\"search\":\"\",\"start\":\"0\",\"student_id\":\"2\",\"length\":\"4\",\"limit\":\"3\"}'),
(280, '{\"search\":\"\",\"start\":\"2\",\"student_id\":\"2\",\"length\":\"5\",\"limit\":\"3\"}'),
(281, '{\"search\":\"\",\"start\":\"3\",\"student_id\":\"2\",\"length\":\"6\",\"limit\":\"3\"}'),
(282, '{\"student_id\":\"2\"}'),
(283, '{\"batch_id\":\"9\",\"student_id\":\"2\"}'),
(284, '{\"total_question\":\"5\",\"student_id\":\"2\",\"start_time\":\"03:50:46 PM\",\"admin_id\":\"1\",\"time_duration\":\"5\",\"question_answer\":\"{\\\"5\\\":\\\"A\\\",\\\"4\\\":\\\"A\\\",\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"A\\\"}\",\"paper_id\":\"1\",\"paper_name\":\"testing\",\"exam_type\":\"mock_test\"}'),
(285, '{\"student_id\":\"2\"}'),
(286, '{\"batch_id\":\"9\",\"student_id\":\"2\"}'),
(287, '{\"total_question\":\"5\",\"student_id\":\"2\",\"start_time\":\"04:05:12 PM\",\"admin_id\":\"1\",\"time_duration\":\"2\",\"question_answer\":\"{\\\"5\\\":\\\"A\\\",\\\"4\\\":\\\"A\\\",\\\"1\\\":\\\"A\\\",\\\"3\\\":\\\"A\\\",\\\"2\\\":\\\"A\\\"}\",\"paper_id\":\"2\",\"paper_name\":\"demo\",\"exam_type\":\"practice\"}'),
(288, '{\"student_id\":\"2\"}'),
(289, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(290, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(291, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(292, '{\"search\":\"\",\"start\":\"4\",\"length\":\"8\",\"limit\":\"3\"}'),
(293, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(294, '{\"search\":\"\",\"start\":\"4\",\"length\":\"8\",\"limit\":\"3\"}'),
(295, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\",\"student_id\":\"2\"}'),
(296, '{\"search\":\"\",\"start\":\"2\",\"length\":\"5\",\"limit\":\"3\",\"student_id\":\"2\"}'),
(297, '{\"student_id\":\"2\"}'),
(298, '{\"batch_id\":\"9\",\"student_id\":\"2\"}'),
(299, '{\"student_id\":\"2\"}'),
(300, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(301, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(302, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(303, '{\"search\":\"\",\"start\":\"4\",\"length\":\"8\",\"limit\":\"3\"}'),
(304, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(305, '{\"search\":\"\",\"start\":\"4\",\"length\":\"8\",\"limit\":\"3\"}'),
(306, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\"}'),
(307, '{\"search\":\"\",\"start\":\"4\",\"length\":\"8\",\"limit\":\"3\"}'),
(308, '{\"search\":\"\",\"start\":\"0\",\"length\":\"4\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(309, '{\"search\":\"\",\"start\":\"2\",\"length\":\"5\",\"limit\":\"3\",\"student_id\":\"1\"}'),
(310, '{\"student_id\":\"1\"}'),
(311, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(312, '{\"exam_type\":\"practice\",\"paper_name\":\"demo\",\"start_time\":\"11:43:43 AM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"5\",\"time_duration\":\"2\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"B\\\",\\\"3\\\":\\\"A\\\",\\\"4\\\":\\\"A\\\",\\\"5\\\":\\\"A\\\"}\",\"paper_id\":\"2\"}'),
(313, '{\"student_id\":\"1\"}'),
(314, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(315, '{\"exam_type\":\"mock_test\",\"paper_name\":\"Testing Mock\",\"start_time\":\"11:52:14 AM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"5\",\"time_duration\":\"12\",\"question_answer\":\"{\\\"1\\\":\\\"D\\\",\\\"2\\\":\\\"A\\\",\\\"3\\\":\\\"A\\\",\\\"4\\\":\\\"A\\\",\\\"5\\\":\\\"A\\\"}\",\"paper_id\":\"3\"}'),
(316, '{\"student_id\":\"1\"}'),
(317, '{\"student_id\":\"1\"}'),
(318, '{\"student_id\":\"1\"}'),
(319, '{\"student_id\":\"1\"}'),
(320, '{\"student_id\":\"1\"}'),
(321, '{\"student_id\":\"1\"}'),
(322, '{\"student_id\":\"1\"}'),
(323, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(324, '{\"student_id\":\"1\"}'),
(325, '{\"student_id\":\"1\"}'),
(326, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(327, '{\"student_id\":\"1\"}'),
(328, '{\"student_id\":\"1\"}'),
(329, '{\"student_id\":\"1\"}'),
(330, '{\"student_id\":\"1\"}'),
(331, '{\"student_id\":\"1\"}'),
(332, '{\"student_id\":\"1\"}'),
(333, '{\"student_id\":\"1\"}'),
(334, '{\"student_id\":\"1\"}'),
(335, '{\"student_id\":\"1\"}'),
(336, '{\"student_id\":\"1\"}'),
(337, '{\"student_id\":\"1\"}'),
(338, '{\"student_id\":\"1\"}'),
(339, '{\"student_id\":\"1\"}'),
(340, '{\"student_id\":\"1\"}'),
(341, '{\"student_id\":\"1\"}'),
(342, '{\"student_id\":\"1\"}'),
(343, '{\"student_id\":\"1\"}'),
(344, '{\"student_id\":\"1\"}'),
(345, '{\"student_id\":\"1\"}'),
(346, '{\"student_id\":\"1\"}'),
(347, '{\"student_id\":\"1\"}'),
(348, '{\"student_id\":\"1\"}'),
(349, '{\"student_id\":\"1\"}'),
(350, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(351, '{\"student_id\":\"1\"}'),
(352, '{\"student_id\":\"1\"}'),
(353, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(354, '{\"student_id\":\"1\"}'),
(355, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(356, '{\"student_id\":\"1\"}'),
(357, '{\"student_id\":\"1\"}'),
(358, '{\"student_id\":\"1\"}'),
(359, '{\"student_id\":\"1\"}'),
(360, '{\"student_id\":\"1\"}'),
(361, '{\"student_id\":\"1\"}'),
(362, '{\"student_id\":\"1\"}'),
(363, '{\"student_id\":\"1\"}'),
(364, '{\"student_id\":\"1\"}'),
(365, '{\"student_id\":\"1\"}'),
(366, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(367, '{\"student_id\":\"1\"}'),
(368, '{\"student_id\":\"1\"}'),
(369, '{\"student_id\":\"1\"}'),
(370, '{\"student_id\":\"1\"}'),
(371, '{\"student_id\":\"1\"}'),
(372, '{\"student_id\":\"1\"}'),
(373, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(374, '{\"student_id\":\"1\"}'),
(375, '{\"student_id\":\"1\"}'),
(376, '{\"student_id\":\"1\"}'),
(377, '{\"student_id\":\"1\"}'),
(378, '{\"student_id\":\"1\"}'),
(379, '{\"student_id\":\"1\"}'),
(380, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(381, '{\"student_id\":\"1\"}'),
(382, '{\"student_id\":\"1\"}'),
(383, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(384, '{\"student_id\":\"1\"}'),
(385, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(386, '{\"student_id\":\"1\"}'),
(387, '{\"student_id\":\"1\"}'),
(388, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(389, '{\"student_id\":\"1\"}'),
(390, '{\"student_id\":\"1\"}'),
(391, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(392, '{\"student_id\":\"1\"}'),
(393, '{\"student_id\":\"1\"}'),
(394, '{\"student_id\":\"1\"}'),
(395, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(396, '{\"student_id\":\"1\"}'),
(397, '{\"student_id\":\"1\"}'),
(398, '{\"student_id\":\"1\"}'),
(399, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(400, '{\"student_id\":\"1\"}'),
(401, '{\"student_id\":\"1\"}'),
(402, '{\"student_id\":\"1\"}'),
(403, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(404, '{\"student_id\":\"1\"}'),
(405, '{\"student_id\":\"1\"}'),
(406, '{\"student_id\":\"1\"}'),
(407, '{\"student_id\":\"1\"}'),
(408, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(409, '{\"student_id\":\"1\"}'),
(410, '{\"student_id\":\"1\"}'),
(411, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(412, '{\"student_id\":\"1\"}'),
(413, '{\"student_id\":\"1\"}'),
(414, '{\"student_id\":\"1\"}'),
(415, '{\"student_id\":\"1\"}'),
(416, '{\"student_id\":\"1\"}'),
(417, '{\"student_id\":\"1\"}'),
(418, '{\"student_id\":\"1\"}'),
(419, '{\"student_id\":\"1\"}'),
(420, '{\"student_id\":\"1\"}'),
(421, '{\"student_id\":\"1\"}'),
(422, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(423, '{\"student_id\":\"1\"}'),
(424, '{\"student_id\":\"1\"}'),
(425, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(426, '{\"student_id\":\"1\"}'),
(427, '{\"student_id\":\"1\"}'),
(428, '{\"student_id\":\"1\"}'),
(429, '{\"student_id\":\"1\"}'),
(430, '{\"student_id\":\"1\"}'),
(431, '{\"student_id\":\"1\"}'),
(432, '{\"student_id\":\"1\"}'),
(433, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(434, '{\"student_id\":\"1\"}'),
(435, '{\"student_id\":\"1\"}'),
(436, '{\"student_id\":\"1\"}'),
(437, '{\"student_id\":\"1\"}'),
(438, '{\"student_id\":\"1\"}'),
(439, '{\"student_id\":\"1\"}'),
(440, '{\"student_id\":\"1\"}'),
(441, '{\"student_id\":\"1\"}'),
(442, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(443, '{\"student_id\":\"1\"}'),
(444, '{\"student_id\":\"1\"}'),
(445, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(446, '{\"student_id\":\"1\"}'),
(447, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(448, '{\"student_id\":\"1\"}'),
(449, '{\"batch_id\":\"24\",\"student_id\":\"1\"}'),
(450, '{\"student_id\":\"1\"}'),
(451, '{\"student_id\":\"1\"}'),
(452, '{\"student_id\":\"1\"}'),
(453, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(454, '{\"student_id\":\"1\"}'),
(455, '{\"student_id\":\"1\"}'),
(456, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(457, '{\"student_id\":\"1\"}'),
(458, '{\"student_id\":\"1\"}'),
(459, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(460, '{\"student_id\":\"1\"}'),
(461, '{\"student_id\":\"1\"}'),
(462, '{\"student_id\":\"1\"}'),
(463, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(464, '{\"exam_type\":\"practice\",\"paper_name\":\"Inorganic Organic quizes\",\"start_time\":\"03:10:56 PM\",\"admin_id\":\"1\",\"student_id\":\"1\",\"total_question\":\"5\",\"time_duration\":\"20\",\"question_answer\":\"{\\\"1\\\":\\\"A\\\",\\\"2\\\":\\\"A\\\",\\\"3\\\":\\\"A\\\",\\\"4\\\":\\\"A\\\",\\\"5\\\":\\\"A\\\"}\",\"paper_id\":\"6\"}'),
(465, '{\"student_id\":\"1\"}'),
(466, '{\"student_id\":\"1\"}'),
(467, '{\"student_id\":\"1\"}'),
(468, '{\"student_id\":\"1\"}'),
(469, '{\"student_id\":\"1\"}'),
(470, '{\"student_id\":\"1\"}'),
(471, '{\"student_id\":\"1\"}'),
(472, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(473, '{\"student_id\":\"1\"}'),
(474, '{\"student_id\":\"1\"}'),
(475, '{\"batch_id\":\"9\",\"student_id\":\"1\"}'),
(476, '{\"student_id\":\"1\"}'),
(477, '{\"student_id\":\"1\"}'),
(478, '{\"batch_id\":\"9\",\"student_id\":\"1\"}');

-- --------------------------------------------------------

--
-- Table structure for table `term_condition_data`
--

CREATE TABLE `term_condition_data` (
  `id` int(11) NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `term_condition_data`
--

INSERT INTO `term_condition_data` (`id`, `description`) VALUES
(1, '&lt;p&gt;Suspendisse consectetur metus tellus, nec efficitur metus lobortis in. Fusce dapibus lacus sed sapien tincidunt dictum. Aliquam quisSuspendisse consectetur metus tellus, nec efficitur metus lobortis in. Fusce dapibus lacus sed sapien tincidunt dictum. Aliquam quisSuspendisse consectetur metus tellus, nec efficitur metus lobortis in. Fusce dapibus lacus sed sapien tincidunt dictum. Aliquam quisdsfdgf&amp;nbsp;&lt;/p&gt;\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `theme_color`
--

CREATE TABLE `theme_color` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `admin_themes` text NOT NULL,
  `teacher_themes` text NOT NULL,
  `student_themes` text NOT NULL,
  `frontend_themes` text NOT NULL,
  `login_themes` text NOT NULL,
  `status` bigint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `theme_color`
--

INSERT INTO `theme_color` (`id`, `admin_id`, `admin_themes`, `teacher_themes`, `student_themes`, `frontend_themes`, `login_themes`, `status`) VALUES
(1, 1, '{\"admin_primary\":\"#3d85c6\",\"admin_secondary\":\"white\",\"admin_accent\":\"red\",\"admin_text\":\"black\",\"admin_alternate\":\"orange\",\"admin_header\":\"#6aa84f\"}', '{\"teacher_primary\":\"#741b47\",\"teacher_secondary\":\"#d9d2e9\",\"teacher_accent\":\"#ff9900\",\"teacher_text\":\"#c27ba0\",\"teacher_alternate\":\"#741b47\",\"teacher_header\":\"#674ea7\"}', '{\"student_primary\":\"#6fa8dc\",\"student_secondary\":\"white\",\"student_accent\":\"#6aa84f\",\"student_text\":\"#f4cccc\",\"student_alternate\":\"#741b47\",\"student_header\":\"#ffd966\"}', '{\"frontend_primary\":\"#ff6e66\",\"frontend_secondary\":\"white\",\"frontend_accent\":\"#e06666\",\"frontend_text\":\"#00000a\",\"frontend_alternate\":\"red\",\"frontend_header\":\"blue\"}', '{\"login_primary\":\"#45818e\",\"login_secondary\":\"black\",\"login_alternate\":\"white\"}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `admin_id` text NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL COMMENT '1 - admin, 3 - teacher',
  `teach_education` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `teach_image` varchar(255) NOT NULL,
  `teach_batch` varchar(255) NOT NULL,
  `teach_subject` varchar(255) NOT NULL,
  `teach_gender` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `token` text NOT NULL,
  `brewers_check` varchar(500) NOT NULL,
  `super_admin` text NOT NULL,
  `access` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `admin_id`, `name`, `email`, `password`, `role`, `teach_education`, `teach_image`, `teach_batch`, `teach_subject`, `teach_gender`, `parent_id`, `status`, `token`, `brewers_check`, `super_admin`, `access`) VALUES
(19, '1', 'Charlotte Washington', 'vuxucarin@mailinator.com', '202cb962ac59075b964b07152d234b70', 3, 'Dolorum ipsa debiti', 'client3.png', '', '[\"3\",\"2\",\"1\"]', 'female', 1, 0, '1', 'ryHM3sFIZu', '', '{\"live_class\":\"1\",\"notice\":\"1\",\"assignment\":\"1\",\"extraclasses\":\"1\",\"doubtsask\":\"1\",\"video_lecture\":\"1\",\"course_content\":\"1\",\"question_manager\":\"1\",\"student_leave\":\"1\",\"student_manage\":\"1\",\"exam\":\"1\"}'),
(18, '1', 'Ravi', 'Ravi@gmail.com', '202cb962ac59075b964b07152d234b70', 3, 'MCA', '7.jpg', '24,21', '[\"7\",\"3\",\"2\",\"1\"]', 'male', 1, 1, '1', '4VNf5p6aTH', '', '{\"academics\":null,\"live_class\":\"1\",\"notice\":\"1\",\"assignment\":\"1\",\"extraclasses\":\"1\",\"doubtsask\":\"1\",\"video_lecture\":\"1\",\"question_manager\":\"1\",\"course_content\":\"1\",\"student_leave\":\"1\",\"student_manage\":\"1\",\"exam\":\"1\"}'),
(17, '16', '  g gg gbgg', 'hari@mailinator.com', '99b2ab7d7022136a8f09bb7afe001279', 3, 'mba', 'logo.jpg', '34', '[\"17\"]', 'female', 16, 1, '', '', '', '{\"live_class\":\"1\",\"notice\":null,\"assignment\":\"1\",\"extraclasses\":null,\"doubtsask\":null,\"video_lecture\":\"1\",\"course_content\":null,\"question_manager\":\"1\",\"student_leave\":\"1\",\"student_manage\":\"1\",\"exam\":null}'),
(8, '16', 'jon', 'jon@eacademy.com', '9bc65c2abec141778ffaa729489f3e87', 3, 'MSc', 'Entrevista-El-Pais-Carlos-Torres-BBVA-home-e1523695441593-1920x950.jpg', '5,8,10,15,16,17', '[\"2\",\"1\"]', 'male', 1, 1, '1', 'mpKG2qlLH6', '', ''),
(16, '1', 'Mikayla Byers', 'zuguwizy@mailinator.com', '202cb962ac59075b964b07152d234b70', 1, '', 'logo.jpg', '', '', '', 1, 1, '1', 'OTxKHe1caV', '', '{\"academics\":\"1\",\"gallary_manage\":\"1\",\"library_manager\":\"1\",\"question_manager\":\"1\",\"video_lecture\":\"1\",\"doubtsask\":\"1\",\"exam\":\"1\",\"teacher_manager\":\"1\",\"student_manage\":\"1\",\"enquiry\":\"1\"}'),
(10, '16', 'Maria', 'maria@eacademy.com', '202cb962ac59075b964b07152d234b70', 3, 'M.A.', 'teenager-1887364_960_7203.jpg', '10,24,21,9,8,5', '[\"9\",\"8\",\"7\"]', 'female', 1, 1, '1', 'IwTFWyZ9sO', '', ''),
(1, '1', 'admin', 'admin@eacademy.com', '202cb962ac59075b964b07152d234b70', 1, '', '', '', '', '', 0, 1, '1', 'O5RdBs1Qtu', '1', ''),
(11, '16', 'John', 'jimmy@eacademy.com', '202cb962ac59075b964b07152d234b70', 3, 'Computer Science', 'white_hat_hacker.jpg', '10,12,14,18,19,20,21,22,15', '[\"13\",\"12\",\"11\"]', 'male', 1, 1, '1', 'krRM2of7m8', '', ''),
(9, '16', 'Diana', 'diana@eacademy.com', '202cb962ac59075b964b07152d234b70', 3, 'MSc.', 'teaching-website1.jpeg', '9', '[\"4\",\"3\"]', 'female', 1, 1, '1', 'NKJDseEYdt', '', ''),
(13, '1', 'Brianna Edwards', 'lujugeq@mailinator.com', 'f3ed11bbdb94fd9ebdefbaf646ab94d3', 1, '', 'image_2022_09_19T09_53_01_900Z.png', '', '', '', 1, 1, '1', 'D9VcH8bvNf', '', '{\"academics\":\"1\",\"gallary_manage\":\"1\",\"library_manager\":null,\"question_manager\":null,\"video_lecture\":\"1\",\"doubtsask\":null,\"exam\":null,\"teacher_manager\":\"1\",\"student_manage\":null,\"enquiry\":null}'),
(21, '1', 'ravi', 'ravi.kushwah@pixelnx.com', '202cb962ac59075b964b07152d234b70', 1, '', 'image_2022_09_19T09_53_01_900Z1.png', '', '', '', 1, 1, '1', '9mfhKlHZDy', '', '{\"academics\":\"1\",\"gallary_manage\":\"1\",\"library_manager\":\"1\",\"question_manager\":\"1\",\"video_lecture\":\"1\",\"doubtsask\":\"1\",\"exam\":\"1\",\"teacher_manager\":\"1\",\"student_manage\":\"1\",\"enquiry\":\"1\"}'),
(26, '1', 'svfdg', 'fd@gmail.com', '202cb962ac59075b964b07152d234b70', 1, '', 'student_img11.png', '', '', '', 1, 1, '1', 'phyAUKJPd0', '', '{\"academics\":null,\"gallary_manage\":null,\"library_manager\":null,\"question_manager\":null,\"video_lecture\":null,\"doubtsask\":null,\"exam\":null,\"teacher_manager\":null,\"student_manage\":\"1\",\"enquiry\":null}'),
(27, '1', 'Martha Hernandez', 'xytabepo@mailinator.com', '202cb962ac59075b964b07152d234b70', 1, '', '1xeFM_CJOG5334ZPneNmkkWwSK8g78Ky_.jpg', '', '', '', 1, 0, '1', 'GY3VAmSd0K', '', '{\"academics\":\"1\",\"gallary_manage\":\"1\",\"library_manager\":\"1\",\"question_manager\":\"1\",\"video_lecture\":\"1\",\"doubtsask\":\"1\",\"exam\":\"1\",\"teacher_manager\":\"1\",\"student_manage\":\"1\",\"enquiry\":\"1\"}'),
(28, '1', 'jaya', 'jaya@gmail.com', '202cb962ac59075b964b07152d234b70', 3, 'bsc', 'client31.png', '31,24', '[\"7\",\"3\",\"2\",\"1\"]', 'female', 1, 1, '1', 'NM4mBRix1n', '', '{\"academics\":null,\"live_class\":\"1\",\"notice\":\"1\",\"assignment\":\"1\",\"extraclasses\":null,\"doubtsask\":null,\"video_lecture\":null,\"question_manager\":null,\"course_content\":null,\"student_leave\":null,\"student_manage\":null,\"exam\":\"1\"}'),
(29, '1', 'Jessica Torres', 'hicytud@mailinator.com', '202cb962ac59075b964b07152d234b70', 3, 'Enim aut et eos lib', 'carousal4.jpg', '30,5,32,24', '[\"3\",\"2\",\"1\"]', 'female', 1, 1, '1', 'b9nsy1X0oF', '', '{\"academics\":null,\"live_class\":\"1\",\"notice\":\"1\",\"assignment\":\"1\",\"extraclasses\":\"1\",\"doubtsask\":\"1\",\"video_lecture\":\"1\",\"question_manager\":\"1\",\"course_content\":\"1\",\"student_leave\":\"1\",\"student_manage\":\"1\",\"exam\":\"1\"}'),
(30, '1', 'create', 'create@mail.com', '202cb962ac59075b964b07152d234b70', 3, '', 'jp-team2-2.jpg', '32', '[\"7\",\"3\",\"2\",\"1\"]', 'male', 1, 1, '', '', '', '{\"live_class\":\"1\",\"notice\":\"1\",\"assignment\":\"1\",\"extraclasses\":\"1\",\"doubtsask\":\"1\",\"video_lecture\":\"1\",\"course_content\":\"1\",\"question_manager\":\"1\",\"student_leave\":\"1\",\"student_manage\":\"1\",\"exam\":\"1\"}'),
(31, '1', 'Elvis Hammond', 'serutoxipo@mailinator.com', 'f3ed11bbdb94fd9ebdefbaf646ab94d3', 3, 'Quia esse deleniti l', 'clien_thumb.jpg', '', '[\"2\",\"1\"]', 'other', 1, 1, '', '', '', '{\"live_class\":\"1\",\"notice\":\"1\",\"assignment\":\"1\",\"extraclasses\":\"1\",\"doubtsask\":\"1\",\"video_lecture\":\"1\",\"course_content\":\"1\",\"question_manager\":\"1\",\"student_leave\":\"1\",\"student_manage\":\"1\",\"exam\":null}'),
(32, '21', 'cdccdc', 'Harsh@mailinator.com', '202cb962ac59075b964b07152d234b70', 3, 'mca', '04.png', '', '[\"19\"]', 'female', 21, 1, '', '', '', '{\"live_class\":null,\"notice\":\"1\",\"assignment\":\"1\",\"extraclasses\":null,\"doubtsask\":\"1\",\"video_lecture\":\"1\",\"course_content\":null,\"question_manager\":null,\"student_leave\":null,\"student_manage\":null,\"exam\":null}'),
(33, '1', 'teacher hu', 'teacher@mailinator.com', '202cb962ac59075b964b07152d234b70', 3, 'bhaut jada', 'customer_image.png', '9', '[\"7\",\"3\",\"2\"]', 'male', 1, 1, '1', 'HYaB0USImq', '', '{\"academics\":null,\"live_class\":\"1\",\"notice\":\"1\",\"assignment\":\"1\",\"extraclasses\":\"1\",\"doubtsask\":\"1\",\"video_lecture\":\"1\",\"question_manager\":\"1\",\"course_content\":\"1\",\"student_leave\":\"1\",\"student_manage\":\"1\",\"exam\":\"1\"}'),
(34, '1', 'nameteacher', 'nikasgupt52@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 3, 'biker', 'customer_image1.png', '', '[\"7\",\"3\",\"2\",\"1\"]', 'male', 1, 1, '', '', '', '{\"live_class\":null,\"notice\":\"1\",\"assignment\":\"1\",\"extraclasses\":null,\"doubtsask\":null,\"video_lecture\":\"1\",\"course_content\":\"1\",\"question_manager\":null,\"student_leave\":null,\"student_manage\":\"1\",\"exam\":\"1\"}');

-- --------------------------------------------------------

--
-- Table structure for table `vacancy`
--

CREATE TABLE `vacancy` (
  `id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `last_date` date NOT NULL,
  `mode` varchar(255) NOT NULL,
  `files` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `added_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vacancy`
--

INSERT INTO `vacancy` (`id`, `title`, `description`, `start_date`, `last_date`, `mode`, `files`, `status`, `admin_id`, `added_at`) VALUES
(1, 'Eos facere et lorem ', 'Non reiciendis beata', '1979-10-08', '2017-06-08', 'Online', '', 1, 1, '2023-04-27 15:34:14'),
(2, 'cdccdcdscdcsdd', 'csdcsdsdsdcsdc', '2023-04-27', '2023-04-29', 'Online', '[\"logo_(1).png\"]', 1, 1, '2023-04-27 15:59:58');

-- --------------------------------------------------------

--
-- Table structure for table `verification_login`
--

CREATE TABLE `verification_login` (
  `id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `otp` text NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `video_lectures`
--

CREATE TABLE `video_lectures` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `batch` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `topic` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `url` varchar(255) NOT NULL,
  `video_type` varchar(255) NOT NULL,
  `preview_type` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `video_lectures`
--

INSERT INTO `video_lectures` (`id`, `admin_id`, `title`, `batch`, `topic`, `subject`, `description`, `url`, `video_type`, `preview_type`, `status`, `added_by`, `added_at`) VALUES
(23, 1, 'test', '21', 'Article', 'English', 'ddf', 'uploads/video/WhatsApp_Video_2022-07-04_at_12_40_32_PM.mp4', 'video', 'preview', 1, 1, '2022-07-07 14:07:30'),
(21, 1, 'fdd', '24', 'Article', 'English', 'fdfdf', '', 'embed', 'preview', 1, 1, '2022-07-05 11:52:46'),
(17, 13, 'AXx', '25', 'addition', 'math', 'asdasdasda', 'https://youtu.be/MUMCZZl9QCY', 'youtube', 'preview', 1, 13, '2022-05-04 15:57:52'),
(18, 13, 'adasdas', '25', 'addition', 'math', 'qweqwewqqweqwe', 'uploads/video/big_buck_bunny_720p_1mb.mp4', 'video', 'preview', 1, 13, '2022-05-04 15:58:53'),
(26, 1, 'deals with the properties', '9', 'Periodic table', 'Chemistry', 'Chemistry is the branch of science that deals with the properties, composition, and structure of elements and compounds, how they can change, and the energy that is released or absorbed when they change.', 'uploads/video/big_buck_bunny_720p_1mb11.mp4', 'video', 'preview', 1, 1, '2023-04-18 18:06:02'),
(24, 1, '3424224', '9', 'Periodic table', 'Chemistry', 'fgdfgfg', 'uploads/video/big_buck_bunny_720p_1mb1.mp4', 'video', 'preview', 1, 1, '2023-04-17 17:31:57'),
(25, 1, 'sdsdf', '9', 'Organic', 'Chemistry', 'cdcds', 'https://www.youtube.com/watch?v=NrzthCtKCho', 'youtube', 'preview', 1, 1, '2023-04-17 17:34:45'),
(27, 1, 'hii', '9', 'Periodic table', 'Chemistry', '', 'https://youtu.be/9xwazD5SyVg', 'youtube', 'preview', 1, 1, '2023-04-27 16:10:28');

-- --------------------------------------------------------

--
-- Table structure for table `views_notification_student`
--

CREATE TABLE `views_notification_student` (
  `n_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `notice_type` varchar(100) NOT NULL,
  `views_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `views_notification_student`
--

INSERT INTO `views_notification_student` (`n_id`, `student_id`, `notice_type`, `views_time`) VALUES
(1, 16, 'book_notes_paper', '2021-09-02 11:53:07'),
(2, 16, 'extraClass', '2021-09-02 11:53:30'),
(3, 16, 'homeWork', '2021-09-02 11:53:37'),
(4, 16, 'videoLecture', '2021-09-02 15:45:40'),
(5, 15, 'mockPaper', '2021-09-02 04:45:17'),
(6, 15, 'book_notes_paper', '2021-09-29 17:07:33'),
(7, 16, 'practicePaper', '2021-09-04 12:02:22'),
(8, 15, 'videoLecture', '2021-09-21 15:38:50'),
(9, 15, 'extraClass', '2021-09-04 18:27:52'),
(10, 27, 'notices', '2021-09-15 18:30:35'),
(11, 65, 'notices', '2021-09-22 18:43:10'),
(12, 65, 'practicePaper', '2021-09-22 06:44:19'),
(13, 65, 'vacancy', '2021-09-22 15:33:37'),
(14, 65, 'videoLecture', '2021-09-22 18:43:06'),
(15, 65, 'homeWork', '2021-09-22 18:43:52'),
(16, 65, 'extraClass', '2021-09-22 12:12:35'),
(17, 65, 'book_notes_paper', '2021-09-22 12:58:32'),
(18, 65, 'mockPaper', '2021-09-22 06:45:19'),
(19, 67, 'book_notes_paper', '2021-09-24 12:08:42'),
(20, 67, 'mockPaper', '2021-09-24 12:12:19'),
(21, 67, 'practicePaper', '2021-09-24 12:13:02'),
(22, 70, 'notices', '2021-09-24 16:58:30'),
(23, 85, 'notices', '2021-09-29 17:07:51'),
(24, 85, 'homeWork', '2021-09-29 17:08:16'),
(25, 85, 'practicePaper', '2021-10-01 05:44:15'),
(26, 85, 'mockPaper', '2021-10-01 05:52:11'),
(27, 85, 'vacancy', '2021-09-30 18:06:13'),
(28, 85, 'videoLecture', '2021-09-29 17:03:11'),
(29, 85, 'extraClass', '2021-09-29 17:08:35'),
(30, 85, 'book_notes_paper', '2021-09-29 17:09:36'),
(31, 87, 'book_notes_paper', '2021-09-29 17:12:46'),
(32, 15, 'homeWork', '2021-09-29 16:51:11'),
(33, 87, 'notices', '2021-09-29 17:21:34'),
(34, 87, 'mockPaper', '2021-09-29 05:42:47'),
(35, 87, 'extraClass', '2021-09-29 17:26:47'),
(36, 87, 'vacancy', '2021-09-29 18:06:32'),
(37, 87, 'practicePaper', '2021-09-29 05:59:20'),
(38, 87, 'videoLecture', '2021-09-29 17:13:18'),
(39, 15, 'practicePaper', '2021-09-29 05:28:16'),
(40, 43, 'practicePaper', '2022-05-03 02:52:07'),
(41, 43, 'mockPaper', '2022-05-03 02:53:45'),
(42, 90, 'vacancy', '2022-05-04 15:18:31'),
(43, 90, 'book_notes_paper', '2022-05-04 15:59:28'),
(44, 90, 'extraClass', '2022-05-04 15:59:02'),
(45, 90, 'videoLecture', '2022-05-04 15:59:14'),
(46, 90, 'practicePaper', '2022-05-04 03:06:00'),
(47, 90, 'mockPaper', '2022-05-04 03:16:10'),
(48, 90, 'homeWork', '2022-05-04 15:16:38'),
(49, 90, 'notices', '2022-05-04 15:59:17'),
(50, 93, 'mockPaper', '2022-05-04 05:52:30'),
(51, 93, 'homeWork', '2022-05-05 14:48:57'),
(52, 93, 'vacancy', '2022-05-05 14:49:02'),
(53, 93, 'practicePaper', '2022-05-05 02:49:09'),
(54, 22, 'practicePaper', '2023-02-09 11:12:23'),
(55, 22, 'vacancy', '2023-02-09 23:12:33'),
(56, 22, 'homeWork', '2023-02-09 23:12:35'),
(57, 22, 'videoLecture', '2023-02-09 23:12:45'),
(58, 22, 'notices', '2023-02-09 23:12:49'),
(59, 22, 'book_notes_paper', '2023-02-09 23:13:00'),
(60, 6, 'notices', '2023-04-18 14:44:48'),
(61, 6, 'videoLecture', '2023-04-24 16:06:25'),
(62, 6, 'practicePaper', '2023-04-24 04:42:55'),
(63, 6, 'vacancy', '2023-04-24 16:06:37'),
(64, 6, 'book_notes_paper', '2023-04-17 12:30:18'),
(65, 6, 'homeWork', '2023-04-17 12:30:25'),
(66, 6, 'mockPaper', '2023-04-25 12:39:26'),
(67, 1, 'mockPaper', '2023-04-29 03:10:40'),
(68, 1, 'practicePaper', '2023-04-29 03:10:54'),
(69, 1, 'videoLecture', '2023-04-27 15:15:18'),
(70, 1, 'homeWork', '2023-04-26 12:37:35'),
(71, 1, 'notices', '2023-04-27 14:43:32'),
(72, 2, 'notices', '2023-04-27 15:30:11'),
(73, 2, 'mockPaper', '2023-04-27 03:50:42'),
(74, 2, 'practicePaper', '2023-04-27 11:26:18'),
(75, 2, 'extraClass', '2023-04-27 15:49:36'),
(76, 2, 'vacancy', '2023-04-27 16:00:09'),
(77, 2, 'videoLecture', '2023-04-27 16:02:19'),
(78, 2, 'book_notes_paper', '2023-04-27 16:03:24');

-- --------------------------------------------------------

--
-- Table structure for table `zoom_api_credentials`
--

CREATE TABLE `zoom_api_credentials` (
  `id` int(11) NOT NULL,
  `android_api_key` varchar(250) NOT NULL,
  `android_api_secret` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zoom_api_credentials`
--

INSERT INTO `zoom_api_credentials` (`id`, `android_api_key`, `android_api_secret`) VALUES
(1, 'alfa', 'alfa one');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_versions`
--
ALTER TABLE `app_versions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `batches`
--
ALTER TABLE `batches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `batch_category`
--
ALTER TABLE `batch_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Indexes for table `batch_fecherd`
--
ALTER TABLE `batch_fecherd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batch_subcategory`
--
ALTER TABLE `batch_subcategory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `batch_subjects`
--
ALTER TABLE `batch_subjects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `batch_id` (`batch_id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_comments`
--
ALTER TABLE `blog_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_comments_reply`
--
ALTER TABLE `blog_comments_reply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_pdf`
--
ALTER TABLE `book_pdf`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `certificate`
--
ALTER TABLE `certificate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `certificate_setting`
--
ALTER TABLE `certificate_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chapters`
--
ALTER TABLE `chapters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `enquiry`
--
ALTER TABLE `enquiry`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `exams`
--
ALTER TABLE `exams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `extra_classes`
--
ALTER TABLE `extra_classes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `extra_class_attendance`
--
ALTER TABLE `extra_class_attendance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `frontend_details`
--
ALTER TABLE `frontend_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `general_settings`
--
ALTER TABLE `general_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homeworks`
--
ALTER TABLE `homeworks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `jetsi_setting`
--
ALTER TABLE `jetsi_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_management`
--
ALTER TABLE `leave_management`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`teacher_id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `live_class_history`
--
ALTER TABLE `live_class_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `live_class_setting`
--
ALTER TABLE `live_class_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mock_result`
--
ALTER TABLE `mock_result`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `notes_pdf`
--
ALTER TABLE `notes_pdf`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `notices`
--
ALTER TABLE `notices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `old_paper_pdf`
--
ALTER TABLE `old_paper_pdf`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `practice_result`
--
ALTER TABLE `practice_result`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `privacy_policy_data`
--
ALTER TABLE `privacy_policy_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `site_details`
--
ALTER TABLE `site_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `student_doubts_class`
--
ALTER TABLE `student_doubts_class`
  ADD PRIMARY KEY (`doubt_id`);

--
-- Indexes for table `student_payment_history`
--
ALTER TABLE `student_payment_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sudent_batchs`
--
ALTER TABLE `sudent_batchs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_data`
--
ALTER TABLE `temp_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `term_condition_data`
--
ALTER TABLE `term_condition_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `theme_color`
--
ALTER TABLE `theme_color`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `vacancy`
--
ALTER TABLE `vacancy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `video_lectures`
--
ALTER TABLE `video_lectures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `views_notification_student`
--
ALTER TABLE `views_notification_student`
  ADD PRIMARY KEY (`n_id`);

--
-- Indexes for table `zoom_api_credentials`
--
ALTER TABLE `zoom_api_credentials`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_versions`
--
ALTER TABLE `app_versions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `batches`
--
ALTER TABLE `batches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `batch_category`
--
ALTER TABLE `batch_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `batch_fecherd`
--
ALTER TABLE `batch_fecherd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `batch_subcategory`
--
ALTER TABLE `batch_subcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `batch_subjects`
--
ALTER TABLE `batch_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=252;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `blog_comments`
--
ALTER TABLE `blog_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blog_comments_reply`
--
ALTER TABLE `blog_comments_reply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `book_pdf`
--
ALTER TABLE `book_pdf`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `certificate`
--
ALTER TABLE `certificate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `certificate_setting`
--
ALTER TABLE `certificate_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `chapters`
--
ALTER TABLE `chapters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `enquiry`
--
ALTER TABLE `enquiry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exams`
--
ALTER TABLE `exams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `extra_classes`
--
ALTER TABLE `extra_classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `extra_class_attendance`
--
ALTER TABLE `extra_class_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `facilities`
--
ALTER TABLE `facilities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `frontend_details`
--
ALTER TABLE `frontend_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `general_settings`
--
ALTER TABLE `general_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `homeworks`
--
ALTER TABLE `homeworks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jetsi_setting`
--
ALTER TABLE `jetsi_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `leave_management`
--
ALTER TABLE `leave_management`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `live_class_history`
--
ALTER TABLE `live_class_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `live_class_setting`
--
ALTER TABLE `live_class_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `mock_result`
--
ALTER TABLE `mock_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `notes_pdf`
--
ALTER TABLE `notes_pdf`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `notices`
--
ALTER TABLE `notices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `old_paper_pdf`
--
ALTER TABLE `old_paper_pdf`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `practice_result`
--
ALTER TABLE `practice_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `privacy_policy_data`
--
ALTER TABLE `privacy_policy_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `site_details`
--
ALTER TABLE `site_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `student_doubts_class`
--
ALTER TABLE `student_doubts_class`
  MODIFY `doubt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `student_payment_history`
--
ALTER TABLE `student_payment_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `sudent_batchs`
--
ALTER TABLE `sudent_batchs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `temp_data`
--
ALTER TABLE `temp_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=479;

--
-- AUTO_INCREMENT for table `term_condition_data`
--
ALTER TABLE `term_condition_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `theme_color`
--
ALTER TABLE `theme_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `vacancy`
--
ALTER TABLE `vacancy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `video_lectures`
--
ALTER TABLE `video_lectures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `views_notification_student`
--
ALTER TABLE `views_notification_student`
  MODIFY `n_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `zoom_api_credentials`
--
ALTER TABLE `zoom_api_credentials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

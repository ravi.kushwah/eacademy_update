
<?php 
$lang['ltr_dashboard'] = "لوحة التحكم";
$lang['ltr_theme_coloroption'] = "خيار لون السمة";
$lang['ltr_admin_dashboard'] = "لوحة تحكم المشرف";
$lang['ltr_teacher_dashboard'] = "لوحة تحكم المعلم";
$lang['ltr_student_dashboard'] = "لوحة تحكم الطالب";
$lang['ltr_alternate_color'] = "لون بديل";
$lang['ltr_secondary_color'] = "اللون الثانوي";
$lang['ltr_primary_color'] = "اللون الأساسي";
$lang['ltr_edit_icon_color'] = "تحرير لون الرمز";
$lang['ltr_active_button'] = "زر نشط";
$lang['ltr_alternate_color'] = "لون بديل";
$lang['ltr_delete_icon_color'] = "حذف لون الرمز";
$lang['ltr_frontend_Web'] = "الويب الأمامي";
$lang['ltr_academics'] = "أكاديميون";
$lang['ltr_batch_manager'] = "مدير الدفعة";
$lang['ltr_notice_manager'] = "إشعار مدير";
$lang['ltr_subject_manager'] = "مدير الموضوع";
$lang['ltr_our_location'] = "موقعك";
$lang['ltr_question_manager'] = "مدير الأسئلة";
$lang['ltr_upcoming_exams_manager'] = "مدير الامتحانات القادمة";
$lang['ltr_upcoming_exams'] = "الامتحانات القادمة";
$lang['ltr_live_class'] = "لايف كلاس";
$lang['ltr_live_class_history'] = "تاريخ الفئة الحية";
$lang['ltr_student_manager'] = "مدير الطلاب";
$lang['ltr_add_student'] = "أضف طالب";
$lang['ltr_manage_students'] = "إدارة الطلاب";
$lang['ltr_manage_student_leave'] = "إدارة إجازة الطالب";
$lang['ltr_payment_history'] = "تاريخ الدفع";
$lang['ltr_teacher_manager'] = "مدير المعلم";
$lang['ltr_extra_classes'] = "Extra Classes";
$lang['ltr_manage_teachers'] = "إدارة المعلمين";
$lang['ltr_manage_teacher_leave'] = "إدارة إجازة المعلم";
$lang['ltr_exam'] = "امتحان";
$lang ['ltr_paper_manager'] = "مدير الورق";
$lang ['ltr_create_paper'] = "إنشاء ورقة";
$lang ['ltr_manage_paper'] = "إدارة الورق";
$lang ['ltr_gallery_manager'] = "مدير المعرض";
$lang ['ltr_video_lecture_manager'] = "مدير محاضرات الفيديو";
$lang['ltr_practice_result'] = "نتيجة الممارسة";
$lang['ltr_mock_test_result'] = "نتيجة اختبار وهمية";
$lang['ltr_video_lecture'] = "محاضرة فيديو";
$lang['ltr_login_signup_theme_option'] = "تسجيل الدخول والاشتراك ونسي خيار لون موضوع كلمة المرور";
$lang['ltr_enquiry'] = "استفسار";
$lang['ltr_enquiry_manager'] = "مدير الاستفسار";
$lang['ltr_set_time_zone'] = "تعيين المنطقة الزمنية";
$lang['ltr_frontend_settings'] = "إعدادات الواجهة الأمامية";
$lang['ltr_site_settings'] = "إعدادات الموقع";
$lang['ltr_home_page'] = "الصفحة الرئيسية";
$lang['ltr_about_page'] = "حول الصفحة";
$lang['ltr_course_page'] = "صفحة الدورة التدريبية";
$lang['ltr_course_manager'] = "مدير الدورة";
$lang['ltr_facility_manager'] = "مدير المرافق";
$lang['ltr_facility_page'] = "صفحة المنشأة";
$lang['ltr_contact_page'] = "صفحة الاتصال";
$lang['ltr_privacy_policy'] = "سياسة الخصوصية";
$lang['ltr_general_settings'] = "الإعدادات العامة";
$lang['ltr_payment_settings'] = "إعدادات الدفع";
$lang['ltrlangae_settings'] = "إعدادات اللغة";
$lang['ltr_certificate'] = "شهادة";
$lang['ltr_Total_sale'] = "إجمالي البيع";
$lang['ltr_offline'] = "غير متصل على الانترنت";
$lang['ltr_online'] = "عبر الإنترنت";
$lang['ltr_Total_Amount'] = "المبلغ الإجمالي";
$lang['ltr_Intitute'] = "تفاصيل المعهد";
$lang['ltr_Intitute_name'] = "اسم المعهد";
$lang['ltr_Intitute_logo'] = "شعار المعهد";
$lang['ltr_Intitute_add'] = "أضف المعهد";
$lang['ltr_Intitute_edit'] = "تحرير المعهد";
$lang['ltr_duplicate_email'] = "تكرار البريد الإلكتروني يرجى استخدام أي بريد إلكتروني آخر ";
$lang['ltr_Course_purchase'] = "لم تقم بشراء أي دورة حتى الآن ، فقط انقر فوق استعراض الدورة التدريبية للشراء.";
// صفحة لوحة القيادة
$lang['ltr_total_students'] = "مجموع الطلاب";
$lang['ltr_present'] = "حاضر";
$lang['ltr_absent'] = "غائب";
$lang['ltr_total_batches'] = "إجمالي الدُفعات";
$lang['ltr_active'] = "نشيط";
$lang['ltr_inactive'] = "غير نشط";
$lang['ltr_total_questions'] = "إجمالي الأسئلة";
$lang['ltr_vimp'] = "فيمب";
$lang['ltr_imp'] = "عفريت";
$lang['ltr_leave_request'] = "طلب مغادرة";
$lang['ltr_student'] = "طالب";
$lang['ltr_teacher'] = "مدرس";
$lang['ltr_select_teacher'] = "حدد مدرس";
$lang['ltr_student_performance'] = "أداء الطالب";
$lang['ltr_good'] = "جيد";
$lang['ltr_average'] = "متوسط";
$lang['ltr_poor'] = "فقير";
$lang['ltr_choose_want_anage'] = "اختر ما تريد إدارته";
$lang['ltr_send_mail_user'] = "إرسال هذه التفاصيل على البريد الإلكتروني ";
$lang['ltr_role'] = "دور";
$lang['ltr_revanue'] = "تفاصيل الإيرادات";
//مدير الدفعة

$lang['ltr_batch_name'] = "اسم الدفعة";
$lang['ltr_start_date'] = "تاريخ البدء";
$lang['ltr_end_date'] = "تاريخ الانتهاء";
$lang['ltr_batch_time'] = "وقت الدفعة";

$lang['ltr_status'] = "الحالة";

$lang['ltr_batch_type'] = "نوع الدفعة";
$lang['ltr_batch_price'] = "سعر الدفعة";
$lang['ltr_free'] = "مجاني";

$lang['ltr_subject_s'] = "الموضوع";
$lang['ltr_select_subject'] = "تحديد الموضوع";
$lang['ltr_select_subjects'] = "تحديد الموضوعات";

$lang['ltr_select_chapter'] = "تحديد الفصل";
$lang['ltr_assign_subject'] = "تعيين الموضوع";

$lang['ltr_edit_batch'] = "تحرير الدفعة";
$lang['ltr_change_password'] = "تغيير كلمة المرور";

$lang['ltr_logout'] = "تسجيل الخروج";
$lang['ltr_batch_no_data'] = 'لم تقم بإنشاء أي دفعة بعد. لا تتردد في إنشاء دفعة جديدة من خلال النقر على زر "دفعة جديدة" ويرجى التأكد من إضافة مواضيع موضوعهم ومعلمهم قبل إضافة دفعة جديدة.';
// إدارة الإشعار
$lang['ltr_add_notice'] = "إضافة إشعار";
$lang['ltr_notice_no_data'] = 'لم تقم بإضافة أي إشعار بعد ، لا تتردد في إضافة إشعار بالنقر فوق الزر "إضافة إشعار.';
$lang['ltr_notice_title'] = "عنوان الإشعار ";

$lang['ltr_both'] = "كلاهما";
$lang['ltr_type_notice_here'] = "نوع الإشعار هنا";
$lang['ltr_notice_description'] = "وصف الإشعار";
$lang['ltr_common_notice'] = "إشعار مشترك";
$lang['ltr_student_notice'] = "إشعار الطالب";
$lang['ltr_teacher_notice'] = "إشعار المعلم";
$lang['ltr_title'] = "العنوان";

//إدارة الموضوع
$lang['ltr_add_subject'] = "إضافة موضوع";
$lang['ltr_subject_name'] = "اسم الموضوع";

$lang['ltr_chapters_name'] = "اسم الفصول";

$lang['ltr_add_chapter'] = "إضافة فصل";
$lang['ltr_chsapter_note'] = "إضافة فصل متعدد مفصول بفاصلة ( ، ).";
$lang['ltr_chsapter_no_data'] = 'لم تقم بإضافة أي موضوع بعد ، لا تتردد في إضافة الموضوع بالنقر فوق الزر "إضافة موضوع.';
//إدارة الأسئلة
$lang['ltr_add_question'] = "إضافة سؤال";
$lang['ltr_question_no_data_admin'] = 'لم تقم بإضافة أي سؤال حتى الآن ، لا تتردد في إضافة سؤال بالنقر فوق الزر "إضافة سؤال. & يرجى التأكد من أنك قد أضفت بالفعل موضوعًا للمتابعة.';
$lang['ltr_question_no_data_teacher'] = 'لم تقم بإضافة أي سؤال حتى الآن ، لا تتردد في إضافة سؤال بالنقر فوق الزر "إضافة سؤال.';
$lang['ltr_single_question'] = "سؤال واحد";
$lang['ltr_question'] = "سؤال";

$lang['ltr_right_answer'] = "الإجابة الصحيحة";

$lang['ltr_bulk_upload'] = "تحميل السائبة";

$lang['ltr_download_demo_file'] = "تنزيل ملف تجريبي";
$lang['ltr_upload_question'] = "تحميل سؤال";

$lang['ltr_edit_question'] = "اختبار السؤال";
$lang['ltr_update_question'] = "تحديث السؤال";
$lang['ltr_search'] = "بحث";

//إدارة الشواغر
$lang['ltr_add_upcoming_exam'] = "إضافة اختبار قادم";

$lang['ltr_offline_mode'] = "وضع عدم الاتصال";

$lang['ltr_application_mode'] = "وضع التطبيق";
$lang['ltr_upload_files'] = "تحميل الملفات";
$lang['ltr_upload_files_type'] = "تحميل الملف يسمح فقط jpg و png و pdf و doc و docx.";

$lang['ltr_notification'] = "إشعار";

$lang['ltr_upcoming_no_data_student'] = "لا يوجد اختبار قادم هنا للعرض.";
$lang['ltr_upcoming_no_data_admin'] = 'لم تقم بإضافة أي اختبار حتى الآن ، لا تتردد في إضافة اختبار بالنقر فوق الزر "إضافة اختبار قادم.';
$lang['ltr_last_date'] = "آخر تاريخ";

//الطبقة الحية

$lang['ltr_live_no_data'] = 'لم تقم بإضافة أي فئة مباشرة بعد ، لا تتردد في إضافة فئة مباشرة بالنقر فوق الزر "إضافة فئة مباشرة. يرجى قراءة التعليمات من هنا.';

$lang['ltr_meeting_number'] = "رقم الاجتماع";

$lang['ltr_live_his_no_data'] = "لا توجد أي بيانات متاحة للعرض.";
//إدارة المعرض

$lang['ltr_add_image'] = "إضافة صورة / فيديو";

$lang['ltr_video'] = "فيديو";

$lang['ltr_gallery_no_data'] = 'لم تقم بإضافة أي عنصر بعد ، لا تتردد في إضافة صورة / فيديو بالنقر فوق الزر "إضافة صورة / فيديو.';
//إدارة الفيديو
$lang['ltr_video_no_data_admin'] = 'لم تقم بإضافة أي فيديو بعد ، لا تتردد في إضافة مقاطع فيديو بالنقر فوق الزر "إضافة فيديو.';
$lang['ltr_video_no_data_student'] = 'لا توجد محاضرة فيديو متاحة للعرض.';
$lang['ltr_video_no_data_teacher'] = 'لم تقم بإضافة أي فيديو حتى الآن ، لا تتردد في إضافة محاضرات فيديو بالنقر فوق الزر "إضافة فيديو.';
//إدارة الاستفسار

$lang['ltr_set_note'] = "يرجى تحديد المنطقة الزمنية الصحيحة ، وسوف تنعكس في توقيتات ورقة امتحان الطلاب.";
//إعدادات الموقع

$lang['ltr_favicon_size'] = "البعد الموصى به 32x32 px.";
$lang['ltr_site_logo_size'] = "البعد الموصى به 198x44 px.";
$lang['ltr_minisite_logo_size'] = "البعد الموصى به 25X35 px.";
$lang['ltr_preloader_size'] = "يجب أن يكون التحميل المسبق بتنسيق GIF.";
$lang['ltr_site_title'] = "عنوان الموقع";
$lang['ltr_site_author_name'] = "اسم مؤلف الموقع";
$lang['ltr_author_name'] = "اسم المؤلف";
$lang['ltr_site_keywords'] = "الكلمات الرئيسية للموقع";
$lang['ltr_site_description'] = "وصف الموقع";
$lang['ltr_site_keywords_note'] = "مفصولة بفاصلة ( ، )";
$lang['ltr_enrollment_word'] = "وثيقة التسجيل";
$lang['ltr_copyright_text'] = "نص حقوق الطبع والنشر";
$lang['ltr_enrollment_word_note'] = "يجب أن تكون الكلمة 3-5 أحرف وفي رأس المال ( ex - ACAD ).";
$lang['ltr_visit_site'] = "زيارة الموقع";
//صفحة الرئيسية 

$lang['ltr_header_button'] = "زر الرأس / العميل";

$lang['ltr_header_buttons'] = "زر الرأس";
$lang['ltr_trusted_teachers'] = "المعلمون الموثوق بهم";
$lang['ltr_years_history'] = "سنوات التاريخ";
$lang['ltr_selected_students'] = "الطلاب المختارون";

$lang['ltr_add_positions'] = "إضافة مواقع";
$lang['ltr_manage_testimonials'] = "إدارة الشهادات";
$lang['ltr_add_their_feedbacks'] = "إضافة ملاحظاتهم";
$lang['ltr_teacher_section'] = "قسم المعلم";

$lang['ltr_button_url'] = "عنوان URL للزر";
$lang['ltr_client_images'] = "صور العميل";
$lang['ltr_upload_multiple_files'] = "تحميل ملفات متعددة";
//حول الصفحة
$lang['ltr_first_section'] = "القسم الأول";

$lang['ltr_first_image'] = "الصورة الأولى";
$lang['ltr_second_image'] = "الصورة الثانية";

$lang['ltr_third_section'] = "القسم الثالث";

$lang['ltr_no_of_courses'] = "لا. من الدورات";
$lang['ltr_nu_of_courses'] = "عدد الدورات التدريبية التي سيتم عرضها";
//إدارة الدورة 

$lang['ltr_course_duration'] = "مدة الدورة";
$lang['ltr_class_size'] = "حجم الفئة";
$lang['ltr_time_duration'] = "المدة الزمنية";

$lang['ltr_class_time_duration'] = "مدة الفصل";

$lang['ltr_facility_manage'] = "إدارة المرافق";
$lang['ltr_icon_note'] = 'ثم انسخ قيمة الفئة من علامة < i > والصق هنا ( ex:" icofont-address-book ").';
$lang['ltr_no_of_facilities'] = "لا. من المرافق";
$lang['ltr_nu_of_facilities'] = "عدد التسهيلات التي يجب إظهارها";
//صفحة الاتصال
$lang['ltr_contact_settings'] = "إعدادات الاتصال";
$lang['ltr_form_heading'] = "عنوان النموذج";
$lang['ltr_mobile_number'] = "رقم الجوال";

$lang['ltr_address'] = "العنوان";
//إعدادات الدفع
$lang['ltr_payment_type'] = "نوع الدفع";

$lang['ltr_secret_key'] = "المفتاح السري";

$lang['ltr_sandbox_accounts'] = "حسابات صندوق البريد";
//إضافة طالب 
$lang['ltr_PERSONAL_INFORMATION'] = "معلومات شخصية";
$lang['ltr_father_name'] = "اسم الأب";
$lang['ltr_father_occupation'] = "احتلال الأب";

$lang['ltr_other'] = "أخرى";
$lang['ltr_date_birth'] = "تاريخ الميلاد";

$lang['ltr_CONTACT_INFORMATION'] = "معلومات الاتصال";
$lang['ltr_contact_number'] = "رقم الاتصال";

$lang['ltr_year'] = "السنة";

$lang['ltr_admission_date'] = "تاريخ القبول";
$lang['ltr_attendance'] = "Attendance";
$lang['ltr_extra_class_attendance'] = "إضافية فئة إضافية";

$lang['ltr_unpaid'] = "غير مدفوع";
$lang['ltr_add_regular_attendance'] = "إضافة حضور منتظم";
$lang['ltr_add_extra_class_attendance'] = "إضافة حضور إضافي للفئة";

$lang['ltr_practice_paper_progress_report'] = "تقرير تقدم ورقة الممارسة";
$lang['ltr_progress_report_no'] = "لا يوجد تقرير متاح للعرض.";
$lang['ltr_mock_paper_progress_report'] = "تقرير تقدم ورقة اختبار وهمية";
$lang['ltr_percentage'] = "النسبة المئوية";

$lang['ltr_student_progress'] = "تقدم الطالب";
$lang['ltr_student_academic_record'] = "السجل الأكاديمي للطلاب";
$lang['ltr_student_details'] = "تفاصيل الطالب";

$lang['ltr_add_doubts_date_class'] = "فئة الجدول الزمني";
$lang['ltr_student_description'] = "وصف الطالب";
$lang['ltr_student_name'] = "اسم الطالب";
$lang['ltr_appointment_date'] = "تاريخ الجدول";

$lang['ltr_enter_new_password'] = "أدخل كلمة مرور جديدة";
$lang['ltr_confirm_new_password'] = "تأكيد كلمة مرور جديدة";
$lang['ltr_total_dates'] = "التواريخ الإجمالية";
$lang['ltr_transaction_id'] = "معرف المعاملة";
$lang['ltr_payment_date'] = "تاريخ الدفع";
$lang['ltr_no_record'] = "لا يوجد سجل متاح للعرض.";
$lang['ltr_select_status'] = "تحديد الحالة";

$lang['ltr_incomplete'] = "غير كامل";
$lang['ltr_view_progress'] = "عرض التقدم";
$lang['ltr_add_extra_class'] = "إضافة فئة إضافية";
$lang['ltr_complete_date_time'] = "وقت التاريخ الكامل";
$lang['ltr_extra_no_data'] = "لا توجد أي فئة إضافية هنا لإظهارها.";
$lang['ltr_extra_no_data_admin'] = "لم تقم بإضافة أي فئة إضافية حتى الآن ، فلا تتردد في إضافة فئة إضافية بالنقر فوق الزر إضافة فئة إضافية ، ويرجى التأكد من أنك قمت بالفعل بإضافة مدرس للمتابعة.";
$lang['ltr_teacher_name'] = "اسم المعلم";
$lang['ltr_apply_date'] = "تاريخ التقديم";
$lang['ltr_from_date'] = "من التاريخ";
$lang['ltr_to_date'] = "حتى الآن";

$lang['ltr_teacher_no_data'] = "لم تقم بإضافة أي معلم حتى الآن لا تتردد في إضافة المعلم بالنقر فوق الزر إضافة معلم & يرجى التأكد من أنك قمت بالفعل بإضافة مواضيع للمتابعة.";
$lang['ltr_select_gender'] = "تحديد الجنس";
$lang['ltr_teacher_image'] = "صورة المعلم";

$lang['ltr_teacher_details'] = "تفاصيل المعلم";

$lang['ltr_class_dates'] = "تواريخ الفصل";

$lang['ltr_complete_date'] = "تاريخ الإكمال";
$lang['ltr_teacher_academic_record'] = "السجل الأكاديمي للمعلم";
$lang['ltr_student_image'] = "صورة الطالب";
$lang['ltr_login'] = "تسجيل الدخول";
$lang['ltr_students_no_data_te'] = "لا يوجد سجل متاح للعرض.";
$lang['ltr_students_no_data'] = "لا يوجد سجل متاح للعرض";
// رسالة
$lang['ltr_status_msg'] = "تم تغيير الحالة بنجاح.";
$lang['ltr_something_msg'] = "حدث خطأ ، يرجى المحاولة مرة أخرى.";
$lang['ltr_matching_msg'] = "لم يتم العثور على سجلات مطابقة.";
$lang['ltr_live_class_msg'] = "لا توجد فئة مباشرة متاحة بدلاً من ذلك.";
$lang['ltr_cant_msg'] = "لا يمكن حذفه ، أضف موضوعًا آخر لحذفه.";
$lang['ltr_are_you_msg'] = "هل أنت متأكد أنك تريد الحذف?";
$lang['ltr_are_you_so_msg'] = "هل أنت متأكد";
$lang['ltr_edit_course'] = "تعديل الدورة";
$lang['ltr_add_new_exam'] = "إضافة اختبار جديد";
$lang['ltr_end_date_msg'] = "يجب أن يكون تاريخ الانتهاء أكبر من تاريخ البدء.";
$lang['ltr_subject_msg'] = "إضافة موضوع واحد على الأقل.";
$lang['ltr_characters_msg'] = "لا يمكنك الحصول على أكثر من 50 حرفًا هنا.";
$lang['ltr_password_student_msg'] = "معرف التسجيل وكلمة المرور للطالب";
$lang['ltr_add_another_student'] = "إضافة طالب آخر";
$lang['ltr_select_batch_msg'] = "يرجى تحديد الدفعة.";
$lang['ltr_changed_batch_msg'] = "تم تغيير الدفعة بنجاح.";
$lang['ltr_changed_password_for_msg'] = "تغيير كلمة المرور لـ";
$lang['ltr_confirm_password_msg'] = "يجب أن تكون كلمة المرور وتأكيد كلمة المرور هي نفسها.";
$lang['ltr_password_msg'] = "تم تغيير كلمة المرور بنجاح.";
$lang['ltr_subject_name_msg'] = "الرجاء إدخال اسم الموضوع.";
$lang['ltr_letters_characters_msg'] = "يُسمح فقط بالأحرف والشرطات والأرقام ، ولا يُسمح بالأحرف الخاصة.";
$lang['ltr_subject_updated_msg'] = "تم تحديث الموضوع بنجاح.";
$lang['ltr_subject_add_msg'] = "تمت إضافة الموضوع بنجاح.";
$lang['ltr_subject_exists_msg'] = "اسم الموضوع موجود بالفعل.";
$lang['ltr_subject_delete_alert_msg'] = "ستحذف جميع الفصول والأسئلة حول هذا الموضوع?";
$lang['ltr_atleast_chapter_msg'] = "يرجى إدخال اسم فصل واحد على الأقل.";
$lang['ltr_add_chapter_msg'] = "تمت إضافة الفصول بنجاح.";
$lang['ltr_exists_chapter_msg'] = "اسم الفصل موجود بالفعل.";
$lang['ltr_chapter_name_msg'] = "الرجاء إدخال اسم الفصل.";
$lang['ltr_chapter_updated_msg'] = "تم تحديث الفصل بنجاح.";
$lang['ltr_chapter_delete_msg'] = "ستحذف جميع أسئلة هذا الفصل?";
$lang['ltr_loading_msg'] = "تحميل";
$lang['ltr_select_subject_msg'] = "يرجى تحديد الموضوع";
$lang['ltr_select_subject_both_msg'] = "يرجى تحديد الموضوع والفصل على حد سواء.";
$lang['ltr_word_msg'] = "الرجاء إدخال الكلمة.";
$lang['ltr_answer_msg'] = "يرجى اختيار الإجابة الصحيحة.";
$lang['ltr_start_date_greater_msg'] = "يجب أن يكون تاريخ البدء أكبر من التاريخ الأخير.";
$lang['ltr_past_time_msg'] = "لا يمكن قبول الوقت الماضي.";
$lang['ltr_end_greater_msg'] = "يجب أن يكون وقت الانتهاء أكبر من وقت البدء.";
$lang['ltr_today_greater_msg'] = "يجب أن يكون التاريخ اليوم أو أكبر من اليوم.";

$lang['ltr_class_already_added_msg'] = "تمت إضافة الفئة بالفعل في نفس الوقت.";
$lang['ltr_valid_time_msg'] = "يرجى إدخال الوقت المناسب.";
$lang['ltr_select_date_msg'] = "يرجى تحديد التاريخ.";
$lang['ltr_atleast_question_msg'] = "يرجى تحديد سؤال واحد على الأقل.";
$lang['ltr_select_year_msg'] = "يرجى تحديد السنة.";
$lang['ltr_select_paper_msg'] = "يرجى تحديد الورق.";
$lang['ltr_select_from_date_msg'] = "يرجى التحديد من التاريخ.";
$lang['ltr_select_to_date_msg'] = "يرجى التحديد حتى الآن.";
$lang['ltr_batch_inactive_msg'] = "هذه الدفعة نشطة يرجى الاتصال بالإدارة.";
$lang['ltr_mark_complete'] = "وضع علامة كاملة.";
$lang['ltr_all_fields_msg'] = "جميع الحقول مطلوبة.";
$lang['ltr_password_same_msg'] = "يجب أن تكون كلمة المرور هي نفسها.";
$lang['ltr_hide_password'] = "إخفاء كلمة المرور";
$lang['ltr_new_password_msg'] = "الرجاء إدخال كلمة مرور جديدة.";
$lang['ltr_all_test_record_msg'] = "سيتم مسح جميع البيانات وسجل الاختبار هل تريد حقًا المتابعة?";
$lang['ltr_once_deleted_alert_msg'] = "بمجرد حذفها ، لن تتمكن من استرداد هذه البيانات!";
$lang['ltr_are_deleted_alert_msg'] = "هل أنت متأكد من الحذف?";
$lang['ltr_updated_msg'] = "تم التحديث بنجاح.";
$lang['ltr_alert_updated_msg'] = "هل أنت متأكد من التحديث?";
$lang['ltr_category_changed_msg'] = "تم تغيير الفئة بنجاح.";
$lang['ltr_invalid_birth_msg'] = "تاريخ الميلاد غير صالح.";
$lang['ltr_to_greater_msg'] = "يجب أن يكون التاريخ أكبر من التاريخ.";
$lang['ltr_atleast_student_msg'] = "يرجى تحديد طالب واحد على الأقل.";
$lang['ltr_atleast_date_msg'] = "يرجى تحديد بيانات واحدة على الأقل.";
$lang['ltr_maximum40_characters_msg'] = "يُسمح بحد أقصى 40 حرفًا.";
$lang['ltr_maximum50_characters_msg'] = "يُسمح بحد أقصى 50 حرفًا.";
$lang['ltr_double_class_date_msg'] = "يرجى تحديد تاريخ ووقت فئة مزدوجة.";

$lang['ltr_update_class'] = "فئة التحديث";
$lang['ltr_edit_extra_class'] = "تحرير فئة إضافية";

$lang['ltr_edit_live_class'] = "تحرير فئة مباشرة";

$lang['ltr_cancel'] = "إلغاء";
$lang['ltr_not_allowed_msg'] = "غير مسموح بالوصول إلى direclty.";
$lang['ltr_course_updated_msg'] = "تم تحديث الدورة بنجاح.";
$lang['ltr_course_name_already_msg'] = "اسم الدورة موجود بالفعل.";
$lang['ltr_course_add_msg'] = "تمت إضافة الدورة بنجاح.";
$lang['ltr_batch_updated_msg'] = "تم تحديث الدفعة بنجاح.";
$lang['ltr_batch_name_already_msg'] = "اسم الدفعة موجود بالفعل.";
$lang['ltr_no_data_msg'] = "لم يتم العثور على بيانات.";
$lang['ltr_student_details_updated_msg'] = "تم تحديث التفاصيل بنجاح.";
$lang['ltr_email_already_msg'] = "هذا البريد الإلكتروني قيد الاستخدام بالفعل.";
$lang['ltr_added_msg'] = "تمت إضافته بنجاح.";
$lang['ltr_char_qu_msg'] = "يتم حذف الموضوع وفصوله وأسئلتهم بنجاح..";
$lang['ltr_char_qus_msg'] = "يتم حذف الفصل وأسئلتهم بنجاح..";
$lang['ltr_question_updated_msg'] = "تم تحديث السؤال بنجاح.";
$lang['ltr_question_added_msg'] = "تم إضافة السؤال بنجاح.";
$lang['ltr_question_exists_msg'] = "السؤال موجود بالفعل.";
$lang['ltr_notice_added_msg'] = "تمت إضافة الإشعار بنجاح.";
$lang['ltr_vacancy_added_msg'] = "Vacancy تمت إضافته بنجاح.";
$lang['ltr_video_added_msg'] = "تمت إضافة الفيديو بنجاح.";
$lang['ltr_teacher_added_msg'] = "تمت إضافة المعلم بنجاح.";
$lang['ltr_vacancy_updated_msg'] = "تم تحديث الشواغر بنجاح.";
$lang['ltr_class_updated_msg'] = "تم تحديث الفئة بنجاح.";
$lang['ltr_mock_test_paper'] = "ورقة اختبار وهمية";
$lang['ltr_practice_paper'] = "ورقة الممارسة";
$lang['ltr_paper_added_msg'] = "تم إنشاء الورق بنجاح.";
$lang['ltr_view_mock_paper'] = "عرض الورق الوهمي";
$lang['ltr_view_practice_paper'] = "عرض ورقة الممارسة";
$lang['ltr_add_paper_already_msg'] = "لديك بالفعل ورقة تم إنشاؤها في هذا التاريخ والوقت ، يرجى تغيير وقت الجدول الزمني.";
$lang['ltr_facility_updated_msg'] = "تم تحديث المنشأة بنجاح.";
$lang['ltr_facility_added_msg'] = "تمت إضافة المنشأة بنجاح.";
$lang['ltr_homework_updated_msg'] = "تم تحديث المهمة بنجاح.";
$lang['ltr_homework_added_msg'] = "تمت إضافة المهمة بنجاح.";
$lang['ltr_view_homework'] = "عرض المهمة";
$lang['ltr_password_changed_msg'] = "تم تغيير كلمة المرور بنجاح";
$lang['ltr_profile_updated_msg'] = "تم تحديث الملف الشخصي بنجاح.";
$lang['ltr_deleted_msg'] = "محذوف بنجاح.";
$lang['ltr_leave_apply_msg'] = "اتركه مطبقًا بنجاح."; 
$lang['ltr_no_result'] = "لم يتم العثور على نتيجة.";
$lang['ltr_class_added_msg'] = "تم إضافة الفئة بنجاح.";
$lang['ltr_batch_exists_msg'] = "الدفعة موجودة بالفعل.";
$lang['ltr_data_added_msg'] = "تمت إضافة البيانات بنجاح.";
$lang['ltr_data_updated_msg'] = "تم تحديث البيانات بنجاح.";
$lang['ltr_attendance_added_msg'] = "تمت إضافة الحضور بنجاح.";
$lang['ltr_data_delete_msg'] = "تم حذف البيانات بنجاح.";
$lang['ltr_certificate_generated_msg'] = "الشهادة التي تم إنشاؤها بنجاح.";
$lang['ltr_certificate_updated_msg'] = "تم تحديث الشهادة بنجاح.";
$lang['ltr_privacy_updated_msg'] = "تم تحديث سياسة الخصوصية بنجاح.";
$lang['ltr_no_record_msg'] = "لم يتم العثور على سجل.";
$lang['ltr_missing_parameters_msg'] = "معلمات مفقودة.";
$lang['ltr_doubt_request_msg'] = "تم إرسال طلب الشك الخاص بك.";
$lang['ltr_doubt_request_already_msg'] = "طلب الشك الخاص بك موجود بالفعل.";
$lang['ltr_logged_msg'] = "تم تسجيله بنجاح.";
$lang['ltr_contact_to_admin_msg'] = "تم تعليق حسابك بواسطة المسؤول يرجى الاتصال بالإدارة.";
$lang['ltr_batch_in_msg'] = "دُفعتك غير نشطة.";
$lang['ltr_wrong_credentials_msg'] = "بيانات اعتماد خاطئة.";
$lang['ltr_email_not_exists_msg'] = "معرف البريد الإلكتروني غير موجود.";
$lang['ltr_site_updated_msg'] = "تم تحديث تفاصيل الموقع بنجاح.";
$lang['ltr_payment_updated_msg'] = "تم تحديث تفاصيل الدفع بنجاح.";
$lang['ltr_language_updated_msg'] = "تفاصيل اللغة محدثة بنجاح.";
$lang['ltr_contact_updated_msg'] = "تم تحديث تفاصيل الاتصال بنجاح.";
$lang['ltr_facility_page_updated_msg'] = "تم تحديث إعدادات صفحة المنشأة بنجاح.";
$lang['ltr_about_page_updated_msg'] = "حول إعدادات الصفحة التي تم تحديثها بنجاح.";
$lang['ltr_course_page_updated_msg'] = "تم تحديث إعدادات صفحة الدورة التدريبية بنجاح.";
$lang['ltr_home_page_updated_msg'] = "تم تحديث إعدادات الصفحة الرئيسية بنجاح.";
$lang['ltr_timezone_updated_msg'] = "تم تحديث المنطقة الزمنية بنجاح.";
$lang['ltr_contact_send_updated_msg'] = "رسالة مرسلة بنجاح ، سنتصل بك قريبًا.";

$lang['ltr_class_dates'] = "تواريخ الفصل";

$lang['ltr_complete_date'] = "تاريخ الإكمال";
$lang['ltr_teacher_academic_record'] = "السجل الأكاديمي للمعلم";
$lang['ltr_student_image'] = "صورة الطالب";
$lang['ltr_login'] = "تسجيل الدخول";
$lang['ltr_students_no_data_te'] = "لا يوجد سجل متاح للعرض.";
$lang['ltr_students_no_data'] = "لم تقم بعد بإضافة طالب لا تتردد في إضافة طالب بالنقر فوق الزر إضافة طالب.";
// رسالة
$lang['ltr_status_msg'] = "تم تغيير الحالة بنجاح.";
$lang['ltr_something_msg'] = "حدث خطأ ، يرجى المحاولة مرة أخرى.";
$lang['ltr_matching_msg'] = "لم يتم العثور على سجلات مطابقة.";
$lang['ltr_live_class_msg'] = "لا توجد فئة مباشرة متاحة بدلاً من ذلك.";
$lang['ltr_cant_msg'] = "لا يمكن حذفه ، أضف موضوعًا آخر لحذفه.";
$lang['ltr_are_you_msg'] = "هل أنت متأكد أنك تريد الحذف?";
$lang['ltr_are_you_so_msg'] = "هل أنت متأكد";
$lang['ltr_edit_course'] = "تعديل الدورة";
$lang['ltr_add_new_exam'] = "إضافة اختبار جديد";
$lang['ltr_end_date_msg'] = "يجب أن يكون تاريخ الانتهاء أكبر من تاريخ البدء.";
$lang['ltr_subject_msg'] = "إضافة موضوع واحد على الأقل.";
$lang['ltr_characters_msg'] = "لا يمكنك الحصول على أكثر من 50 حرفًا هنا.";
$lang['ltr_password_student_msg'] = "معرف التسجيل وكلمة المرور للطالب";
$lang['ltr_add_another_student'] = "إضافة طالب آخر";
$lang['ltr_select_batch_msg'] = "يرجى تحديد الدفعة.";
$lang['ltr_changed_batch_msg'] = "تم تغيير الدفعة بنجاح.";
$lang['ltr_changed_password_for_msg'] = "تغيير كلمة المرور لـ";
$lang['ltr_confirm_password_msg'] = "يجب أن تكون كلمة المرور وتأكيد كلمة المرور هي نفسها.";
$lang['ltr_password_msg'] = "تم تغيير كلمة المرور بنجاح.";
$lang['ltr_subject_name_msg'] = "الرجاء إدخال اسم الموضوع.";
$lang['ltr_letters_characters_msg'] = "يُسمح فقط بالأحرف والشرطات والأرقام ، ولا يُسمح بالأحرف الخاصة.";
$lang['ltr_subject_updated_msg'] = "تم تحديث الموضوع بنجاح.";
$lang['ltr_subject_add_msg'] = "تمت إضافة الموضوع بنجاح.";
$lang['ltr_subject_exists_msg'] = "اسم الموضوع موجود بالفعل.";
$lang['ltr_subject_delete_alert_msg'] = "ستحذف جميع الفصول والأسئلة حول هذا الموضوع?";
$lang['ltr_atleast_chapter_msg'] = "يرجى إدخال اسم فصل واحد على الأقل.";
$lang['ltr_add_chapter_msg'] = "تمت إضافة الفصول بنجاح.";
$lang['ltr_exists_chapter_msg'] = "اسم الفصل موجود بالفعل.";
$lang['ltr_chapter_name_msg'] = "الرجاء إدخال اسم الفصل.";
$lang['ltr_chapter_updated_msg'] = "تم تحديث الفصل بنجاح.";
$lang['ltr_chapter_delete_msg'] = "ستحذف جميع أسئلة هذا الفصل?";
$lang['ltr_loading_msg'] = "تحميل";
$lang['ltr_select_subject_msg'] = "يرجى تحديد الموضوع";
$lang['ltr_select_subject_both_msg'] = "يرجى تحديد الموضوع والفصل على حد سواء.";
$lang['ltr_word_msg'] = "الرجاء إدخال الكلمة.";
$lang['ltr_answer_msg'] = "يرجى اختيار الإجابة الصحيحة.";
$lang['ltr_start_date_greater_msg'] = "يجب أن يكون تاريخ البدء أكبر من التاريخ الأخير.";
$lang['ltr_past_time_msg'] = "لا يمكن قبول الوقت الماضي.";
$lang['ltr_end_greater_msg'] = "يجب أن يكون وقت الانتهاء أكبر من وقت البدء.";
$lang['ltr_today_greater_msg'] = "يجب أن يكون التاريخ اليوم أو أكبر من اليوم.";
$lang['ltr_today'] = "Today";
$lang['ltr_Month'] = "Month";
$lang['ltr_class_already_added_msg'] = "تمت إضافة الفئة بالفعل في نفس الوقت.";
$lang['ltr_valid_time_msg'] = "يرجى إدخال الوقت المناسب.";
$lang['ltr_select_date_msg'] = "يرجى تحديد التاريخ.";
$lang['ltr_atleast_question_msg'] = "يرجى تحديد سؤال واحد على الأقل.";
$lang['ltr_select_year_msg'] = "يرجى تحديد السنة.";
$lang['ltr_select_paper_msg'] = "يرجى تحديد الورق.";
$lang['ltr_select_from_date_msg'] = "يرجى التحديد من التاريخ.";
$lang['ltr_select_to_date_msg'] = "يرجى التحديد حتى الآن.";
$lang['ltr_batch_inactive_msg'] = "هذه الدفعة نشطة يرجى الاتصال بالإدارة.";
$lang['ltr_mark_complete'] = "وضع علامة كاملة.";
$lang['ltr_all_fields_msg'] = "جميع الحقول مطلوبة.";
$lang['ltr_password_same_msg'] = "يجب أن تكون كلمة المرور هي نفسها.";
$lang['ltr_hide_password'] = "إخفاء كلمة المرور";
$lang['ltr_new_password_msg'] = "الرجاء إدخال كلمة مرور جديدة.";
$lang['ltr_all_test_record_msg'] = "سيتم مسح جميع البيانات وسجل الاختبار هل تريد حقًا المتابعة?";
$lang['ltr_once_deleted_alert_msg'] = "بمجرد حذفها ، لن تتمكن من استرداد هذه البيانات!";
$lang['ltr_are_deleted_alert_msg'] = "هل أنت متأكد من الحذف?";
$lang['ltr_updated_msg'] = "تم التحديث بنجاح.";
$lang['ltr_alert_updated_msg'] = "هل أنت متأكد من التحديث?";
$lang['ltr_category_changed_msg'] = "تم تغيير الفئة بنجاح.";
$lang['ltr_invalid_birth_msg'] = "تاريخ الميلاد غير صالح.";
$lang['ltr_to_greater_msg'] = "يجب أن يكون التاريخ أكبر من التاريخ.";
$lang['ltr_atleast_student_msg'] = "يرجى تحديد طالب واحد على الأقل.";
$lang['ltr_atleast_date_msg'] = "يرجى تحديد بيانات واحدة على الأقل.";
$lang['ltr_maximum40_characters_msg'] = "يُسمح بحد أقصى 40 حرفًا.";
$lang['ltr_maximum50_characters_msg'] = "يُسمح بحد أقصى 50 حرفًا.";
$lang['ltr_double_class_date_msg'] = "يرجى تحديد تاريخ ووقت فئة مزدوجة.";

$lang['ltr_update_class'] = "فئة التحديث";
$lang['ltr_edit_extra_class'] = "تحرير فئة إضافية";

$lang['ltr_edit_live_class'] = "تحرير فئة مباشرة";
$lang['ltr_ok'] = "Ok";
$lang['ltr_cancel'] = "إلغاء";
$lang['ltr_not_allowed_msg'] = "غير مسموح بالوصول إلى direclty.";
$lang['ltr_course_updated_msg'] = "تم تحديث الدورة بنجاح.";
$lang['ltr_course_name_already_msg'] = "اسم الدورة موجود بالفعل.";
$lang['ltr_course_add_msg'] = "تمت إضافة الدورة بنجاح.";
$lang['ltr_batch_updated_msg'] = "تم تحديث الدفعة بنجاح.";
$lang['ltr_batch_name_already_msg'] = "اسم الدفعة موجود بالفعل.";
$lang['ltr_no_data_msg'] = "لم يتم العثور على بيانات.";
$lang['ltr_student_details_updated_msg'] = "تم تحديث التفاصيل بنجاح.";
$lang['ltr_email_already_msg'] = "هذا البريد الإلكتروني قيد الاستخدام بالفعل.";
$lang['ltr_added_msg'] = "تمت إضافته بنجاح.";
$lang['ltr_char_qu_msg'] = "يتم حذف الموضوع وفصوله وأسئلتهم بنجاح..";
$lang['ltr_char_qus_msg'] = "يتم حذف الفصل وأسئلتهم بنجاح..";
$lang['ltr_question_updated_msg'] = "تم تحديث السؤال بنجاح.";
$lang['ltr_question_added_msg'] = "تم إضافة السؤال بنجاح.";
$lang['ltr_question_exists_msg'] = "السؤال موجود بالفعل.";
$lang['ltr_notice_added_msg'] = "تمت إضافة الإشعار بنجاح.";
$lang['ltr_vacancy_added_msg'] = "Vacancy تمت إضافته بنجاح.";
$lang['ltr_video_added_msg'] = "تمت إضافة الفيديو بنجاح.";
$lang['ltr_teacher_added_msg'] = "تمت إضافة المعلم بنجاح.";
$lang['ltr_vacancy_updated_msg'] = "تم تحديث الشواغر بنجاح.";
$lang['ltr_class_updated_msg'] = "تم تحديث الفئة بنجاح.";
$lang['ltr_mock_test_paper'] = "ورقة اختبار وهمية";
$lang['ltr_practice_paper'] = "ورقة الممارسة";
$lang['ltr_paper_added_msg'] = "تم إنشاء الورق بنجاح.";
$lang['ltr_view_mock_paper'] = "عرض الورق الوهمي";
$lang['ltr_view_practice_paper'] = "عرض ورقة الممارسة";
$lang['ltr_add_paper_already_msg'] = "لديك بالفعل ورقة تم إنشاؤها في هذا التاريخ والوقت ، يرجى تغيير وقت الجدول الزمني.";
$lang['ltr_facility_updated_msg'] = "تم تحديث المنشأة بنجاح.";
$lang['ltr_facility_added_msg'] = "تمت إضافة المنشأة بنجاح.";
$lang['ltr_homework_updated_msg'] = "تم تحديث المهمة بنجاح.";
$lang['ltr_homework_added_msg'] = "تمت إضافة المهمة بنجاح.";
$lang['ltr_view_homework'] = "عرض المهمة";
$lang['ltr_password_changed_msg'] = "تم تغيير كلمة المرور بنجاح";
$lang['ltr_profile_updated_msg'] = "تم تحديث الملف الشخصي بنجاح.";
$lang['ltr_deleted_msg'] = "محذوف بنجاح.";
$lang['ltr_leave_apply_msg'] = "اتركه مطبقًا بنجاح."; 
$lang['ltr_no_result'] = "لم يتم العثور على نتيجة.";
$lang['ltr_class_added_msg'] = "تم إضافة الفئة بنجاح.";
$lang['ltr_batch_exists_msg'] = "الدفعة موجودة بالفعل.";
$lang['ltr_data_added_msg'] = "تمت إضافة البيانات بنجاح.";
$lang['ltr_data_updated_msg'] = "تم تحديث البيانات بنجاح.";
$lang['ltr_attendance_added_msg'] = "تمت إضافة الحضور بنجاح.";
$lang['ltr_data_delete_msg'] = "تم حذف البيانات بنجاح.";
$lang['ltr_certificate_generated_msg'] = "الشهادة التي تم إنشاؤها بنجاح.";
$lang['ltr_certificate_updated_msg'] = "تم تحديث الشهادة بنجاح.";
$lang['ltr_privacy_updated_msg'] = "تم تحديث سياسة الخصوصية بنجاح.";
$lang['ltr_no_record_msg'] = "لم يتم العثور على سجل.";
$lang['ltr_missing_parameters_msg'] = "معلمات مفقودة.";
$lang['ltr_doubt_request_msg'] = "تم إرسال طلب الشك الخاص بك.";
$lang['ltr_doubt_request_already_msg'] = "طلب الشك الخاص بك موجود بالفعل.";
$lang['ltr_logged_msg'] = "تم تسجيله بنجاح.";
$lang['ltr_contact_to_admin_msg'] = "تم تعليق حسابك بواسطة المسؤول يرجى الاتصال بالإدارة.";
$lang['ltr_batch_in_msg'] = "دُفعتك غير نشطة.";
$lang['ltr_wrong_credentials_msg'] = "بيانات اعتماد خاطئة.";
$lang['ltr_email_not_exists_msg'] = "معرف البريد الإلكتروني غير موجود.";
$lang['ltr_site_updated_msg'] = "تم تحديث تفاصيل الموقع بنجاح.";
$lang['ltr_payment_updated_msg'] = "تم تحديث تفاصيل الدفع بنجاح.";
$lang['ltr_language_updated_msg'] = "تفاصيل اللغة محدثة بنجاح.";
$lang['ltr_contact_updated_msg'] = "تم تحديث تفاصيل الاتصال بنجاح.";
$lang['ltr_facility_page_updated_msg'] = "تم تحديث إعدادات صفحة المنشأة بنجاح.";
$lang['ltr_about_page_updated_msg'] = "حول إعدادات الصفحة التي تم تحديثها بنجاح.";
$lang['ltr_course_page_updated_msg'] = "تم تحديث إعدادات صفحة الدورة التدريبية بنجاح.";
$lang['ltr_home_page_updated_msg'] = "تم تحديث إعدادات الصفحة الرئيسية بنجاح.";
$lang['ltr_timezone_updated_msg'] = "تم تحديث المنطقة الزمنية بنجاح.";
$lang['ltr_contact_send_updated_msg'] = "رسالة مرسلة بنجاح ، سنتصل بك قريبًا.";
$lang['ltr_our_facilities'] = "مرافقنا";
$lang['ltr_our_gallery'] = "معرضنا";
$lang['ltr_our_video_gallery'] = "معرض الفيديو الخاص بنا";

$lang['ltr_enhance'] = "نحن نحسن موهبتك";
$lang['ltr_active_show'] = "في العرض النشط";

$lang['ltr_years_of_history'] = "سنوات التاريخ";
$lang['ltr_read_more'] = "اقرأ المزيد";
$lang['ltr_about_eacademy'] = "حول الأكاديمية الإلكترونية";
$lang['ltr_home_thousands'] = "لماذا تختارنا من بين الآلاف";
$lang['ltr_our_facilities'] = "مرافقنا";
$lang['ltr_batch_spe_msg'] = "لا يمكن الحذف ، أضف عنوانًا آخر لحذفه.";

$lang['ltr_our_selections'] = "اختياراتنا";

$lang['ltr_our_courses'] = "دوراتنا";
$lang['ltr_price'] = "السعر";

$lang['ltr_our_links'] = "روابطنا";
$lang['ltr_my_profile'] = "ملفي الشخصي";
$lang['ltr_you_delete'] = "هل تريد الحذف ?";
$lang['ltr_amount'] = "المبلغ";
$lang['ltr_i_learn'] = "ماذا سأتعلم ?";


$lang['ltr_filter_by_subject'] = "فلتر حسب الموضوع";
$lang['ltr_answer'] = "إجابة";

$lang['ltr_enter_word_comma'] = "أدخل Word ( مفصولة بفاصلة)";
$lang['ltr_questions_selected'] = "الأسئلة المحددة";
$lang['ltr_reset'] = "إعادة تعيين";
$lang['ltr_create_paper_msg'] = "لم تقم بإضافة أي ورقة حتى الآن لا تتردد في إضافة الورق بالنقر فوق الزر إنشاء ورقة يرجى التأكد من إضافة سؤال.";
$lang['ltr_create_exam'] = "إنشاء امتحان";
$lang['ltr_total_selected'] = "إجمالي الأسئلة المحددة";
$lang['ltr_offer_price'] = "سعر العرض";
$lang['ltr_offer_price_msg'] = "يجب أن يكون سعر العرض أكبر من سعر الدفعة.";
$lang['ltr_batch_price_msg'] = "الرجاء إدخال سعر الدفعة.";

$lang['ltr_api_key'] = "API key";
$lang['ltr_paper_type'] = "نوع الورق";
$lang['ltr_select_type'] = "Select Type";
$lang['ltr_mock_test_schedule_date'] = "تاريخ جدول اختبار وهمية";
$lang['ltr_schedule_date'] = "تاريخ الجدول";
$lang['ltr_schedule_time'] = "جدول الوقت";
$lang['ltr_time_duration_min'] = "المدة الزمنية ( Min )";
$lang['ltr_time_duration_mini'] = "المدة الزمنية بالدقيقة";
$lang['ltr_paper_format'] = "تنسيق الورق";
$lang['ltr_new_password'] = "كلمة مرور جديدة";
$lang['ltr_aproved'] = "موافق عليه";
$lang['ltr_scheduled_date'] = "التاريخ المجدول";
$lang['ltr_scheduled_time'] = "الوقت المجدول";
$lang['ltr_credentials'] = "Credentials";
$lang['ltr_congrature'] = "مبروك!!";
$lang['ltr_hey'] = "Hey";
$lang['ltr_successility_enroll'] = "أنت مسجل بنجاح معنا.";
$lang['ltr_login_details'] = "إليك تفاصيل تسجيل الدخول";
$lang['ltr_email_settings'] = "إعدادات البريد الإلكتروني";
$lang['ltr_server_type'] = "نوع الخادم";

$lang['ltr_server_mail'] = "بريد الخادم ( الافتراضي )";

$lang['ltr_firebase_settings'] = "إعدادات قاعدة الإطارات";
$lang['ltr_firebase_key'] = "مفتاح Firebase";
$lang['ltr_key_fire'] = "أدخل مفتاح Firebase الخاص بك.";
$lang['ltr_smtp_emaila'] = "أدخل اسم مستخدم عنوان بريدك الإلكتروني هنا.";
$lang['ltr_test_email'] = "اختبار خادم البريد الإلكتروني";
$lang['ltr_new_certificate'] = "شهادة جديدة";
$lang['ltr_earned_certificate'] = "لقد حصلت على شهادة يرجى فتح رابط للتنزيل";

$lang['ltr_total_question'] = "السؤال الإجمالي";
$lang['ltr_certificate_settings'] = "إعدادات الشهادة";
$lang['ltr_view_paper'] = "عرض الورق";
$lang['ltr_approve'] = "موافق عليه";
$lang['ltr_payment_msg'] = "الدفع منخفض جدًا.";

//دورة

$lang['ltr_course_price'] = "$500";
$lang['tr_course_rating'] = "4.5";
$lang['ltr_course_rati_no'] = "( 5،124 )";
$lang['ltr_course_title'] = "في عام 2021 ، أكمل WordPress من Zero إلى Hero في WordPress";

$lang['ltr_course_view'] = "عرض المزيد";

$lang['ltr_ic_ttl'] = "تتضمن هذه الدورة:";
$lang['ltr_recod_course'] = "الدورات الموصى بها";
$lang['ltr_course_details'] = "تفاصيل الدورة";

// تحديث الويب الجديد 

$lang['ltr_comments'] = "تعليقات";
$lang['ltr_no_image'] = "لا توجد صورة متاحة.";
$lang['ltr_no_blog'] = "لا توجد مدونة متاحة.";
$lang['ltr_no_news'] = "لا توجد أخبار متاحة.";
$lang['ltr_blog_details'] = "تفاصيل المدونة";
$lang['ltr_leave_comments'] = "اترك تعليقات";

$lang['ltr_comments_msg'] = "تمت إضافة التعليق بنجاح.";

$lang['ltr_username'] = "اسم المستخدم";
$lang['ltr_email_address'] = "الرجاء إدخال البريد الإلكتروني";

$lang['ltr_online'] = "عبر الإنترنت";

//تحديث جديد

$lang['ltr_pdf'] = "ملف PDF";

$lang['ltr_book_added_msg'] = "تمت إضافة الكتاب بنجاح.";

$lang['ltr_notes_added_msg'] = "ملاحظات تمت إضافتها بنجاح.";

$lang['ltr_book_no_data_admin'] = 'لم تقم بإضافة أي كتاب بعد ، لا تتردد في إضافة كتاب بالنقر فوق الزر "إضافة كتاب.';
$lang['ltr_book_no_data_student'] = 'لا يوجد كتاب متاح للعرض.';

$lang['ltr_notes_no_data_admin'] = 'لم تقم بإضافة أي ملاحظات بعد ، لا تتردد في إضافة ملاحظات بالنقر فوق الزر "إضافة ملاحظات.';
$lang['ltr_notes_no_data_student'] = 'لا توجد ملاحظات متاحة للعرض.';
$lang['ltr_library_no_data_admin'] = 'لم تقم بإضافة أي مكتبة حتى الآن ، لا تتردد في إضافة مكتبة بالنقر فوق الزر "إضافة مكتبة.';
$lang['ltr_library_no_data_student'] = 'لا توجد كتب مكتبة متاحة للعرض.';
$lang['ltr_book_request'] = "طلب كتاب";
$lang['ltr_book_name'] = "اسم الكتاب";
$lang['ltr_request'] = "طلب";
$lang['ltr_library_request'] = "طلب مكتبة";

$lang['ltr_add_request'] = "إضافة طلب";
$lang['ltr_request_exists'] = "طلبك موجود بالفعل"; 

$lang['ltr_online'] = "عبر الإنترنت";

$lang['ltr_response_date'] = "تاريخ الاستجابة";
$lang['ltr_file_view'] = "عرض الملف";
$lang['ltr_practice_merit_list'] = "قائمة مزايا الممارسة";
$lang['ltr_mock_merit_list'] = "قائمة الجدارة الوهمية";
$lang['ltr_video_type'] = "نوع الفيديو";

// 01 مايو 2021
$lang['ltr_pdf_msg'] = "يرجى تحديد ملف pdf.";
$lang['ltr_view_video'] = "عرض الفيديو";

$lang['ltr_video_msg'] = "يرجى تحديد ملف الفيديو.";
$lang['ltr_my_course'] = "My Course";
$lang['ltr_paid_course'] = "Paid Course";
$lang['ltr_all_course'] = "All Course";
$lang['ltr_free_course'] = "دورة مجانية";
$lang['ltr_preview_type'] = "نوع المعاينة";
$lang['ltr_paid_preview'] = "Paid Preview";
$lang['ltr_free_preview'] = "معاينة مجانية";
$lang['ltr_video_lectures'] = "محاضرات الفيديو";
$lang['ltr_preview'] = "معاينة";
$lang['ltr_batch_change_msg'] = "تغيير الدفعة بنجاح.";

$lang['ltr_oldp_no_data_admin'] = 'لم تقم بإضافة أي ورقة قديمة بعد ، لا تتردد في إضافة ورقة قديمة بالنقر فوق الزر "إضافة ورق قديم.';
$lang['ltr_old_paper_no_data_student'] = "لا يتوفر ورق قديم لإظهاره.";
$lang['ltr_add_old_paper'] = "إضافة ورق قديم";
$lang['ltr_oldp_added_msg'] = "تمت إضافة الورق القديم بنجاح.";

$lang['ltr_no_course_result'] = "لا توجد أي دورة متاحة للعرض.";
$lang['ltr_no_notification'] = "لا يوجد أي إشعار متاح للعرض.";
$lang['ltr_view_all'] = "عرض الكل";

$lang['ltr_terms_conditions'] = "الشروط والأحكام";
$lang['ltr_terms_updated_msg'] = "تم تحديث حالة الشروط بنجاح.";

$lang['ltr_add_category'] = "إضافة فئة";
$lang['ltr_edit_category'] = "تحرير الفئة";
$lang['ltr_category_name'] = "اسم الفئة";
$lang['ltr_cat_updated_msg'] = "تم تحديث الفئة بنجاح.";
$lang['ltr_cat_add_msg'] = "تمت إضافة الفئة بنجاح.";
$lang['ltr_cat_name_msg'] = "الرجاء إدخال اسم الفئة.";
$lang['ltr_cat_exists_msg'] = "اسم الفئة موجود بالفعل.";
$lang['ltr_cat_delete_alert_msg'] = "سوف يحذف الفئة ?";
$lang['ltr_cat_delete_msg'] = "تم حذف الفئة بنجاح..";
$lang['ltr_cat_no_data'] = 'لم تقم بإضافة أي فئة بعد ، لا تتردد في إضافة فئة بالنقر فوق الزر "إضافة فئة.';
$lang['ltr_batch_subcat_manager'] = "مدير الفئة الفرعية";
$lang['ltr_add_subcategory'] = "إضافة فئة فرعية";
$lang['ltr_subCategory_name'] = "اسم الفئة الفرعية";
$lang['ltr_select_category'] = "تحديد الفئة";
$lang['ltr_subcat_no_data'] = 'لم تقم بإضافة أي فئة فرعية بعد ، لا تتردد في إضافة فئة فرعية بالنقر فوق الزر "إضافة فئة فرعية.';
$lang['ltr_subcat_delete_msg'] = "تم حذف الفئة الفرعية بنجاح..";
$lang['ltr_edit_subcategory'] = "تحرير الفئة الفرعية";
$lang['ltr_select_subcategory'] = "تحديد الفئة الفرعية";
$lang['ltr_p_register_continue'] = "الرجاء التسجيل للمتابعة";

$lang['ltr_login_now'] = "تسجيل الدخول الآن";
$lang['ltr_already_account'] = "هل لديك بالفعل حساب من فضلك";
$lang['ltr_dont_account'] = "ليس لديك حساب يرجى التسجيل للمتابعة...";
$lang['ltr_registered_msg'] = "مبروك!! لقد أرسلنا تفاصيل تسجيل الدخول على بريدك الإلكتروني. يرجى تسجيل الدخول للمتابعة.";
$lang['ltr_browse_course'] = "تصفح الدورات";
$lang['ltr_sign_purchase'] = "تسجيل للشراء";
$lang['ltr_thnx_msg'] = "شكرًا لك";
$lang['ltr_edit_image'] = "تحرير الصورة / الفيديو";

$lang['ltr_successully_enroll_in'] = "أنت مسجل بنجاح";
$lang['ltr_enjoy'] = "استمتع بتعلمك";
$lang['ltr_thanks_msg'] = "شكرًا";

$lang['ltr_batch_found'] = "لم يتم العثور على دفعة.";
$lang['ltr_no_dashboard'] = "يرجى تحديد دورة من دوراتي تحت ملف التعريف أو النقر على تصفح الدورات لشراء دورات جديدة..";
$lang['ltr_already_enrolled'] = "تم التسجيل بالفعل";
$lang['ltr_manage_certificate'] = "إدارة الشهادة";

$lang['ltr_not_assign'] = "لم يتم تعيينه بعد";
$lang['ltr_mobile_already_msg'] = "رقم الهاتف المحمول هذا قيد الاستخدام بالفعل.";
$lang['ltr_edit_book'] = "تعديل الكتاب";
$lang['ltr_edit_notes'] = "تعديل الملاحظات";
$lang['ltr_edit_old_paper'] = "تحرير الورق القديم";
$lang['ltr_notes_update_msg'] = "ملاحظات تم تحديثها بشكل ناجح";
$lang['ltr_book_update_msg'] = "تم تحديث الكتاب بشكل ناجح";
$lang['ltr_oldpaper_update_msg'] = "تم تحديث الورق القديم بشكل كامل";
$lang['ltr_access_files_msg'] = "ليس لديك حق الوصول إلى حذف ملفات المشرف";
$lang['ltr_manager_admin'] = "مدير المستخدم";
$lang['ltr_add_admin'] = "إضافة مستخدم";
$lang['ltr_admin_added_msg'] = "تمت إضافة المستخدم بنجاح.";
$lang['ltr_admin_updated_msg'] = "تم تحديث المستخدم بنجاح.";
$lang['ltr_admin_no_data'] = "لم تقم بإضافة أي مستخدم حتى الآن لا تتردد في إضافة مستخدم بالنقر فوق الزر إضافة مستخدم";
$lang['ltr_select_user'] = "تحديد المستخدم";
// Custome إنشاء لغة 
$lang['ltr_module_access'] = "الوصول إلى الوحدة";


$lang['ltr_edit_manage_paper'] = "تعديل إدارة الورق";

$lang['ltr_google_recaptcha'] = "فشل التحقق حاول مرة أخرى";
$lang['ltr_add_question_p'] = "إضافة سؤال";
$lang['ltr_Create_New_Question'] = "إنشاء سؤال جديد ";
$lang['ltr_old_question'] = "سؤال قديم";
$lang['ltr_question_select'] = "تحديد السؤال";

// إضافة علامات 
$lang['ltr_Questions'] = "أسئلة";
$lang['ltr_add_mask'] = "أضف علامات";
$lang['ltr_question_mask'] = "علامات الاستفهام";
$lang['ltr_enter_number'] = "الرجاء إدخال الرقم ";

$lang['ltr_Negative'] = "تعيين علامات سلبية %";
$lang['ltr_value_set'] = "القيمة الافتراضية 0.25٪ العلامات السلبية ";
$lang['ltr_marking_value'] = "الرجاء إدخال القيمة";
$lang['ltr_themes_option'] = "خيار السمات";
$lang['ltr_add_themes_color'] = "إضافة لون السمات بنجاح";

$lang['ltr_update_themes_color'] = "تحديث لون السمات بنجاح";
$lang['ltr_add_themes_color_error'] = "الرجاء تحديد لون جميع السمات ";
$lang['ltr_Jetsi_Meet'] = "لقاء جيتسي";
$lang['ltr_zoom_meeting'] = "تكبير الاجتماع";
$lang['ltr_jetsi_meeting'] = "اجتماع Jetsi";
$lang['ltr_themes_color_option'] = "خيار لون المظهر";
$lang['ltr_Marking'] = "تمييز";
$lang['ltr_Update_Paper'] = "تحديث الورقة";
$lang['ltr_book_manage'] = "مدير الكتب";
$lang['ltr_add_book'] = "إضافة كتاب";
$lang['ltr_book'] = "كتاب";
$lang['ltr_books'] = "كتب";
$lang['ltr_notes_manage'] = "مدير الملاحظات";
$lang['ltr_add_notes'] = "إضافة ملاحظات";
$lang['ltr_notes'] = "ملاحظات";
$lang['ltr_library'] = "المكتبة";
$lang['ltr_library_manager'] = "مدير المكتبة";
$lang['ltr_add_library'] = "إضافة مكتبة";
$lang['ltr_author'] = "المؤلف";
$lang['ltr_file'] = "ملف";
$lang['ltr_select_book'] = "اختر كتابًا";
$lang['ltr_homeworks'] = "واجبات منزلية";
$lang['ltr_offline'] = "غير متصل";
$lang['ltr_muse'] = "Muse";
$lang['ltr_youtube'] = "Youtube";
$lang['ltr_vimeo'] = "Vimeo";
$lang['ltr_dropbox'] = "Dropbox";
$lang['ltr_embed'] = "تضمين";
$lang['ltr_url'] = "عنوان Url";
$lang['ltr_old_paper'] = "ورقة قديمة";
$lang['ltr_old_papers'] = "أوراق قديمة";
$lang['ltr_follow_us_on'] = "تابعنا على";
$lang['ltr_not_preview'] = "ليست معاينة";
$lang['ltr_all_notification'] = "كل التنبيهات";
$lang['ltr_batch_cat_manager'] = "مدير فئة";
$lang['ltr_register'] = "تسجيل";
$lang['ltr_register_now'] = "سجل الآن";
$lang['ltr_enter_position'] = "أدخل المركز";
$lang['ltr_syllabus'] = "المنهج";
$lang['ltr_eacademy'] = "أكاديمية إلكترونية";
$lang['ltr_assigned'] = "Assed";
$lang['ltr_Update'] = "تحديث";
$lang['ltr_login_creadential'] = "بيانات تسجيل الدخول";
$lang['ltr_pay_mode'] = "وضع الدفع";
$lang['ltr_upload'] = "تحميل";
$lang['ltr_add_teacher'] = "إضافة مدرس";
$lang['ltr_doubts_class'] = "فئة الشكوك";
$lang['ltr_eductaion'] = "Eductaion";
$lang['ltr_class_time'] = "وقت الفصل";
$lang['ltr_today'] = "اليوم";
$lang['ltr_Month'] = "شهر";
$lang['ltr_enrollment_id'] = "معرف التسجيل";
$lang['ltr_edit_teacher'] = "تحرير المدرس";
$lang['ltr_update_teacher'] = "تحديث المدرس";
$lang['ltr_add_class'] = "إضافة فصل دراسي";
$lang['ltr_edit_facility'] = "تحرير المنشأة";
$lang['ltr_add_assignment'] = "إضافة مهمة";
$lang['ltr_edit_assignment'] = "تحرير الواجب";
$lang['ltr_update_assignment'] = "تحديث التعيين";
$lang['ltr_ok'] = "حسنًا";
$lang['ltr_eductaion'] = "Eductaion";
$lang['ltr_class_time'] = "وقت الفصل";
$lang['ltr_enrollment_id'] = "معرف التسجيل";
$lang['ltr_edit_teacher'] = "تحرير المدرس";
$lang['ltr_update_teacher'] = "تحديث المدرس";
$lang['ltr_add_class'] = "إضافة فصل دراسي";
$lang['ltr_edit_facility'] = "تحرير المنشأة";
$lang['ltr_add_assignment'] = "إضافة مهمة";
$lang['ltr_edit_assignment'] = "تحرير الواجب";
$lang['ltr_update_assignment'] = "تحديث التعيين";
$lang['ltr_load_more'] = "تحميل المزيد";
$lang['ltr_officia_deserunt'] = "استثناءً من الأحداث المهمة في حالة عدم وجود مسئولية عن العمل.";
$lang['ltr_course'] = "دورة";
$lang['ltr_class_duration'] = "مدة الفصل الدراسي";
$lang['ltr_our_toppers'] = "القبعات العالية لدينا";
$lang['ltr_home_text'] = "Consectetur adipisicing elit sed do eiusmod tempor incidunt ut labore eesdoeit dolore magna aliqua. reprehenderit في voluptate esse cillum.
بستثنء ما يحدث حتى الآن ، يجب أن يكون السبب وراء ذلك هو أن السبب هو العمل. Sed ut peerspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantiuws totam rem aperiam، eaque ipsa quae. ";
$lang['ltr_toppers_here'] = "القمم هنا";
$lang['ltr_designation'] = "التعيين";
$lang['ltr_assignment_manager'] = "مدير المهام";
$lang['ltr_chapters'] = "الفصل";
$lang['ltr_options'] = "خيارات";
$lang['ltr_add_book'] = "إضافة كتاب";
$lang['ltr_filter_by_word'] = "تصفية حسب الكلمة";
$lang['ltr_currency_converter_API'] = "واجهة برمجة تطبيقات تحويل العملات";
$lang['ltr_smtp_server'] = "خادم SMTP";
$lang['ltr_smtp_host'] = "مضيف SMTP";
$lang['ltr_host'] = "المضيف";
$lang['ltr_smtp_username'] = "اسم مستخدم SMTP";
$lang['ltr_smtp_password'] = "كلمة مرور SMTP";
$lang['ltr_smtp_port'] = "منفذ SMTP";
$lang['ltr_smtp_encryption'] = "تشفير SMTP";
$lang['ltr_tlc'] = "TLC";
$lang['ltr_ssl'] = "SSL";
$lang['ltr_link'] = "رابط";
$lang['ltr_add_android_key'] = "إضافة مفتاح ANDROID";
$lang['ltr_paper'] = "ورق";
$lang['ltr_best_seller'] = "الأفضل مبيعًا";
$lang['ltr_add_to_cart'] = "إضافة إلى عربة التسوق";
$lang['ltr_course_des'] = "Consectetur adipisicing elit، sed do eiusmod tempor incidunt ut labore et dolore magna aliqua. إضافة المزيد من التفاصيل.";
$lang['ltr_course_sgl_des'] = "Lorem ipsum dolor sit amet, consectetur adipisicing eliesdt, sedesd eiusmod tempor incididunt ut labore et dolore magna aliqua. esdet enim ad minim veniam, quis nostrud exercitation ullamco labesoris nisi ut aliquip ex ea commodo consequat.";
$lang['ltr_course_sgl_des2'] = "Duis aute irure dolor in reprehenderit in voluptate velit esse cilluim dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidiaitat non proident, sunt in culpa qui officia deserunt mollit anim iesdeed est laborum.ed ut perspiciatis unde omnis sint occaecat cupidiaitat non proident,qui officia.";
$lang['ltr_enroll_now'] = "سجل الآن";
$lang['ltr_offer'] = "عرض";
$lang['ltr_blog'] = "مدونة";
$lang['ltr_blog_manager'] = "مدير المدونة";
$lang['ltr_blog_manage'] = "إدارة المدونة";
$lang['ltr_add_blog'] = "إضافة مدونة";
$lang['ltr_blog_recent'] = "آخر رسالة";
$lang['ltr_archives'] = "أرشيفات";
$lang['ltr_blog_news'] = "آخر الأخبار";
$lang['ltr_send'] = "إرسال";
$lang['ltr_reply'] = "رد";
$lang['ltr_benefit'] = "فائدة";
$lang['ltr_assign_benefit'] = "تعيين الميزة";
$lang['ltr_Source'] = "المصدر";
$lang['ltr_offline'] = "غير متصل";
$lang['ltr_notice_for'] = "إشعار لـ";
$lang['ltr_description'] = "الوصف";
$lang['ltr_date'] = "التاريخ";
$lang['ltr_add'] = "إضافة";
$lang['ltr_update'] = "تحديث";
$lang['ltr_add_chapters'] = "إضافة فصول";
$lang['ltr_option_A'] = "الخيار أ";
$lang['ltr_option_B'] = "الخيار ب";
$lang['ltr_option_C'] = "الخيار ج";
$lang['ltr_option_D'] = "الخيار د";
$lang['ltr_a'] = "A";
$lang['ltr_b'] = "B";
$lang['ltr_c'] = "C";
$lang['ltr_d'] = "D";
$lang['ltr_excel'] = "Excel";
$lang['ltr_added_on'] = "تمت الإضافة في";
$lang['ltr_added_by'] = "تمت إضافته بواسطة";
$lang['ltr_category'] = "الفئة";
$lang['ltr_none'] = "بلا";
$lang['ltr_online_mode'] = "وضع الاتصال";
$lang['ltr_mode'] = "الوضع";
$lang['ltr_select_mode'] = "تحديد الوضع";
$lang['ltr_note'] = "ملاحظة";
$lang['ltr_exam_title'] = "عنوان الاختبار";
$lang['ltr_view_vacancy'] = "عرض الوظيفة الشاغرة";
$lang['ltr_save'] = "حفظ";
$lang['ltr_add_mobile_key'] = "إضافة مفتاح الجوال";
$lang['ltr_add_live_class'] = "إضافة فصل دراسي مباشر";
$lang['ltr_add_ljetsi_class'] = "إضافة فئة Jetsi";
$lang['ltr_sdk_key'] = "مفتاح SDK";
$lang['ltr_sdk_secret'] = "سر SDK";
$lang['ltr_continue'] = "متابعة";
$lang['ltr_class_by'] = "التصنيف حسب";
$lang['ltr_gallery'] = "المعرض";
$lang['ltr_type'] = "النوع";
$lang['ltr_youtube_URL'] = "عنوان URL على YouTube";
$lang['ltr_image'] = "صورة";
$lang['ltr_add_video'] = "إضافة فيديو";
$lang['ltr_delete'] = "حذف";
$lang['ltr_name'] = "الاسم";
$lang['ltr_email'] = "بريد إلكتروني";
$lang['ltr_mobile'] = "الجوال";
$lang['ltr_message'] = "رسالة";
$lang['ltr_time_zone'] = "المنطقة الزمنية";
$lang['ltr_set_time_zone'] = "تعيين المنطقة الزمنية";
$lang['ltr_favicon'] = "الرمز المفضل";
$lang['ltr_site_logo'] = "شعار الموقع";
$lang['ltr_mini_site_logo'] = "شعار الموقع المصغر";
$lang['ltr_site_preloader'] = "أداة التحميل المسبق للموقع";
$lang['ltr_slider'] = "شريط التمرير";
$lang['ltr_counter'] = "عداد";
$lang['ltr_selection'] = "اختيار";
$lang['ltr_testimonial'] = "شهادة";
$lang['ltr_BANNER_SLIDES'] = "BANNER SLIDES";
$lang['ltr_add_more'] = "إضافة المزيد";
$lang['ltr_heading'] = "العنوان";
$lang['ltr_sub_heading'] = "عنوان فرعي";
$lang['ltr_select_student'] = "اختر الطالب";
$lang['ltr_no_of_teachers'] = "عدد المعلمين";
$lang['ltr_button_text'] = "نص الزر";
$lang['ltr_started_year'] = "عام البدء";
$lang['ltr_second_section'] = "القسم الثاني";
$lang['ltr_since_year'] = "منذ العام";
$lang['ltr_manage_course'] = "إدارة الدورة التدريبية";
$lang['ltr_add_course'] = "إضافة دورة";
$lang['ltr_course_name'] = "اسم المقرر";
$lang['ltr_course_image'] = "صورة المقرر الدراسي";
$lang['ltr_icon'] = "أيقونة";
$lang['ltr_icons'] = "أيقونات";
$lang['ltr_go_to'] = "اذهب إلى";
$lang['ltr_add_facility'] = "إضافة منشأة";
$lang['ltr_facebook'] = "Facebook";
$lang['ltr_you_tube'] = "You Tube";
$lang['ltr_twitter'] = "Twitter";
$lang['ltr_instagram'] = "Instagram";
$lang['ltr_linked_in'] = "مرتبط في";
$lang['ltr_google_API'] = "مفتاح واجهة برمجة تطبيقات خرائط Google";
$lang['ltr_razorpay'] = "Razorpay";
$lang['ltr_paypal'] = "Paypal";
$lang['ltr_razorpay_key_id'] = "معرف مفتاح Razorpay";
$lang['ltr_key_id'] = "معرّف المفتاح";
$lang['ltr_razorpay_secret_key'] = "مفتاح Razorpay السري";
$lang['ltr_paypal_client_id'] = "معرف عميل Paypal";
$lang['ltr_client_id'] = "معرف العميل";
$lang['ltr_paypal_secret_key'] = "مفتاح Paypal السري";
$lang['ltr_gender'] = "الجنس";
$lang['ltr_male'] = "ذكر";
$lang['ltr_female'] = "أنثى";
$lang['ltr_dob'] = "DOB";
$lang['ltr_BATCH_INFORMATION'] = "معلومات مجمعة";
$lang['ltr_month'] = "شهر";
$lang['ltr_day'] = "يوم";
$lang['ltr_update_student'] = "تحديث الطالب";
$lang['ltr_enrolment_id'] = "معرف التسجيل";
$lang['ltr_progress'] = "التقدم";
$lang['ltr_academic_record'] = "السجل الأكاديمي";
$lang['ltr_notice'] = "إشعار";
$lang['ltr_doubts_ask'] = "طرح شكوك";
$lang['ltr_paid'] = "مدفوع";
$lang['ltr_add_certificate'] = "إضافة شهادة";
$lang['ltr_all'] = "الكل";
$lang['ltr_edit'] = "تحرير";
$lang['ltr_filter'] = "عامل التصفية";
$lang['ltr_select_month'] = "اختر الشهر";
$lang['ltr_select_year'] = "اختر السنة";
$lang['ltr_time'] = "الوقت";
$lang['ltr_contact_no'] = "رقم الاتصال";
$lang['ltr_pending'] = "معلق";
$lang['ltr_pending'] = "معلق";
$lang['ltr_approved'] = "موافق عليه";
$lang['ltr_decline'] = "رفض";
$lang['ltr_student_doubts_ask'] = "شكوك";
$lang['ltr_complete'] = "مكتمل";
$lang['ltr_education'] = "التعليم";
$lang['ltr_subjects'] = "الموضوعات";
$lang['ltr_batchs'] = "دفعات";
$lang['ltr_batch'] = "دفعة";
$lang['ltr_select_batch'] = "اختر دفعة";
$lang['ltr_new_batch'] = "دفعة جديدة";
$lang['ltr_add_batch'] = "إضافة دفعة";
$lang['ltr_students'] = "الطلاب";
$lang['ltr_teachers'] = "المعلمون";
$lang['ltr_actions'] = "الإجراءات";
$lang['ltr_action'] = "إجراء";
$lang['ltr_start_time'] = "وقت البدء";
$lang['ltr_end_time'] = "وقت الانتهاء";
$lang['ltr_paid'] = "مدفوع";
$lang['ltr_subject'] = "الموضوع";
$lang['ltr_chapter'] = "فصل";
$lang['ltr_save_batch'] = "حفظ دفعة";
$lang['ltr_password'] = "كلمة المرور";
$lang['ltr_courses_offered']="الدورات المقدمة";
$lang['ltr_facilities']="مريح";
$lang['ltr_home']="بيت";
$lang['ltr_about_us']="عن";
$lang['ltr_contact_us']="اتصل بنا";
$lang['ltr_language_settings']="اعدادات اللغة";
$lang['ltr_value_set'] = "إذا لم يتم تحديده ، فسيأخذ القيمة الافتراضية (0.25٪) للعلامة السلبية";
$lang['ltr_totale_mask'] = "مجموع علامات";